package mada.tsara.backend.share;

import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;

import java.io.File;
import java.io.FileOutputStream;
import android.app.Activity;
import android.content.ContentResolver;
import android.os.Build;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;
import android.util.Log;
import org.apache.commons.io.FileUtils;
import java.io.IOException;

/**
 * Created by Miary on 27/05/2017.
 */

public final class UriHelpers {

    private UriHelpers() {}

    public static void removeAllTmpFiles()
    {
        String root = Environment.getExternalStorageDirectory().toString();
        File directory = new File(root + "/mada.tsara.backend");
        try {
            //Deleting the directory recursively using FileUtils.
            FileUtils.deleteDirectory(directory);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static String getFileFromUri(Context context, Uri uri)
    {
        InputStream inputStream = null;
        OutputStream outputStream = null;
        String privateFilePath = "";
        if (uri.getAuthority() != null) {
            try {
                inputStream =
                    context.getContentResolver().openInputStream(uri);
                Bitmap bmp = BitmapFactory.decodeStream(inputStream);
                return getSaveTempBitmap(bmp);
            } catch (FileNotFoundException e) {
               Log.d(Config.TAGKEY, "FileNotFoundException: " + e);
            } finally {
                // closeInputStream(inputStream);
            }
        }
        return "";
    }
    public static String getSaveTempBitmap(Bitmap bitmap) {
        if (isExternalStorageWritable()) {
            return getSavedImage(bitmap);
        }
        return "";
    }
    private static String getSavedImage(Bitmap finalBitmap) {

        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/mada.tsara.backend");
        myDir.mkdirs();

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String fname = timeStamp +".jpg";

        File file = new File(myDir, fname);
        if (file.exists()) file.delete ();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();
            return root + "/mada.tsara.backend/" + fname;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }
    public static boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }
}
