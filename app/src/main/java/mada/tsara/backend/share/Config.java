package mada.tsara.backend.share;

/**
 * Created by Miary on 24/05/2017.
 */

public class Config {

    // Tag log
    public static final String TAGKEY = "TSARADMIN";

    public static final String BASE_URL = "https://www.madatsara.com";
    public static final String URLAPI = BASE_URL + "/rest/";

    /**
     * CHEMIN REPERTOIRE thumb
     */
    public static final String PATH_FLYERS_THUMB = BASE_URL+ "/media/cache/my_thumb/assets/flyers/";

    public static final String PATH_FLYERS_THUMB_70 = BASE_URL+ "/media/cache/my_thumb_70/assets/flyers/";
    public static final String PATH_FLYERS_CROP_100 = BASE_URL+ "/media/cache/crop_100_100/assets/flyers/";

    /**
     * CHEMIN REPERTOIRE thumb
     */
    public static final String PATH_FLYERS = BASE_URL+ "/assets/flyers/";
    public static final String PATH_TMP = BASE_URL+ "/tmp/";

    /**
     * NOM par defaut
     */
    public static final String DEFAULT_PREF_NAME = "MADATSARA";

    /**
     * NOM par defaut
     */
    public static final String DEFAULT_EMAIL_ADMIN = "rabehasy@gmail.com";

    /**
     * Identifiant domaine dans parseurl
     */
    public static final int URL_DOMAIN = 2;

    /**
     * Zoom par defaut
     */
    public static final int ZOOM_DEFAULT = 16;

    /**
     * Limite par defaut realm
     */
    public static final int DEFAULT_LIMIT_REALM = 100;

    /**
     * Default thresold autocomplete
     */
    public static final int DEFAULT_THRESHOLD = 2;
    public static final int DEFAULT_LENGTH_ARTISTE = 50;

    /**
     * CHEMIN REPERTOIRE thumb
     */
    public static final String PATH_ARTISTES_THUMB = BASE_URL+ "/media/cache/my_thumb_70/assets/artistes_orig/";

    public static final int ENTREETYPE_ID_SHOWPRICE = 2;

    /**
     * IDENTIFIANT Thematique par defaut - Sorties/musique
     */
    public static final String THEMATIQUEID = "1";

    /**
     * IDENTIFIANT Entrée par defaut - Payant
     */
    public static final String ENTREETYPEID = "2";

    /**
     * IDENTIFIANT Type artiste - artiste
     */
    public static final String ARTISTETYPE = "artiste";


}
