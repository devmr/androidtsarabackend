package mada.tsara.backend.share;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Random;

import mada.tsara.backend.activities.ImageFullscreenActivity;
import mada.tsara.backend.models.ArtistetypeRetrofit;

/**
 * Created by Miary on 25/05/2017.
 */

public class Utils {

    /**
     * A-t-on les permissions?
     *
     * @return
     */
    public static boolean getPermissionReadExternalStorage(Activity activity)
    {
        int REQUEST_EXTERNAL_STORAGE = 0;
        //Permissions?
        int hasPermission = ContextCompat.checkSelfPermission( activity, Manifest.permission.READ_EXTERNAL_STORAGE);
        if ( hasPermission != PackageManager.PERMISSION_GRANTED) {
            if ( ! ActivityCompat.shouldShowRequestPermissionRationale( activity, Manifest.permission.READ_EXTERNAL_STORAGE )) {

                ActivityCompat.requestPermissions( activity, new String[] {Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_EXTERNAL_STORAGE);
                return false;
            }

            ActivityCompat.requestPermissions( activity,
                    new String[] {Manifest.permission.READ_EXTERNAL_STORAGE},
                    REQUEST_EXTERNAL_STORAGE);
            return false;
        }
        return true;
    }

    public static void showSnackLong(CoordinatorLayout coordinatorLayout, String message) {
        Snackbar.
                make(coordinatorLayout, message, Snackbar.LENGTH_LONG).show();
    }

    public static void showSnackLong(CoordinatorLayout coordinatorLayout, String message, String Action, View.OnClickListener clickSnack) {
        Snackbar.
                make(coordinatorLayout, message, Snackbar.LENGTH_LONG)
                .setAction(Action, clickSnack).show();
    }

    public static int dipToPx(Context c, float dipValue) {
        DisplayMetrics metrics = c.getResources().getDisplayMetrics();
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dipValue, metrics);
    }

    public static int getRandomNumberInRange(int min, int max) {

        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }

    public static CharSequence getHumanizeDate(String date, String patternIn, String patternOut) {



        if (date==null)
            return "";

        if (!isValidDate(date,patternIn))
            return "";


        String dateSortie = "";
        DateTimeFormatter dtf = DateTimeFormat.forPattern(patternIn);
        DateTime jdt = dtf.parseDateTime(date);
        DateTimeFormatter dtfOut = DateTimeFormat.forPattern(patternOut).withLocale(Locale.FRENCH);
        dateSortie = dtfOut.print(jdt);

        return dateSortie;
    }

    public static boolean isValidDate(String dateToValidate,String patternIn){

        try {
            DateTimeFormatter fmt = DateTimeFormat.forPattern(patternIn);
            fmt.parseDateTime(dateToValidate);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public static ArrayList<ArtistetypeRetrofit> getGenericArtisteType() {
        ArrayList<ArtistetypeRetrofit> artistetypes = new ArrayList<ArtistetypeRetrofit>();

        ArrayList<String> sList = new ArrayList<String>();
        sList.add("--- Choisir ---");
        sList.add("artiste");
        sList.add("dj");
        sList.add("organisateur");

        for ( String s: sList) {
            ArtistetypeRetrofit artistetype = new ArtistetypeRetrofit();
            artistetype.setName(s);
            artistetype.setValue(s);

            artistetypes.add(artistetype);
        }
        return artistetypes;
    }

    public static void doOpenFullscreenImage(Uri mimageUri, String saveHiddenFieldImageView, Context context) {
//        Log.d(Config.TAGKEY, " doOpenFullscreenImage - mimageUri: "+mimageUri.toString()+" - saveHiddenFieldImageView: "+saveHiddenFieldImageView);
        Intent i = new Intent(context, ImageFullscreenActivity.class );
        i.putExtra("mimageUri", mimageUri );
        i.putExtra("fileremote", saveHiddenFieldImageView );
        context.startActivity(i);

    }


}
