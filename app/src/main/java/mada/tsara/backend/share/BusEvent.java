package mada.tsara.backend.share;

import mada.tsara.backend.models.EventRetrofit;

/**
 * Created by Miary on 31/05/2017.
 */

public class BusEvent{

    boolean scrollToTop;
    boolean fullScreen;
    boolean exitFullScreen;
    boolean snackevent;
    EventRetrofit event;
    int eventID;
    String articleCount = "";
    int currentTabSelected;
    boolean draweropen = false;
    boolean appbarExpanded;
    boolean finishActivity;
    boolean finishPopupActivity;
    boolean showkeyboard;
    boolean hideKeyboard;
    boolean shownodata;
    boolean hidenodata;
    boolean backpress;
    boolean backpressByEdit;
    String fragmentOpened;
    boolean resultActivity;
    int artisteId;
    String artisteName;

    public BusEvent() {}

    public BusEvent(boolean scrollToTop) {
        this.scrollToTop = scrollToTop;
    }

    public boolean getScrollToTop(){
        return this.scrollToTop;
    }

    public void setScrollToTop(boolean scrollToTop){
        this.scrollToTop = scrollToTop;
    }

    public boolean getFullScreen(){
        return this.fullScreen;
    }

    public void setFullScreen(boolean fullScreen){
        this.fullScreen = fullScreen;
    }

    public boolean getExitFullScreen(){
        return this.exitFullScreen;
    }

    public void setExitFullScreen(boolean exitFullScreen){
        this.exitFullScreen = exitFullScreen;
    }

    public EventRetrofit getEventSent(){
        return this.event;
    }

    public void setEventSend(EventRetrofit event){
        this.event = event;
    }

    public void setSnackEvent(boolean snackevent){ this.snackevent = snackevent; }

    public boolean getSnackEvent(){
        return this.snackevent;
    }

    public void setEventID(int eventID){ this.eventID = eventID; }

    public int getEventID(){
        return this.eventID;
    }

    public void setArticleCount(String articleCount){ this.articleCount = articleCount; }

    public String getArticleCount(){
        return this.articleCount;
    }

    public void setCurrentTabSelected(int currentTabSelected){ this.currentTabSelected = currentTabSelected; }

    public int getCurrentTabSelected(){ return this.currentTabSelected; }

    public void setDrawerOpen(boolean drawerOpen) {
        this.draweropen = drawerOpen;
    }
    public boolean getDrawerOpen(){
        return this.draweropen;
    }

    public boolean getAppbarExpanded(){
        return this.appbarExpanded;
    }

    public void setAppbarExpanded(boolean appbarExpanded){
        this.appbarExpanded = appbarExpanded;
    }

    public boolean getFinishActivity(){ return this.finishActivity; }
    public boolean getFinishPopupActivity(){ return this.finishPopupActivity; }

    public void setFinishActivity(boolean finishActivity) { this.finishActivity = finishActivity; }
    public void setFinishPopupActivity(boolean finishPopupActivity) { this.finishPopupActivity = finishPopupActivity; }

    public boolean getShowKeyboard(){ return this.showkeyboard; }

    public void setShowKeyboard(boolean showkeyboard) { this.showkeyboard = showkeyboard; }

    public boolean getHideKeyboard(){ return this.hideKeyboard; }

    public void setHideKeyboard(boolean hideKeyboard) { this.hideKeyboard = hideKeyboard; }

    public boolean getShowNodata(){ return this.shownodata; }
    public void setShowNodata(boolean shownodata){  this.shownodata = shownodata; }

    public boolean getHideNodata(){ return this.hidenodata; }
    public void setHideNodata(boolean hidenodata){  this.hidenodata = hidenodata; }

    public boolean getParentBackPressed(){ return this.backpress; }
    public void setParentBackPressed(boolean backpress){  this.backpress = backpress; }

    public void setFragmentOpened(String fragmentOpened) {
        this.fragmentOpened = fragmentOpened;
    }
    public String getFragmentOpened() {
        return this.fragmentOpened;
    }

    public void setResultActivity(int id, String name) {
        this.resultActivity = true;
        this.artisteId = id;
        this.artisteName = name;
    }

    public boolean getResultActivity() {
        return this.resultActivity;
    }

    public int getResultActivityArtisteId() {
        return this.artisteId;
    }

    public String getResultActivityArtisteName() {
        return this.artisteName;
    }

    public boolean getParentBackPressedByEdit(){ return this.backpressByEdit; }
    public void setParentBackPressedByEdit(boolean backpressByEdit){  this.backpressByEdit = backpressByEdit; }
}
