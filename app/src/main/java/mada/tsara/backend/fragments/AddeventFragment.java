package mada.tsara.backend.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.bumptech.glide.Glide;
import com.joanzapata.iconify.widget.IconButton;
import com.joanzapata.iconify.widget.IconTextView;
import com.squareup.timessquare.CalendarPickerView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import io.realm.Realm;
import io.realm.exceptions.RealmMigrationNeededException;
import mada.tsara.backend.R;
import mada.tsara.backend.activities.EditeventActivity;
import mada.tsara.backend.activities.PopupActivity;
import mada.tsara.backend.adapters.ArtistsRecyclerviewAdapter;
import mada.tsara.backend.adapters.AutocompleteArtistsAdapter;
import mada.tsara.backend.adapters.AutocompleteFeteAdapter;
import mada.tsara.backend.adapters.AutocompleteLieuxAdapter;
import mada.tsara.backend.adapters.AutocompleteSearchEventAdapter;
import mada.tsara.backend.adapters.FeteRecyclerviewAdapter;
import mada.tsara.backend.adapters.FlyersRecyclerviewAdapter;
import mada.tsara.backend.adapters.LieuxRecyclerviewAdapter;
import mada.tsara.backend.adapters.SpinArtistetypeAdapter;
import mada.tsara.backend.adapters.SpinBaseAdapter;
import mada.tsara.backend.adapters.SpinEntreetypeAdapter;
import mada.tsara.backend.customview.ClickListener;
import mada.tsara.backend.customview.RecyclerTouchListener;
import mada.tsara.backend.models.AllEntreeTypeRetrofit;
import mada.tsara.backend.models.AllEventTypeRetrofit;
import mada.tsara.backend.models.EntreetypeRetrofit;
import mada.tsara.backend.models.EventArtistesDjOrganisateurRetrofit;
import mada.tsara.backend.models.EventFlyersRetrofit;
import mada.tsara.backend.models.EventLocalRetrofit;
import mada.tsara.backend.models.EventMultiLieuRetrofit;
import mada.tsara.backend.models.EventRetrofit;
import mada.tsara.backend.models.EventlieuRetrofit;
import mada.tsara.backend.models.EventtypeRetrofit;
import mada.tsara.backend.models.GenericResultRetrofit;
import mada.tsara.backend.models.UploadFlyerRetrofit;
import mada.tsara.backend.service.RetrofitApi;
import mada.tsara.backend.service.RetrofitInterface;
import mada.tsara.backend.share.BusEvent;
import mada.tsara.backend.share.Config;
import mada.tsara.backend.share.UriHelpers;
import mada.tsara.backend.share.Utils;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;
import static java.util.Collections.max;
import static mada.tsara.backend.R.id.edittext_artiste;
import static mada.tsara.backend.R.id.iconDeleteT;
import static mada.tsara.backend.share.Config.PATH_FLYERS;
import java.text.SimpleDateFormat;
import java.util.Date;
import android.os.Environment;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AddeventFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AddeventFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddeventFragment extends Fragment {



    // UI
    View rootView;

    ImageView imageView;

    IconButton buttonUploadFlyer;
    IconButton btnSaveEvent;
//    IconButton btnSearchEvent;
//    IconButton txtDateEvent;
    IconButton txtDateEventts;
    IconButton btnNoDataAddLieu;
    IconButton btnNoDataAddArtists;
    IconButton btnNoDataAddFete;

    IconButton btnDateEventUnique;
    IconButton btnDateEventRange;
    IconButton btnDateEventMultiple;

    ProgressBar pgHorizontal;
    ProgressBar progressBarSave;

    CoordinatorLayout coordinatorLayout;

    IconTextView txtUploadFlyer;

    Spinner spinnThematique;
    Spinner spinnEntreetype;

    ArrayList<EventtypeRetrofit> eventtypeRetrofits;
    ArrayList<EntreetypeRetrofit> entreetypeRetrofits;

    SpinBaseAdapter spinnAdapter;

    SpinEntreetypeAdapter spinnEntreetypeAdapter;

    RetrofitInterface apiService = RetrofitApi.getClient().create(RetrofitInterface.class);


    Uri imageUri;

    DatePickerDialog dpUnique;
    DatePickerDialog dpDdebut;
    DatePickerDialog dpDFin;
    DatePickerDialog dpDMulti;
    TimePickerDialog tp;

    AutoCompleteTextView editLieu;
    AutoCompleteTextView editArtist;
    AutoCompleteTextView editFete;
    AutoCompleteTextView editSearchEvent;



    TextInputLayout textInputLayoutEditPrice;

    EditText editPrice;
    EditText editName;

    AutocompleteLieuxAdapter autocompleteLieuAdapter;
    AutocompleteArtistsAdapter autocompleteArtistsAdapter;
    AutocompleteFeteAdapter autocompleteFeteAdapter;
    AutocompleteSearchEventAdapter autocompleteSearchEvent;

    private static final String ARG_PARAM1 = "imageUri";
    private static final String ARG_PARAM2 = "eventId";
    private static final String ARG_PARAM3 = "fieldImageView";

    private Uri mimageUri;

    private OnFragmentInteractionListener mListener;

    Calendar c = Calendar.getInstance();
    int mYear = c.get(Calendar.YEAR);
    int mMonth = c.get(Calendar.MONTH);
    int mDay = c.get(Calendar.DAY_OF_MONTH);
    int hour = c.get(Calendar.HOUR_OF_DAY);
    int minute = c.get(Calendar.MINUTE);
    boolean is24hour;

    ArrayList<Integer> lieuxTAGIDsArray = new ArrayList<Integer>();
    ArrayList<String> lieuxTAGNamesArray = new ArrayList<String>();
    ArrayList<Integer> artistsTAGIDsArray = new ArrayList<Integer>();
    ArrayList<String> artistsTAGNamesArray = new ArrayList<String>();
    ArrayList<Integer> feteTAGIDsArray = new ArrayList<Integer>();
    ArrayList<String> feteTAGNamesArray = new ArrayList<String>();
    ArrayList<Integer> flyersTAGIDsArray = new ArrayList<Integer>();
    ArrayList<String> flyersTAGImagesArray = new ArrayList<String>();

    ArrayList<Date> dateSelected = new ArrayList<>();

    Dialog dArtist;

    TextView hiddenFieldImageView;
    TextView hiddenFieldDate;
    TextView hiddenFieldOptDate;
    TextView txtNoDataLieux;
    TextView txtNoDataArtists;
    TextView txtNoDataFete;
    TextView txtdateSelected;

    SwitchCompat swHidden;
    SwitchCompat swCancelled;

    RelativeLayout relativeNoDataLieux;
    RelativeLayout relativeNoDataArtists;
    RelativeLayout relativeNoDataFete;

    // Saved Instance
    String saveFieldname;
    Uri saveMimageUri;
    String saveSpinnThematique;
    String saveSpinnEntreetype;
    String saveEditPrice;
    String saveTxtDateEvent;
    String saveHiddenFieldImageView;
    String saveHiddenFieldDate;
    String saveHiddenFieldOptDate;
    boolean saveSwCancelled = false;
    boolean saveSwHidden = false;

    // a t-on le droit de fermer l'activity parente?
    boolean canCloseActivityParent = true;
    boolean isEditMode = false;
    boolean saveAfterUploadFlyer = false;
    Realm realm;

    int eventId;

    RecyclerView recyclerviewLieux;
    RecyclerView recyclerviewArtists;
    RecyclerView recyclerviewFetes;
    RecyclerView recyclerviewFlyers;

    List<EventlieuRetrofit> lieuxLists  = new ArrayList<EventlieuRetrofit>();
    List<EventArtistesDjOrganisateurRetrofit> artistsLists  = new ArrayList<EventArtistesDjOrganisateurRetrofit>();
    List<EventLocalRetrofit> fetesLists  = new ArrayList<EventLocalRetrofit>();
    List<EventFlyersRetrofit> flyersLists  = new ArrayList<EventFlyersRetrofit>();

    LieuxRecyclerviewAdapter adapterLieux;
    ArtistsRecyclerviewAdapter adapterArtists;
    FeteRecyclerviewAdapter adapterFetes;
    FlyersRecyclerviewAdapter adapterFlyers;

    CalendarPickerView dialogView;
    AlertDialog theDialog;


    public AddeventFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AddeventFragment.
     */
    public static AddeventFragment newInstance(Uri imageUri) {
        Log.d(Config.TAGKEY,  " - STEP - AddeventFragment newInstance NEW "+imageUri.toString() );
        AddeventFragment fragment = new AddeventFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM1, imageUri);
        fragment.setArguments(args);
        return fragment;
    }

    public static AddeventFragment newInstance(int eventId, Uri imageUri, String fieldImageView) {
        Log.d(Config.TAGKEY,  " - STEP - AddeventFragment newInstance EDIT "+imageUri.toString()+"-"+eventId+"-"+fieldImageView );
        AddeventFragment fragment = new AddeventFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM1, imageUri);
        args.putInt(ARG_PARAM2, eventId);
        args.putString(ARG_PARAM3, fieldImageView);
        fragment.setArguments(args);
        return fragment;
    }

    public void onSaveInstanceState(Bundle outState) {
        Log.d(Config.TAGKEY,  " - STEP - onSaveInstanceState " );

        outState.putString("editName", editName.getText().toString() );
        outState.putBoolean("swHidden", swHidden.isChecked() );
        outState.putBoolean("swCancelled", swCancelled.isChecked() );
        outState.putParcelable("mImageUri", mimageUri);
        outState.putString("spinnThematique", spinnThematique.getSelectedItem().toString() );
        outState.putString("spinnEntreetype", spinnEntreetype.getSelectedItem().toString() );
        outState.putString("editPrice", editPrice.getText().toString() );
//        outState.putString("txtDateEvent", txtDateEvent.getText().toString() );
        outState.putString("txtDateEvent", txtdateSelected.getText().toString() );
        outState.putString("hiddenFieldImageView", hiddenFieldImageView.getText().toString() );
        outState.putString("hiddenFieldDate", hiddenFieldDate.getText().toString() );

        super.onSaveInstanceState(outState);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(Config.TAGKEY,  " AddeventFragment - STEP - onCreate");
        super.onCreate(savedInstanceState);
        // Keeps this Fragment alive during configuration changes
        setRetainInstance(true);
        if (getArguments() != null) {
            mimageUri = getArguments().getParcelable(ARG_PARAM1);
            canCloseActivityParent = false;
            Log.d(Config.TAGKEY,  " AddeventFragment - STEP - onCreate - "+mimageUri.toString());
        }

        // Si l'image a deja ete uploadé
        if (getArguments().getString(ARG_PARAM3)!=null && !TextUtils.isEmpty(getArguments().getString(ARG_PARAM3))) {
            Log.d(Config.TAGKEY,  " AddeventFragment - STEP - onCreate -  - getArguments().getString(ARG_PARAM3) " +getArguments().getString(ARG_PARAM3));
            doActionAfterUploadedFile(getArguments().getString(ARG_PARAM3));
        }

        //Si l'event est edita
        if (  getArguments().getInt(ARG_PARAM2)>0) {
            Log.d(Config.TAGKEY,  " AddeventFragment - STEP - onCreate -  - getArguments().getInt(ARG_PARAM2) " +getArguments().getInt(ARG_PARAM2));
            isEditMode = true;
            eventId = getArguments().getInt(ARG_PARAM2);
        }
        
        doSetRealm();
    }

    private void doSetRealm() {
        try {
            realm = Realm.getInstance(getContext());
        } catch (RealmMigrationNeededException e) {
            Log.d(Config.TAGKEY,  " - STEP - doSetRealm - RealmMigrationNeededException : "+e.getMessage() );
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_addevent, container, false);
        return rootView;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        EventBus.getDefault().unregister(this);
    }
    @Subscribe
    public void onEvent(BusEvent event) {
        Log.d(Config.TAGKEY, " - STEP - onEvent" );
        if ( event.getParentBackPressed() ) {
            Log.d(Config.TAGKEY,  " - STEP - onEvent - event.getParentBackPressed():" );
            onBackPressed();
        }

        if ( event.getParentBackPressedByEdit() ) {
            Log.d(Config.TAGKEY,  " - STEP - onEvent - event.getParentBackPressedByEdit():" );
            onBackPressedByEdit();
        }

        if (event.getResultActivity()) {
            Log.d(Config.TAGKEY,  " - STEP - onEvent - getResultActivity");
            doAddArtistsToRecycler(event.getResultActivityArtisteId(), event.getResultActivityArtisteName());

        }
    }

    private void doAddArtistsToRecycler(int id, String name) {

        EventArtistesDjOrganisateurRetrofit item = new EventArtistesDjOrganisateurRetrofit();

        if (artistsTAGIDsArray.contains(id) || artistsTAGNamesArray.contains(name)) {
//            Utils.showSnackLong(coordinatorLayout,getString(R.string.txtErrorArtistsExistsIntag));
            return;
        }

        artistsTAGIDsArray.add(id);
        artistsTAGNamesArray.add(name);

        item.setId(id);
        item.setName(name);

        // Ajouter dans le adapter du recyclerview
        doRefreshAdapter(item);

        // Cacher bouton add
        relativeNoDataArtists.setVisibility(View.GONE);

        // Vider le champ artiste
        editArtist.setText("");

    }

    public void onBackPressed() {
        Log.d(Config.TAGKEY, " - STEP - onBackPressed in Fragment");
        Log.d(Config.TAGKEY, " - STEP - onBackPressed in Fragment - canCloseActivityParent: "+canCloseActivityParent);
//        getFragmentManager().popBackStack();
        doCloseActivityOrConfirm();

    }

    private void doCloseActivityOrConfirm() {
        if (canCloseActivityParent) {
            // On ferme l'appli parente
            doSendEventCloseActivity(0);
        } else {
            doSetConfirmCloseActivity();
        }
    }

    public void onBackPressedByEdit() {
        Log.d(Config.TAGKEY, " - STEP - onBackPressedByEdit in Fragment");
        canCloseActivityParent = true;
        doCloseActivityOrConfirm();
    }



    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        Log.d(Config.TAGKEY,  " - onActivityCreated");
        super.onActivityCreated(savedInstanceState);

        if ( savedInstanceState != null ) {
            Log.d(Config.TAGKEY,  " - onActivityCreated savedInstanceState not null");
            getSavedInstance(savedInstanceState);
        }

        // Initialiser les vues
        initUi();
        doSetUi();

        doFillUiWithInstancedState();

        // En mode edition de l'event
        doFillUiWhenEditMode();
    }

    private void doSetDefaultValues() {
        if (isEditMode)
            return;
        // Thematique par defaut
        saveSpinnThematique = Config.THEMATIQUEID;

        // Entree par defaut
        saveSpinnEntreetype = Config.ENTREETYPEID;
    }

    private void getSavedInstance(Bundle savedInstanceState) {
        Log.d(Config.TAGKEY,  " - getSavedInstance");
        saveFieldname = savedInstanceState.getString("editName");
        mimageUri = savedInstanceState.getParcelable("mimageUri");
        saveSpinnThematique = savedInstanceState.getString("spinnThematique");
        saveSpinnEntreetype = savedInstanceState.getString("spinnEntreetype");
        saveSpinnEntreetype = savedInstanceState.getString("editPrice");
        saveTxtDateEvent = savedInstanceState.getString("txtDateEvent");
        saveHiddenFieldImageView = savedInstanceState.getString("hiddenFieldImageView");
        saveHiddenFieldDate = savedInstanceState.getString("hiddenFieldDate");
        saveHiddenFieldOptDate = savedInstanceState.getString("hiddenFieldOptDate");
        saveSwCancelled = savedInstanceState.getBoolean("swCancelled");
        saveSwHidden = savedInstanceState.getBoolean("swHidden");

        Log.d(Config.TAGKEY,  " - getSavedInstance - saveFieldname:: "+saveFieldname);
    }

    private void initUi() {
        imageView = (ImageView) rootView.findViewById(R.id.sentImage);

        buttonUploadFlyer = (IconButton) rootView.findViewById(R.id.buttonUploadFlyer);
        btnSaveEvent = (IconButton) rootView.findViewById(R.id.btnSaveGeneric);
//        btnSearchEvent = (IconButton) rootView.findViewById(R.id.btnSearchEvent);
        btnNoDataAddLieu = (IconButton) rootView.findViewById(R.id.btnNoDataAddLieu);
        btnNoDataAddArtists = (IconButton) rootView.findViewById(R.id.btnNoDataAddArtists);
        btnNoDataAddFete = (IconButton) rootView.findViewById(R.id.btnNoDataAddFete);

        pgHorizontal = (ProgressBar) rootView.findViewById(R.id.progressBar);
        progressBarSave = (ProgressBar) rootView.findViewById(R.id.progressBarSave);

        coordinatorLayout = (CoordinatorLayout) rootView.findViewById(R.id.coordinatorLayout);

        txtUploadFlyer = (IconTextView) rootView.findViewById(R.id.txtUploadFlyer);
//        txtDateEvent = (IconButton) rootView.findViewById(R.id.txtDateEvent);
        btnDateEventUnique = (IconButton) rootView.findViewById(R.id.btnDateEventUnique);
        btnDateEventRange = (IconButton) rootView.findViewById(R.id.btnDateEventRange);
        btnDateEventMultiple = (IconButton) rootView.findViewById(R.id.btnDateEventMultiple);

        spinnThematique = (Spinner) rootView.findViewById(R.id.spinnThematique);
        spinnEntreetype = (Spinner) rootView.findViewById(R.id.spinnEntreetype);

        editLieu = (AutoCompleteTextView) rootView.findViewById(R.id.editLieu);
        editArtist = (AutoCompleteTextView) rootView.findViewById(R.id.editArtist);
        editFete = (AutoCompleteTextView) rootView.findViewById(R.id.editFete);
        editSearchEvent = (AutoCompleteTextView) rootView.findViewById(R.id.editSearchEvent);

        editName  = (EditText) rootView.findViewById(R.id.editName);
        editPrice  = (EditText) rootView.findViewById(R.id.editPrice);




        textInputLayoutEditPrice  = (TextInputLayout) rootView.findViewById(R.id.textInputLayoutEditPrice);



        autocompleteLieuAdapter = new AutocompleteLieuxAdapter(getActivity());
        autocompleteArtistsAdapter = new AutocompleteArtistsAdapter(getActivity());
        autocompleteFeteAdapter = new AutocompleteFeteAdapter(getActivity());
        autocompleteSearchEvent = new AutocompleteSearchEventAdapter(getActivity());

        hiddenFieldImageView = (TextView) rootView.findViewById(R.id.hiddenFieldImageView);
        hiddenFieldDate = (TextView) rootView.findViewById(R.id.hiddenFieldDate);
        hiddenFieldOptDate = (TextView) rootView.findViewById(R.id.hiddenFieldOptDate);
        txtNoDataLieux = (TextView) rootView.findViewById(R.id.txtNoDataLieux);
        txtNoDataArtists = (TextView) rootView.findViewById(R.id.txtNoDataArtists);
        txtNoDataFete = (TextView) rootView.findViewById(R.id.txtNoDataFete);
        txtdateSelected = (TextView) rootView.findViewById(R.id.dateSelected);

        swHidden = (SwitchCompat) rootView.findViewById(R.id.switchHidden);
        swCancelled = (SwitchCompat) rootView.findViewById(R.id.switchCancelled);

        relativeNoDataLieux = (RelativeLayout) rootView.findViewById(R.id.relativeNoDataLieux);
        relativeNoDataArtists = (RelativeLayout) rootView.findViewById(R.id.relativeNoDataArtists);
        relativeNoDataFete = (RelativeLayout) rootView.findViewById(R.id.relativeNoDataFete);

        recyclerviewLieux = (RecyclerView) rootView.findViewById(R.id.recyclerviewLieux);
        recyclerviewArtists = (RecyclerView) rootView.findViewById(R.id.recyclerviewArtists);
        recyclerviewFetes = (RecyclerView) rootView.findViewById(R.id.recyclerviewFetes);
        recyclerviewFlyers = (RecyclerView) rootView.findViewById(R.id.recyclerviewFlyers);



    }

    private void doFillUiWhenEditMode() {
        Log.d(Config.TAGKEY,  " - doFillUiWhenEditMode");
        if (!isEditMode)
            return;

        // Disable
        spinnEntreetype.setEnabled(false);
        spinnThematique.setEnabled(false);

        Handler handler = new Handler();
        Runnable runnable;

        runnable = new Runnable() {
            @Override
            public void run() {
                Log.d(Config.TAGKEY," - doFillUiWhenEditMode in Runnable ");
                // Recup infos sur le serveur
                getEventServerDetail(eventId);

            }
        };
        handler.postDelayed(runnable,3000);



    }

    /**
     * Recup infos sur le serveur
     *
     * @param eventid
     */
    private void getEventServerDetail(int eventid) {
        Log.d(Config.TAGKEY,  " - getEventServerDetail");
        if (!isEditMode)
            return;

        //Events de la semaine
        final Call<EventRetrofit> events = apiService.getEvent( eventid );
        events.enqueue(new Callback<EventRetrofit>() {
            @Override
            public void onResponse(Call<EventRetrofit> call, Response<EventRetrofit> response) {
                Log.d(Config.TAGKEY, " - STEP - getEventServerDetail - onResponse - "+response.code()+" - "+response.message()+" - URLS : "+call.request().url()  );

                if ( response.code() != 200 ) {
                    return;
                }

                EventRetrofit event = response.body() ;
                // shareMethod.saveEventToRealm(event);

                // checkRealmAndFill();



                saveFieldname = event.getName();
                saveEditPrice = event.getPrixenclair();
                saveTxtDateEvent = event.getDateClair();
                saveHiddenFieldImageView = event.getFlyer();
                saveHiddenFieldDate = event.getDateClair();
                saveHiddenFieldOptDate = (!TextUtils.isEmpty(event.getOptDateClair())?event.getOptDateClair():"unique");
                saveSwCancelled = event.getCancelledAt();
                saveSwHidden = event.getHidden();
                saveSpinnEntreetype = event.getEntreetype().getId().toString();
                saveSpinnThematique = event.getEventtype().get(0).getId().toString();
               Log.d(Config.TAGKEY, " - STEP - getEventServerDetail "+event.getFlyer() );
               Log.d(Config.TAGKEY, " - STEP - getEventServerDetail "+saveHiddenFieldImageView );
               Log.d(Config.TAGKEY, " - STEP - getEventServerDetail "+saveTxtDateEvent );
               Log.d(Config.TAGKEY, " - STEP - getEventServerDetail "+saveFieldname );
               Log.d(Config.TAGKEY, " - STEP - getEventServerDetail "+saveEditPrice );
               Log.d(Config.TAGKEY, " - STEP - getEventServerDetail "+saveHiddenFieldDate );
               Log.d(Config.TAGKEY, " - STEP - getEventServerDetail "+saveHiddenFieldOptDate );
               Log.d(Config.TAGKEY, " - STEP - getEventServerDetail "+saveSwCancelled );
               Log.d(Config.TAGKEY, " - STEP - getEventServerDetail "+saveSwHidden );
               Log.d(Config.TAGKEY, " - STEP - getEventServerDetail "+saveSpinnEntreetype );
               Log.d(Config.TAGKEY, " - STEP - getEventServerDetail "+saveSpinnThematique );
               /*Log.d(Config.TAGKEY, " - STEP - getEventServerDetail "+event.getEntreetype() );
                Log.d(Config.TAGKEY, " - STEP - getEventServerDetail "+event.getEventLieu().getName() );
                Log.d(Config.TAGKEY, " - STEP - getEventServerDetail - event.getEntreetype().getId(): "+event.getEntreetype().getId() );
                Log.d(Config.TAGKEY, " - STEP - getEventServerDetail "+event.getEventtype().size() );
                Log.d(Config.TAGKEY, " - STEP - getEventServerDetail - event.getEventtype().get(0).getId() "+event.getEventtype().get(0).getId() );*/

                doFillUiWithInstancedState();

                for (EventArtistesDjOrganisateurRetrofit artist:
                     event.getEventArtistesDjOrganisateurs()) {
                    if (!artistsTAGNamesArray.contains(artist.getId())) {

                        artistsTAGIDsArray.add(artist.getId());
                        artistsTAGNamesArray.add(artist.getName());

                        // Ajouter dans le adapter du recyclerview
                        doRefreshAdapter(artist);

                    }
                }

                //Lieu
                if (event.getEventLieu()!=null) {
                    EventlieuRetrofit lieu = event.getEventLieu();
                    Log.d(Config.TAGKEY, " - STEP - getEventServerDetail "+lieuxTAGNamesArray );
                    if (!lieuxTAGIDsArray.contains(lieu.getId())) {

                        Log.d(Config.TAGKEY, " - STEP - getEventServerDetail add "+lieu.getName() );
                        lieuxTAGIDsArray.add(lieu.getId());
                        lieuxTAGNamesArray.add(lieu.getName());


                        // Ajouter dans le adapter du recyclerview
                        doRefreshAdapter(lieu);


                    }
                }

                // Multiples lieux1
                for (EventMultiLieuRetrofit lieu:
                        event.getEventmultilieu()) {
                    if (!lieuxTAGIDsArray.contains(lieu.getId())) {

                        lieuxTAGIDsArray.add(lieu.getId());
                        lieuxTAGNamesArray.add(lieu.getName());


                        // Ajouter dans le adapter du recyclerview
                        doRefreshAdapter(new EventlieuRetrofit(lieu.getId(), lieu.getName()));



                    }
                }

                for (EventLocalRetrofit fete:
                        event.getEventlocal()) {
                    if (!feteTAGIDsArray.contains(fete.getId())) {

                        feteTAGIDsArray.add(fete.getId());
                        feteTAGNamesArray.add(fete.getName());

                        // Ajouter dans le adapter du recyclerview
                        doRefreshAdapter(fete);

                    }
                }

                if (null!=event.getEventFlyers()) {
                    for (EventFlyersRetrofit item :
                            event.getEventFlyers()) {
                        if (!flyersTAGIDsArray.contains(item.getId())) {

                            flyersTAGIDsArray.add(item.getId());
                            flyersTAGImagesArray.add(item.getImage());

                            // Ajouter dans le adapter du recyclerview
                            doRefreshAdapter(item);

                        }
                    }
                }

                // Enable
                spinnEntreetype.setEnabled(true);
                spinnThematique.setEnabled(true);






            }

            @Override
            public void onFailure(Call<EventRetrofit> call, Throwable t) {
                Log.d(Config.TAGKEY,  " - STEP - getEventServerDetail - onFailure - message : "+t.getMessage()+" - URLS : "+call.request().url() );
                // checkRealmAndFill();
            }
        });

    }

    private void doFillUiWithInstancedState() {

        doSetDefaultValues();

        Log.d(Config.TAGKEY,  " - doFillUiWithInstancedState - saveFieldname - saveFieldname: "+saveFieldname);
        Log.d(Config.TAGKEY,  " - doFillUiWithInstancedState - saveFieldname - saveSpinnThematique: "+saveSpinnThematique);
        Log.d(Config.TAGKEY,  " - doFillUiWithInstancedState - saveFieldname - spinnAdapter.getCount() : "+spinnAdapter.getCount());
        Log.d(Config.TAGKEY,  " - doFillUiWithInstancedState - saveFieldname - spinnAdapter.toString() : "+spinnAdapter.getItemByPosition(0).getName());
//        Log.d(Config.TAGKEY,  " - doFillUiWithInstancedState - saveFieldname - spinnAdapter.toString() : "+spinnAdapter.getItemByPosition(1));
        if (!TextUtils.isEmpty(saveSpinnThematique) && spinnAdapter.getCount()>0)
            Log.d(Config.TAGKEY,  " - doFillUiWithInstancedState - saveFieldname - saveSpinnThematique position: "+spinnAdapter.getPositionByItem(Integer.parseInt(saveSpinnThematique)));
        Log.d(Config.TAGKEY,  " - doFillUiWithInstancedState - saveFieldname - saveSpinnEntreetype:  "+saveSpinnEntreetype);
        if (!TextUtils.isEmpty(saveSpinnEntreetype) && spinnEntreetypeAdapter.getCount()>0)
            Log.d(Config.TAGKEY,  " - doFillUiWithInstancedState - saveFieldname - saveSpinnEntreetype position  "+spinnEntreetypeAdapter.getPositionByItem(Integer.parseInt(saveSpinnEntreetype)));

        if (!TextUtils.isEmpty(saveFieldname))
            editName.setText(saveFieldname);

        if (!TextUtils.isEmpty(saveEditPrice))
            editPrice.setText(saveEditPrice);

        /*if (!TextUtils.isEmpty(saveTxtDateEvent))
            txtDateEvent.setText(saveTxtDateEvent);*/

        if (!TextUtils.isEmpty(saveTxtDateEvent))
            txtdateSelected.setText(getTxtDateEvent());

        if (!TextUtils.isEmpty(saveHiddenFieldImageView))
            hiddenFieldImageView.setText(saveHiddenFieldImageView);

        if (!TextUtils.isEmpty(saveHiddenFieldDate))
            hiddenFieldDate.setText(saveHiddenFieldDate);

        if (!TextUtils.isEmpty(saveHiddenFieldOptDate))
            hiddenFieldOptDate.setText(saveHiddenFieldOptDate);

        swHidden.setChecked(saveSwHidden);
        swCancelled.setChecked(saveSwCancelled);

        if (!TextUtils.isEmpty(saveSpinnThematique) && spinnAdapter.getCount()>1) {
            spinnThematique.setSelection(spinnAdapter.getPositionByItem(Integer.parseInt(saveSpinnThematique)));
        }

        if (!TextUtils.isEmpty(saveSpinnEntreetype) && spinnEntreetypeAdapter.getCount()>1) {
            spinnEntreetype.setSelection(spinnEntreetypeAdapter.getPositionByItem(Integer.parseInt(saveSpinnEntreetype)));
        }

        fillImageView();





    }

    private String getTxtDateEvent() {
        if (TextUtils.isEmpty(saveTxtDateEvent) || TextUtils.isEmpty(saveHiddenFieldOptDate))
            return "";

        // Selon le type de la date
        Log.d(Config.TAGKEY," getTxtDateEvent  "+saveHiddenFieldOptDate);

        switch (saveHiddenFieldOptDate) {
            case "multiplesdate":
                return getTxtDateEventMultiplesDate();
            case "rangedate":
                return getTxtDateEventRangeDate();
            default:
                return saveTxtDateEvent;
        }
    }

    private String getTxtDateEventRangeDate() {
        // 2017-07-20 12:00; 2017-07-22 12:00; 2017-07-25 12:00;
        if (TextUtils.isEmpty(saveTxtDateEvent))
            return "";

        String[] sl = TextUtils.split(saveTxtDateEvent,"au");

        String dateDebut = sl[0];
        String[] arrDd = TextUtils.split(dateDebut.trim()," ");
        dateDebut = arrDd[0].trim();

        String[] slDd = TextUtils.split(dateDebut,"-");

        String dateFin = sl[1];
        String[] arrDf = TextUtils.split(dateFin.trim()," ");
        dateFin = arrDf[0].trim();

        String[] slDf = TextUtils.split(dateFin,"-");



        int d1 = Integer.parseInt(slDd[2].trim());
        int m1 = Integer.parseInt(slDd[1].trim());

        int d2 = Integer.parseInt(slDf[2].trim());
        int m2 = Integer.parseInt(slDf[1].trim());
        int y2 = Integer.parseInt(slDf[0].trim());

        String datechoosen =  d1 + "-" + m1 ;
        datechoosen +=  " au ";
        datechoosen +=  d2 + "-" + m2 + "-" + y2;

        return datechoosen;
    }

    private String getTxtDateEventMultiplesDate() {
        // 2017-07-20 12:00; 2017-07-22 12:00; 2017-07-25 12:00;
        if (TextUtils.isEmpty(saveTxtDateEvent))
            return "";

        String[] sl = TextUtils.split(saveTxtDateEvent,";");
        if (sl.length<=0)
            return " ";

        ArrayList<String> ds = new ArrayList<String>();

        int nb = 0;
        for(String date: sl) {

            date = date.trim();
            String[] arrDf = TextUtils.split(date.trim()," ");
            String eachDate = arrDf[0].trim();


            String[] arrD = TextUtils.split(eachDate,"-");

            if (arrD.length!=3)
                return "";

            int d = Integer.parseInt(arrD[2].trim());
            int m = Integer.parseInt(arrD[1].trim());
            int y = Integer.parseInt(arrD[0].trim());

            String showDate = (d<10?"0"+d:d)+"-"+(m<10?"0"+m:m) ;

            if (!ds.contains(showDate) && nb<4)
                ds.add(showDate);



            nb++;



        }
        return TextUtils.join(", ", ds)+(nb>4 ? "..." : "");
    }

    private void doSetUi() {
        // Click image
        doSetOnClickImage();

        // Onclick button
        doClickUploadFlyer();

        // Fill Spinner thematique
        doFillSpinnerThematique();

        // Fill Spinner Prix
        doFillSpinnerEntreetype();

        // OnClick txtDateEvent
//        doClickDate();

        // OnClick btnDateEventUnique
        doClickDateUnique();

        // OnClick btnDateEventRange
        doClickDateRange();

        // OnClick btnDateEventMultiple
        doClickDateMultiple();

        // Autocomplete editLieu
        doFillAutoCompleteEditLieu();

        // Autocomplete editArtists
        doFillAutoCompleteEditArtists();

        // Autocomplete editFete
        doFillAutoCompleteEditFeteLocal();

        // Autocomplete editSearchEvent
        doFillAutoCompleteEditSearchEvent();


        fillImageView();

        // Clic save event
        doClickSaveEvent();

        // Click sur btnSearchEvent
//        doSetOnClickBtnSearchEvent();

        // Clic ajout
        doSetOnClickBtnAddLieux();
        doSetOnClickBtnAddArtists();
        doSetOnClickBtnAddFete();

        // recycler
        doSetRecyclerViewLieux();
        doSetRecyclerViewArtists();
        doSetRecyclerViewFetes();
        doSetRecyclerViewFlyers();

    }

    private void doFillAutoCompleteEditSearchEvent() {
        editSearchEvent.setAdapter(autocompleteSearchEvent);

        // Au clic sur un item dans la liste proposee
        doOnsetItemClickEditSearchEvent();

        // A la saisie d'un texte
        doOnsetTextChangedListenerSearchEvent();
    }


    private void doSetRecyclerViewFetes() {
        recyclerviewFetes.setLayoutManager( new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        adapterFetes = new FeteRecyclerviewAdapter(getActivity(), fetesLists, R.layout.adapter_fetes);
        recyclerviewFetes.setAdapter(adapterFetes);

        // Onclick
        recyclerviewFetes.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerviewFetes, new ClickListener() {
            @Override
            public void onClick(View view,final int position) {
                // Utils.showSnackLong(coordinatorLayout,"Clic sur item");
                LinearLayout fond = (LinearLayout) view.findViewById(R.id.fond);
                // Clic sur Fermer
                IconTextView iconDelete = (IconTextView) view.findViewById(R.id.iconDeleteT);
                if (null==iconDelete)
                    return;

                iconDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // Utils.showSnackLong(coordinatorLayout,"Clic sur close");
                        doRemoveTagFetesByPosition(position);
                    }
                });

                if (null!=fond) {
                    fond.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Utils.showSnackLong(coordinatorLayout,"Clic sur fond");
                            int itemId = fetesLists.get(position).getId();
                            doOpenPopupActivity("fete", itemId);
                        }
                    });
                }
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    private void doSetRecyclerViewArtists() {
        recyclerviewArtists.setLayoutManager( new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        adapterArtists = new ArtistsRecyclerviewAdapter(getActivity(), artistsLists, R.layout.adapter_artists);
        recyclerviewArtists.setAdapter(adapterArtists);

        // Onclick
        recyclerviewArtists.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerviewArtists, new ClickListener() {
            @Override
            public void onClick(View view,final int position) {
                // Utils.showSnackLong(coordinatorLayout,"Clic sur item");


                LinearLayout fond = (LinearLayout) view.findViewById(R.id.fond);

                IconTextView iconDelete = (IconTextView) view.findViewById(iconDeleteT);
                if (null==iconDelete )
                    return;

                if (null!=iconDelete) {
                    iconDelete.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Utils.showSnackLong(coordinatorLayout,"Clic sur close");
                            doRemoveTagArtistByPosition(position);
                        }
                    });
                }

                if (null!=fond) {
                    fond.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Utils.showSnackLong(coordinatorLayout,"Clic sur fond");
                            int itemId = artistsLists.get(position).getId();
                            Log.d(Config.TAGKEY,   " - doSetRecyclerViewArtists - itemId: "+itemId);
                            doOpenPopupActivity("artiste", itemId);
                        }
                    });
                }


            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    private void doOpenPopupActivity(String fragment, int itemId) {
        Intent i = new Intent(getActivity(), PopupActivity.class);
        i.putExtra("fragment", fragment );
        i.putExtra("itemId", itemId );
        getActivity().startActivity(i);
        getActivity().overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
    }

    private void doOpenPopupActivity(String fragment, String artiste ) {
        Intent i = new Intent(getActivity(), PopupActivity.class);
        i.putExtra("fragment", fragment );
        i.putExtra("artisteName", artiste );
        getActivity().startActivityForResult(i,200);
        getActivity().overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
    }

    private void doSetRecyclerViewLieux() {
        recyclerviewLieux.setLayoutManager( new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        adapterLieux = new LieuxRecyclerviewAdapter(getActivity(), lieuxLists, R.layout.adapter_lieux);
        recyclerviewLieux.setAdapter(adapterLieux);

        // Onclick
        recyclerviewLieux.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerviewLieux, new ClickListener() {
            @Override
            public void onClick(View view,final int position) {
                // Utils.showSnackLong(coordinatorLayout,"Clic sur item");

                // Clic sur Fermer
                IconTextView iconDeleteT = (IconTextView) view.findViewById(R.id.iconDeleteT);
                LinearLayout fond = (LinearLayout) view.findViewById(R.id.fond);
                if ( null==iconDeleteT)
                    return;


                if (null!=iconDeleteT ) {
                    iconDeleteT.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Utils.showSnackLong(coordinatorLayout,"Clic sur close");
                            doRemoveTagLieuxByPosition(position);
                        }
                    });
                }

                if (null!=fond) {
                    fond.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Utils.showSnackLong(coordinatorLayout,"Clic sur fond");
                            int itemId = lieuxLists.get(position).getId();
                            doOpenPopupActivity("lieux", itemId);
                        }
                    });
                }
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }
    private void doSetRecyclerViewFlyers() {
        recyclerviewFlyers.setLayoutManager( new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        adapterFlyers = new FlyersRecyclerviewAdapter(getActivity(), flyersLists, R.layout.adapter_flyers);
        recyclerviewFlyers.setAdapter(adapterFlyers);

        // Onclick
        recyclerviewFlyers.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerviewFlyers, new ClickListener() {
            @Override
            public void onClick(View view,final int position) {

                EventFlyersRetrofit eventFlyersRetrofit = flyersLists.get(position);
                boolean isUri = false;
                boolean isTmp = false;

                if (null!=eventFlyersRetrofit.getUri())
                    isUri = eventFlyersRetrofit.getUri();

                if (null!=eventFlyersRetrofit.getTmp())
                    isTmp = eventFlyersRetrofit.getTmp();

                // On peut supprimer sans boite de dialog
                final boolean canRemoteWithoutAlert = (isTmp || isUri);

                Log.d(Config.TAGKEY,   " - recyclerviewFlyers.addOnItemTouchListener - position - "+position+" - size : "+flyersLists.size()+" - isUri : "+isUri+" - isTmp : "+isTmp);

                // Utils.showSnackLong(coordinatorLayout,"Clic sur item");

                // Clic sur Fermer
                final IconButton iconDeleteT = (IconButton) view.findViewById(R.id.iconDeleteT);
                final IconButton iconUpload = (IconButton) view.findViewById(R.id.iconUpload);
                final IconButton iconSetMainFlyer = (IconButton) view.findViewById(R.id.iconSetMainFlyer);
                final ProgressBar progressbar = (ProgressBar) view.findViewById(R.id.progressbar);
                final ImageView image = (ImageView) view.findViewById(R.id.image);

                if ( null==iconDeleteT)
                    return;


                if (null!=iconDeleteT ) {
                    iconDeleteT.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (canRemoteWithoutAlert)
                                doRemoveFlyersByPosition(position);
                            else {
                                // Utils.showSnackLong(coordinatorLayout,"Boite de dialog Alerte");
                                doSetConfirmDeleteFlyer(position);
                            }
                        }
                    });
                }

                if (null!=iconUpload ) {
                    iconUpload.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Utils.showSnackLong(coordinatorLayout,"Clic sur iconUpload");
                            doUploadFlyer(position, flyersLists.get(position));
                            iconUpload.setEnabled(false);
                            iconDeleteT.setEnabled(false);
                            if (null!=progressbar)
                                progressbar.setVisibility(View.VISIBLE);

                        }
                    });
                }

                if (null!=iconSetMainFlyer ) {
                    iconSetMainFlyer.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            //Utils.showSnackLong(coordinatorLayout," Clic setMain flyer");
                            doSetConfirmSetMainFlyer(position);
                        }
                    });
                }

                if (null!=image ) {
                    image.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
//                            Utils.showSnackLong(coordinatorLayout," Clic setMain flyer");
                            String tmpRem = null;
                            Uri tmpUri = null;

                            if (null!=flyersLists.get(position).getmImageUri()) {
                                tmpUri = flyersLists.get(position).getmImageUri();
                                Log.d(Config.TAGKEY, " image.setOnClickListener tmpUri "+tmpUri.toString() );
                            }

                            if (null!=flyersLists.get(position).getImage() && null==tmpUri) {
                                tmpRem = Config.PATH_FLYERS + flyersLists.get(position).getImage();
                                Log.d(Config.TAGKEY, " image.setOnClickListener tmpRem "+tmpRem );
                            }

                            Utils.doOpenFullscreenImage(tmpUri,tmpRem, getActivity());
                        }
                    });
                }
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }




    private void doSetOnClickImage() {
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String tmpRemote = null;
                if (!TextUtils.isEmpty(saveHiddenFieldImageView)) {
                    tmpRemote = Config.PATH_FLYERS+saveHiddenFieldImageView;
                }

                Utils.doOpenFullscreenImage(mimageUri,tmpRemote, getActivity());

            }
        });
    }



    private void doSetOnClickBtnAddLieux() {
        btnNoDataAddLieu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doClickBtnAddLieux();
            }
        });
    }



    private void doSetOnClickBtnAddArtists() {
        btnNoDataAddArtists.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doClickBtnAddArtists();
            }
        });
    }
    private void doSetOnClickBtnAddFete() {
        btnNoDataAddFete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doClickBtnAddFete();
            }
        });
    }



    private void doClickBtnAddLieux() {
        // Ouvrir boite de recherche
        final Dialog d = new Dialog(getActivity());
        d.setContentView(R.layout.dialog_add_generic);

        final EditText textView_name = (EditText) d.findViewById(R.id.edittext_name);
        final ProgressBar progressBarSave = (ProgressBar) d.findViewById(R.id.progressBarSave);


        final IconButton btnSaveGeneric = (IconButton) d.findViewById(R.id.btnSaveGeneric);
        btnSaveGeneric.setEnabled(false);

        if (!TextUtils.isEmpty( editLieu.getText().toString())) {
            String s = editLieu.getText().toString();
            textView_name.setText(s);
            btnSaveGeneric.setEnabled(true);
        }

        // On text change
        textView_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {
                if( s.length()>0 ) {
                    btnSaveGeneric.setEnabled(true);
                } else
                    btnSaveGeneric.setEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        btnSaveGeneric.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doSaveServerLieu(d, editLieu.getText().toString());
            }
        });

        d.show();
    }



    private void doClickBtnAddArtists() {
        Log.d(Config.TAGKEY,   " - doClickBtnAddArtists " );
        String artisteName = null;
        if (!TextUtils.isEmpty( editArtist.getText().toString())) {
            artisteName = editArtist.getText().toString();
        }
        doOpenPopupActivity("artiste", artisteName);
    }
    private void OLD__doClickBtnAddArtists() {
        // Ouvrir boite de recherche
        final Dialog d = new Dialog(getActivity());
        d.setContentView(R.layout.dialog_add_artists);

        final EditText textView_name = (EditText) d.findViewById(edittext_artiste);
        final ProgressBar progressBarSave = (ProgressBar) d.findViewById(R.id.progressBarSave);
        final Spinner spinnType = (Spinner) d.findViewById(R.id.spinnType);




        SpinArtistetypeAdapter spinnAdapter = new SpinArtistetypeAdapter( getActivity() , Utils.getGenericArtisteType() );
        spinnType.setAdapter(spinnAdapter);


        final IconButton btnSaveGeneric = (IconButton) d.findViewById(R.id.btnSaveGeneric);
        btnSaveGeneric.setEnabled(false);

        if (!TextUtils.isEmpty( editArtist.getText().toString())) {
            String s = editArtist.getText().toString();
            textView_name.setText(s);
            btnSaveGeneric.setEnabled(true);
        }

        // On text change
        textView_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {
                if( s.length()>0 ) {
                    btnSaveGeneric.setEnabled(true);
                } else
                    btnSaveGeneric.setEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        btnSaveGeneric.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(Config.TAGKEY,   " - saveArtists - "+spinnType.getSelectedItem().toString());
                String sType = "";
                switch(spinnType.getSelectedItem().toString()) {
                    case "1":
                    default:
                        sType = "artiste";
                        break;
                    case "2":
                        sType = "dj";
                        break;
                    case "3":
                        sType = "organisateur";
                        break;
                }
                doSaveServerArtists(d, textView_name.getText().toString(), sType);
            }
        });

        d.show();
    }



    private void doClickBtnAddFete() {
        // Ouvrir boite de recherche
        final Dialog d = new Dialog(getActivity());
        d.setContentView(R.layout.dialog_add_generic);

        final EditText textView_name = (EditText) d.findViewById(R.id.edittext_name);
        final ProgressBar progressBarSave = (ProgressBar) d.findViewById(R.id.progressBarSave);


        final IconButton btnSaveGeneric = (IconButton) d.findViewById(R.id.btnSaveGeneric);
        btnSaveGeneric.setEnabled(false);

        if (!TextUtils.isEmpty( editFete.getText().toString())) {
            String s = editFete.getText().toString();
            textView_name.setText(s);
            btnSaveGeneric.setEnabled(true);
        }

        // On text change
        textView_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {
                if( s.length()>0 ) {
                    btnSaveGeneric.setEnabled(true);
                } else
                    btnSaveGeneric.setEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        btnSaveGeneric.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doSaveServerFete(d, editFete.getText().toString());
            }
        });

        d.show();
    }

    private void doSaveServerArtists(final Dialog d, String name, String type) {

        final IconButton icb = (IconButton) d.findViewById(R.id.btnSaveGeneric);
        final ProgressBar pb = (ProgressBar) d.findViewById(R.id.progressBarSave);
        final EditText edt = (EditText) d.findViewById(edittext_artiste);

        icb.setEnabled(false);
        edt.setEnabled(false);
        icb.setText(getString(R.string.btnSaveGenericLoad));
        pb.setVisibility(View.VISIBLE);


        Call<GenericResultRetrofit> call = apiService.createArtists(name, type);
        call.enqueue(new Callback<GenericResultRetrofit>() {
            @Override
            public void onResponse(Call<GenericResultRetrofit> call, Response<GenericResultRetrofit> response) {
                Log.d(Config.TAGKEY,   " - uploadFlyer - onResponse - " + response.code() + " - " + response.message() + " - URLS : " + call.request().url());

                if (200!=response.code()) {
                    icb.setEnabled(true);
                    return;
                }

                doAfterSaveServerArtists(d, response.body());
            }

            @Override
            public void onFailure(Call<GenericResultRetrofit> call, Throwable t) {
                Log.d(Config.TAGKEY,   " - uploadFlyer - onFailure - URLS : " + call.request().url()+" - t "+t.getMessage());
                Utils.showSnackLong(coordinatorLayout, getString(R.string.txtErrorServer));
                icb.setEnabled(true);
            }
        });

    }



    private void doSaveServerLieu(final Dialog d, String name) {


        final IconButton icb = (IconButton) d.findViewById(R.id.btnSaveGeneric);
        final ProgressBar pb = (ProgressBar) d.findViewById(R.id.progressBarSave);
        final EditText edt = (EditText) d.findViewById(R.id.edittext_name);

        icb.setEnabled(false);
        edt.setEnabled(false);
        icb.setText(getString(R.string.btnSaveGenericLoad));
        pb.setVisibility(View.VISIBLE);


        Call<GenericResultRetrofit> call = apiService.createLieu(name);
        call.enqueue(new Callback<GenericResultRetrofit>() {
            @Override
            public void onResponse(Call<GenericResultRetrofit> call, Response<GenericResultRetrofit> response) {
                Log.d(Config.TAGKEY,   " - uploadFlyer - onResponse - " + response.code() + " - " + response.message() + " - URLS : " + call.request().url());

                if (200!=response.code()) {
                    icb.setEnabled(true);
                    return;
                }

                doAfterSaveServerLieu(d, response.body());
            }

            @Override
            public void onFailure(Call<GenericResultRetrofit> call, Throwable t) {
                Log.d(Config.TAGKEY,   " - uploadFlyer - onFailure - URLS : " + call.request().url()+" - t "+t.getMessage());
                Utils.showSnackLong(coordinatorLayout, getString(R.string.txtErrorServer));
                icb.setEnabled(true);
            }
        });

    }



    private void doSaveServerFete(final Dialog d, String name) {

        final IconButton icb = (IconButton) d.findViewById(R.id.btnSaveGeneric);
        final ProgressBar pb = (ProgressBar) d.findViewById(R.id.progressBarSave);
        final EditText edt = (EditText) d.findViewById(R.id.edittext_name);

        icb.setEnabled(false);
        edt.setEnabled(false);
        icb.setText(getString(R.string.btnSaveGenericLoad));
        pb.setVisibility(View.VISIBLE);


        Call<GenericResultRetrofit> call = apiService.createLocal(
               new EventLocalRetrofit(name)
        );
        call.enqueue(new Callback<GenericResultRetrofit>() {
            @Override
            public void onResponse(Call<GenericResultRetrofit> call, Response<GenericResultRetrofit> response) {
                Log.d(Config.TAGKEY,   " - uploadFlyer - onResponse - " + response.code() + " - " + response.message() + " - URLS : " + call.request().url());

                if (200!=response.code()) {
                    icb.setEnabled(true);
                    return;
                }

                doAfterSaveServerFete(d, response.body());
            }

            @Override
            public void onFailure(Call<GenericResultRetrofit> call, Throwable t) {
                Log.d(Config.TAGKEY,   " - uploadFlyer - onFailure - URLS : " + call.request().url()+" - t "+t.getMessage());
                Utils.showSnackLong(coordinatorLayout, getString(R.string.txtErrorServer));
                icb.setEnabled(true);
            }
        });
    }

    private void doAfterSaveServerArtists(final Dialog d, GenericResultRetrofit body) {

        String error = body.getError();
        String success = body.getSuccess();
        String name = body.getName();
        int id = body.getId();


        Log.d(Config.TAGKEY,   " - doAfterSaveServer -  getError: " + error+" - getSuccess: "+success  );

        if (error!=null) {
            return;
        }

        final IconButton btn = (IconButton) d.findViewById(R.id.btnSaveGeneric);
        final ProgressBar pb = (ProgressBar) d.findViewById(R.id.progressBarSave);
        final EditText edt = (EditText) d.findViewById(edittext_artiste);

        // Cacher le bouton
        btn.setVisibility(View.GONE);
        edt.setEnabled(false);
        btn.setText(getString(R.string.btnSaveGenericOk));
        pb.setVisibility(View.GONE);

        Utils.showSnackLong(coordinatorLayout, getString(R.string.txtConfirmEntitySaved));


        EventArtistesDjOrganisateurRetrofit item = new EventArtistesDjOrganisateurRetrofit();
        if (!artistsTAGNamesArray.contains(id)) {

            artistsTAGIDsArray.add(id);
            artistsTAGNamesArray.add(name);

            item.setId(id);
            item.setName(name);

            // Ajouter dans le adapter du recyclerview
            doRefreshAdapter(item);

            d.dismiss();

            relativeNoDataArtists.setVisibility(View.GONE);

            editArtist.setText("");


        } else {
            Utils.showSnackLong(coordinatorLayout, getString(R.string.txtErrorArtistsExistsIntag));
            // Toast.makeText(TsaraApplication.getContext(),TsaraApplication.getContext().getString(R.string.txtv_error_artists_exists_intag),Toast.LENGTH_LONG).show();
        }
    }

    private void doAfterSaveServerLieu(final Dialog d, GenericResultRetrofit body) {
        String error = body.getError();
        String success = body.getSuccess();
        String name = body.getName();
        int id = body.getId();


        Log.d(Config.TAGKEY,   " - doAfterSaveServer -  getError: " + error+" - getSuccess: "+success  );

        if (error!=null) {
            return;
        }

        final IconButton btn = (IconButton) d.findViewById(R.id.btnSaveGeneric);
        final ProgressBar pb = (ProgressBar) d.findViewById(R.id.progressBarSave);
        final EditText edt = (EditText) d.findViewById(R.id.edittext_name);

        // Cacher le bouton
        btn.setVisibility(View.GONE);
        edt.setEnabled(false);
        btn.setText(getString(R.string.btnSaveGenericOk));
        pb.setVisibility(View.GONE);

        Utils.showSnackLong(coordinatorLayout, getString(R.string.txtConfirmEntitySaved));


        EventlieuRetrofit item = new EventlieuRetrofit();
        if (!lieuxTAGIDsArray.contains(id)) {

            lieuxTAGIDsArray.add(id);
            lieuxTAGNamesArray.add(name);

            item.setId(id);
            item.setName(name);


            // Ajouter dans le adapter du recyclerview
            doRefreshAdapter(item);

            d.dismiss();

            relativeNoDataLieux.setVisibility(View.GONE);

            editLieu.setText("");


        } else {
            Utils.showSnackLong(coordinatorLayout, getString(R.string.txtErrorLieuExistsIntag));
            // Toast.makeText(TsaraApplication.getContext(),TsaraApplication.getContext().getString(R.string.txtv_error_artists_exists_intag),Toast.LENGTH_LONG).show();
        }
    }

    private void doAfterSaveServerFete(final Dialog d, GenericResultRetrofit body) {
        String error = body.getError();
        String success = body.getSuccess();
        String name = body.getName();
        int id = body.getId();


        Log.d(Config.TAGKEY,   " - doAfterSaveServer -  getError: " + error+" - getSuccess: "+success  );

        if (error!=null) {
            return;
        }

        final IconButton btn = (IconButton) d.findViewById(R.id.btnSaveGeneric);
        final ProgressBar pb = (ProgressBar) d.findViewById(R.id.progressBarSave);
        final EditText edt = (EditText) d.findViewById(R.id.edittext_name);

        // Cacher le bouton
        btn.setVisibility(View.GONE);
        edt.setEnabled(false);
        btn.setText(getString(R.string.btnSaveGenericOk));
        pb.setVisibility(View.GONE);

        Utils.showSnackLong(coordinatorLayout, getString(R.string.txtConfirmEntitySaved));


        EventLocalRetrofit item = new EventLocalRetrofit();
        if (!feteTAGIDsArray.contains(id)) {

            feteTAGIDsArray.add(id);
            feteTAGNamesArray.add(name);

            item.setId(id);
            item.setName(name);


            d.dismiss();

            relativeNoDataFete.setVisibility(View.GONE);

            editFete.setText("");


        } else {
            Utils.showSnackLong(coordinatorLayout, getString(R.string.txtErrorFeteExistsIntag));
            // Toast.makeText(TsaraApplication.getContext(),TsaraApplication.getContext().getString(R.string.txtv_error_artists_exists_intag),Toast.LENGTH_LONG).show();
        }


    }



    private void doSetConfirmUpdateEvent(final EventRetrofit item) {

        Log.d(Config.TAGKEY," - item.getName(): "+item.getName());

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getString(R.string.txtDialogHeaderUpdateEvent));
        builder.setMessage(getString(R.string.txtAskUpdateEvent));
        builder.setNegativeButton(getString(R.string.txtNo  ),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int which) {

                    }
                });
        builder.setPositiveButton(getString(R.string.txtYes),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int which) {
                        doOpenEditActivity(item);

                    }
                });
        builder.show();

    }

    private void doOpenEditActivity(EventRetrofit item) {

        Intent i = new Intent(getActivity(), EditeventActivity.class );
        i.putExtra("eventId", item.getId() );
        i.putExtra("mimageUri", mimageUri );
        i.putExtra("fieldImageView", hiddenFieldImageView.getText().toString() );
        getActivity().startActivity(i);

    }

    private void hideNodata(Dialog d) {
        TextView textView_nodata = (TextView) d.findViewById(R.id.textView_nodata);
        textView_nodata.setText("");
        textView_nodata.setVisibility(View.GONE);
    }





    private void doRemoveTagFetesByPosition(int position) {
        feteTAGNamesArray.remove(position);
        feteTAGIDsArray.remove(position);
        adapterFetes.removeAt(position);
    }




    private void doRemoveTagLieuxByPosition(int position) {
        lieuxTAGIDsArray.remove(position);
        lieuxTAGNamesArray.remove(position);
        adapterLieux.removeAt(position);
    }
    private void doRemoveFlyersByPosition(int position) {
        flyersTAGIDsArray.remove(position);
        flyersTAGImagesArray.remove(position);
        adapterFlyers.removeAt(position);
    }



    private void doClickSaveEvent() {
        btnSaveEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doValidform();
            }
        });
    }

    private void doValidform() {
        // Nom event rempli?
        String fieldName = editName.getText().toString();
        fieldName = getFieldReplaced(fieldName);

        final String tmpFieldName = fieldName;
        editName.setText(tmpFieldName);

        // String fieldThematique = spinnThematique.getSelectedItem().toString();
        int selectedThematique = Integer.parseInt(spinnThematique.getSelectedItem().toString());
        final int fieldThematique = spinnAdapter.getItemByPosition(selectedThematique).getId();

        int selectedAcces = Integer.parseInt(spinnEntreetype.getSelectedItem().toString());
        final int fieldAcces = spinnEntreetypeAdapter.getItemByPosition(selectedAcces).getId();

        String fieldPrice = editPrice.getText().toString();
        final String fieldDate = hiddenFieldDate.getText().toString();
        final String fieldOptDate = hiddenFieldOptDate.getText().toString();
        String fieldImage = hiddenFieldImageView.getText().toString();
        String fieldLieux = TextUtils.join(",", lieuxTAGIDsArray );
        String fieldArtists = TextUtils.join(",", artistsTAGIDsArray );
        String fieldFete = TextUtils.join(",", feteTAGIDsArray );
        int fieldSwHidden = (swHidden.isChecked()?1:0);
        int fieldSwCancelled = (swCancelled.isChecked()?1:0);



        // Les champs obligatoires sont remplies ? fieldName, fieldThematique, fiedAccess, fieldDate
        int nbFieldInvalid = 0;
        nbFieldInvalid += TextUtils.isEmpty(tmpFieldName)?1:0;
        nbFieldInvalid += fieldThematique<=0?1:0;
        nbFieldInvalid += fieldAcces<=0?1:0;
        nbFieldInvalid += TextUtils.isEmpty(fieldDate)?1:0;

        Log.d(Config.TAGKEY,"FORM begin");
        Log.d(Config.TAGKEY,"FORM fieldName : "+tmpFieldName);
        Log.d(Config.TAGKEY,"FORM fieldThematique : "+fieldThematique);
        Log.d(Config.TAGKEY,"FORM fieldAcces : "+fieldAcces);
        Log.d(Config.TAGKEY,"FORM fieldPrice : "+fieldPrice);
        Log.d(Config.TAGKEY,"FORM fieldDate : "+fieldDate);
        Log.d(Config.TAGKEY,"FORM fieldOptDate : "+fieldOptDate);
        Log.d(Config.TAGKEY,"FORM mimageUri : "+mimageUri.toString());
        Log.d(Config.TAGKEY,"FORM fieldImage : "+fieldImage);
        Log.d(Config.TAGKEY,"FORM fieldLieux : "+fieldLieux);
        Log.d(Config.TAGKEY,"FORM fieldArtists : "+fieldArtists);
        Log.d(Config.TAGKEY,"FORM fieldFete : "+fieldFete);
        Log.d(Config.TAGKEY,"FORM fieldSwHidden : "+fieldSwHidden);
        Log.d(Config.TAGKEY,"FORM fieldSwCancelled : "+fieldSwCancelled);
        Log.d(Config.TAGKEY,"FORM nbFieldInvalid : "+nbFieldInvalid);
        Log.d(Config.TAGKEY,"FORM eventId : "+eventId);
        Log.d(Config.TAGKEY,"FORM end");

        // Si image existant mais non encore uploadé sur le serveur
        if (TextUtils.isEmpty(fieldImage) && !TextUtils.isEmpty(mimageUri.toString())) {
            doSetConfirmUpload(nbFieldInvalid);
            return;
        }

        // Des champs ne sont pas remplis
        if (nbFieldInvalid>0) {
            final int finalNbFieldInvalid = nbFieldInvalid;
            Utils.showSnackLong(coordinatorLayout, getString(R.string.txtErrorFieldsEmpty),getString(R.string.txtMore), new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d(Config.TAGKEY, " - showSnackLong - OnClickListener ");
                    ArrayList<String> errorFields = new ArrayList<String>();
                    if (TextUtils.isEmpty(tmpFieldName)) {
                        errorFields.add("Titre");
                    }
                    if (fieldThematique<=0) {
                        errorFields.add("Thematique");
                    }
                    if (fieldAcces<=0) {
                        errorFields.add("Accès");
                    }
                    if (TextUtils.isEmpty(fieldDate)) {
                        errorFields.add("Date");
                    }
                    doOpenDialogError(finalNbFieldInvalid,errorFields);
                }
            });
            return;
        }

        // Continuer si erreur vide

        doSaveServer(tmpFieldName,fieldThematique ,fieldAcces ,fieldPrice,fieldDate,fieldImage,fieldLieux,fieldArtists,fieldFete,fieldSwHidden ,fieldSwCancelled, fieldOptDate );



    }

    private String getFieldReplaced(String fieldName) {

        String artistes = null;
        String lieux = null;
        String fete = null;

        if (artistsTAGNamesArray.size()==2) {
            artistes = TextUtils.join(" & ", artistsTAGNamesArray);
        } else if (artistsTAGNamesArray.size()>2) {
            artistes = TextUtils.join(", ", artistsTAGNamesArray);
        } else {
            artistes = TextUtils.join("", artistsTAGNamesArray);
        }

        if (lieuxTAGNamesArray.size()>1) {
            lieux = TextUtils.join(" - ", lieuxTAGNamesArray);
        } else {
            lieux = TextUtils.join("", lieuxTAGNamesArray);
        }

        if (feteTAGNamesArray.size()>1) {
            fete = TextUtils.join(" - ", feteTAGNamesArray);
        } else {
            fete = TextUtils.join("", feteTAGNamesArray);
        }

        // Remplacer un par un chaque artiste
        if (artistsTAGNamesArray.size()>0) {
            int i = 1;
            for (String item: artistsTAGNamesArray) {
                fieldName = fieldName.replace("@a"+i, item);
                i++;
            }
        }

        if (lieuxTAGNamesArray.size()>0) {
            int i = 1;
            for (String item: lieuxTAGNamesArray) {
                fieldName = fieldName.replace("#l"+i, item);
                i++;
            }
        }

        if (feteTAGNamesArray.size()>0) {
            int i = 1;
            for (String item: lieuxTAGNamesArray) {
                fieldName = fieldName.replace("~f"+i, item);
                i++;
            }
        }

        // Remplacer la totalite
        fieldName = fieldName.replace("#l", lieux);
        fieldName = fieldName.replace("@a", artistes);
        fieldName = fieldName.replace("~f", fete);



        return fieldName;
    }

    private void doSetConfirmUpload(final int nbFieldInvalid) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getString(R.string.txtDialogHeaderFlyer));
        builder.setMessage(getString(R.string.txtAskUploadFlyer));
        builder.setNegativeButton(getString(R.string.txtNo  ),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int which) {
                        // Valider le form si aucune erruer
                        if (nbFieldInvalid==0) {
                            doValidform();
                        }
                    }
                });
        builder.setPositiveButton(getString(R.string.txtYes),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int which) {
                        saveAfterUploadFlyer = true;
                        doUploadFlyer();

                    }
                });
        builder.show();
    }

    private void doSetConfirmCloseActivity() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getString(R.string.txtDialogHeaderClose));
        builder.setMessage(getString(R.string.txtAskConfirmClose));
        builder.setNegativeButton(getString(R.string.txtNo  ),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int which) {
                         // Ne rien faire
                    }
                });
        builder.setPositiveButton(getString(R.string.txtYes),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int which) {
                        canCloseActivityParent = true;
                        doSendEventCloseActivity(0);

                    }
                });
        builder.show();
    }
    private void doSetConfirmDeleteFlyer(final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getString(R.string.txtDialogHeaderDelete));
        builder.setMessage(getString(R.string.txtAskConfirmDelete));
        builder.setNegativeButton(getString(R.string.txtNo  ),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int which) {
                         // Ne rien faire
                    }
                });
        builder.setPositiveButton(getString(R.string.txtYes),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int which) {
                         doDeleteFlyerServer(position);

                    }
                });
        builder.show();
    }
    private void doSetConfirmSetMainFlyer(final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getString(R.string.txtDialogHeaderSetFlyer));
        builder.setMessage(getString(R.string.txtAskConfirmSetFlyer));
        builder.setNegativeButton(getString(R.string.txtNo  ),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int which) {
                         // Ne rien faire
                    }
                });
        builder.setPositiveButton(getString(R.string.txtYes),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int which) {
                         doSetMainFlyerServer(position);

                    }
                });
        builder.show();
    }

    private void doDeleteFlyerServer(final int position) {

        Log.d(Config.TAGKEY," - doDeleteFlyerServer "+position );

        EventFlyersRetrofit flyeritem = flyersLists.get(position);

        if (null==flyeritem.getId())
            return;

        Log.d(Config.TAGKEY," - doDeleteFlyerServer flyer id: "+flyeritem.getId() );

        Call<GenericResultRetrofit> call = apiService.deleteFlyer( flyeritem.getId() );

        call.enqueue(new Callback<GenericResultRetrofit>() {
            @Override
            public void onResponse(Call<GenericResultRetrofit> call, Response<GenericResultRetrofit> response) {
                Log.d(Config.TAGKEY,   " - uploadFlyer - onResponse - " + response.code() + " - " + response.message() + " - URLS : " + call.request().url());

                if (200!=response.code()) {
                    return;
                }

                doAfterDeleteFlyerServer(response.body(), position);
            }

            @Override
            public void onFailure(Call<GenericResultRetrofit> call, Throwable t) {
                Log.d(Config.TAGKEY,   " - uploadFlyer - onFailure - URLS : " + call.request().url()+" - t "+t.getMessage());
                Utils.showSnackLong(coordinatorLayout, getString(R.string.txtErrorServer));
            }
        });



    }
    private void doSetMainFlyerServer(final int position) {

        Log.d(Config.TAGKEY," - doSetMainFlyerServer "+position );

        EventFlyersRetrofit flyeritem = flyersLists.get(position);

        if (null==flyeritem.getId() || eventId<=0)
            return;

        Log.d(Config.TAGKEY," - doSetMainFlyerServer flyer id: "+flyeritem.getId()+" - eventId: "+eventId );

        Call<GenericResultRetrofit> call = apiService.setFlyer(eventId ,  flyeritem.getId() );

        call.enqueue(new Callback<GenericResultRetrofit>() {
            @Override
            public void onResponse(Call<GenericResultRetrofit> call, Response<GenericResultRetrofit> response) {
                Log.d(Config.TAGKEY,   " - uploadFlyer - onResponse - " + response.code() + " - " + response.message() + " - URLS : " + call.request().url());

                if (200!=response.code()) {
                    return;
                }

                doAfterSetFlyerServer(response.body(), position);
            }

            @Override
            public void onFailure(Call<GenericResultRetrofit> call, Throwable t) {
                Log.d(Config.TAGKEY,   " - uploadFlyer - onFailure - URLS : " + call.request().url()+" - t "+t.getMessage());
                Utils.showSnackLong(coordinatorLayout, getString(R.string.txtErrorServer));
            }
        });



    }




    private void doOpenDialogError(int finalNbFieldInvalid, ArrayList<String> errorFields) {
        Log.d(Config.TAGKEY," - doOpenDialogError " );
        Log.d(Config.TAGKEY," - doOpenDialogError - "+TextUtils.join(", ",errorFields) );

        final Dialog dDialog = new Dialog(getActivity());
        dDialog.setContentView(R.layout.dialog_info);

        final TextView textView_name = (TextView) dDialog.findViewById(R.id.textView_name);

        final IconButton iconCancel = (IconButton) dDialog.findViewById(R.id.iconCancel);

        textView_name.setText(getString(R.string.txtErrorDialogFormEvent, TextUtils.join(", ",errorFields)));

        iconCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dDialog.dismiss();
            }
        });

        dDialog.show();
    }

    private void doSaveServer(String fieldName, int fieldThematique, int fieldAcces, String fieldPrice, String fieldDate, String fieldImage, String fieldLieux, String fieldArtists, String fieldFete, int fieldSwHidden, int fieldSwCancelled, String optdateclair) {
        Log.d(Config.TAGKEY," - doSaveEventServer " );

        doDisableUiSendServer();

        Call<GenericResultRetrofit> call = apiService.createEvents(
                fieldName,
                fieldPrice,
                fieldSwHidden,
                fieldSwCancelled,
                fieldDate,
                fieldThematique,
                fieldImage,
                "1", // drag and Drop
                fieldLieux,
                fieldFete,
                fieldArtists,
                fieldAcces,
                "",
                "1" // Android
                ,optdateclair
        );

        if (isEditMode && eventId>0) {
            call  = apiService.updateEvents(
                    fieldName,
                    fieldPrice,
                    fieldSwHidden,
                    fieldSwCancelled,
                    fieldDate,
                    fieldThematique,
                    fieldImage,
                    "1", // drag and Drop
                    fieldLieux,
                    fieldFete,
                    fieldArtists,
                    fieldAcces,
                    "",
                    "1" // Android
                    ,optdateclair
                    ,eventId
            );
        }
        call.enqueue(new Callback<GenericResultRetrofit>() {
            @Override
            public void onResponse(Call<GenericResultRetrofit> call, Response<GenericResultRetrofit> response) {
                Log.d(Config.TAGKEY,   " - uploadFlyer - onResponse - " + response.code() + " - " + response.message() + " - URLS : " + call.request().url());

                if (200!=response.code()) {
                    doResetUploadServer();
                    return;
                }

                doAfterSaveServer(response.body());
            }

            @Override
            public void onFailure(Call<GenericResultRetrofit> call, Throwable t) {
                Log.d(Config.TAGKEY,   " - uploadFlyer - onFailure - URLS : " + call.request().url()+" - t "+t.getMessage());
                Utils.showSnackLong(coordinatorLayout, getString(R.string.txtErrorServer));
                doEnableUiSendServer();
            }
        });
    }

    private void doDisableUiSendServer() {
        // Activer progress bar
        progressBarSave.setVisibility(View.VISIBLE);

        // Changer l'icone du bouton
        btnSaveEvent.setText(getString(R.string.btnSaveGenericLoad));

        // Disable Button
        btnSaveEvent.setEnabled(false);
    }

    private void doResetUploadServer() {
        // Afficher message
        Utils.showSnackLong(coordinatorLayout, getString(R.string.txtErrorUpload));

        doEnableUiSendServer();
    }

    private void doEnableUiSendServer() {
        // Activer progress bar
        progressBarSave.setVisibility(View.GONE);

        // Changer l'icone du bouton
        btnSaveEvent.setText(getString(R.string.btnSaveGeneric));

        // Disable Button
        btnSaveEvent.setEnabled(true);
    }

    private void doAfterSaveServer(GenericResultRetrofit body) {

        doEnableUiSendServer();

        String error = body.getError();
        String success = body.getSuccess();


        Log.d(Config.TAGKEY,   " - doAfterSaveServer -  getError: " + error+" - getSuccess: "+success  );

        if (error!=null) {
            return;
        }

        // Desactiver le bouton
        btnSaveEvent.setEnabled(false);
        btnSaveEvent.setText(getString(R.string.btnSaveGenericOk));

        Utils.showSnackLong(coordinatorLayout, getString(R.string.txtConfirmEntitySaved));

        // Fermer l'activity apres 3 secondes
        doSendEventCloseActivity(3);

    }

    private void doSendEventCloseActivity(int seconds) {

        canCloseActivityParent = true;

        Handler handler = new Handler();
        Runnable runnable;

        runnable = new Runnable() {
            @Override
            public void run() {
                Log.d(Config.TAGKEY," - doSendEventCloseActivity ");

                BusEvent busEvent = new BusEvent();
                busEvent.setFinishActivity(true);
                EventBus.getDefault().post(busEvent);

            }
        };
        handler.postDelayed(runnable,seconds*1000);

    }




    private void doFillSpinnerEntreetype() {

        entreetypeRetrofits = getAllEntreetype();
        spinnEntreetypeAdapter = new SpinEntreetypeAdapter( getActivity() , entreetypeRetrofits);
        spinnEntreetype.setAdapter(spinnEntreetypeAdapter);


        Handler handler = new Handler();
        Runnable runnable;
        runnable = new Runnable() {
            @Override
            public void run() {

                Log.d(Config.TAGKEY,   " - doFillSpinnerThematique "+spinnEntreetypeAdapter.getCount());
                if (!isEditMode && !TextUtils.isEmpty(saveSpinnEntreetype) && spinnEntreetypeAdapter.getCount()>1) {
                    spinnEntreetype.setSelection(spinnEntreetypeAdapter.getPositionByItem(Integer.parseInt(saveSpinnEntreetype)));
                }
            }
        };
        handler.postDelayed(runnable,1000);

        // Au clic sur un item
        spinnEntreetype.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                if (entreetypeRetrofits.size()<=i)
                    return;

                // SI acces selectionne == Payant -> afficher le champ prix
                int selectedId = entreetypeRetrofits.get(i).getId();
                if (selectedId == Config.ENTREETYPE_ID_SHOWPRICE )
                    doEnableFieldPrice();
                else
                    doDisableFieldPrice();



            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                doDisableFieldPrice();
            }
        });
    }

    private void doDisableFieldPrice() {

        // Cacher le prix
        textInputLayoutEditPrice.setVisibility(View.GONE);

        // Vider le input
        editPrice.setText("");
    }

    private void doEnableFieldPrice() {

        // Afficher le champ prix
        textInputLayoutEditPrice.setVisibility(View.VISIBLE);


    }

    private void doClickUploadFlyer() {
        buttonUploadFlyer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doUploadFlyer();
            }
        });
    }

    /**
     * Auto completion lieux
     */
    private void doFillAutoCompleteEditLieu() {
        editLieu.setAdapter(autocompleteLieuAdapter);

        // Au clic sur un item dans la liste proposee
        doOnsetItemClickEditLieu();


        // A la saisie d'un texte
        doOnsetTextChangedListenerLieu();
    }

    private void doOnsetTextChangedListenerLieu() {
        editLieu.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Log.d(Config.TAGKEY, " - edittext_artiste.addTextChangedListener - onTextChanged - s: "+s.length() );
                if( s.length()>0 ) {

                    if (s.length()>2/* && autocompleteLieuAdapter.getCount()<= 0*/) {
                        relativeNoDataLieux.setVisibility(View.VISIBLE);
                        txtNoDataLieux.setText( Html.fromHtml( getString(R.string.txtErrorFieldsNodata , "" ) ) );
                    }
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void doFillAutoCompleteEditArtists() {
        editArtist.setAdapter(autocompleteArtistsAdapter);

        // Au clic sur un item dans la liste proposee
        doOnsetItemClickEditArtists();

        // A la saisie d'un texte
        doOnsetTextChangedListenerArtists();
    }

    private void doOnsetTextChangedListenerArtists() {
        editArtist.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Log.d(Config.TAGKEY, " - edittext_artiste.addTextChangedListener - onTextChanged - s: "+s.length() );
                if( s.length()>0 ) {

                    if (s.length()>2 /*&& autocompleteArtistsAdapter.getCount()<= 0*/) {
                        relativeNoDataArtists.setVisibility(View.VISIBLE);
                        txtNoDataArtists.setText( Html.fromHtml( getString(R.string.txtErrorFieldsNodata , "" ) ) );
                    }
                } else {
                    // Cacher bouton add
                    relativeNoDataArtists.setVisibility(View.GONE);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void doFillAutoCompleteEditFeteLocal() {
        editFete.setAdapter(autocompleteFeteAdapter);

        // Au clic sur un item dans la liste proposee
        doOnsetItemClickEditFete();

        // A la saisie d'un texte
        doOnsetTextChangedListenerFete();
    }

    private void doOnsetTextChangedListenerFete() {
        editFete.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Log.d(Config.TAGKEY, " - edittext_artiste.addTextChangedListener - onTextChanged - s: "+s.length() );
                if( s.length()>0 ) {

                    if (s.length()>2/* && autocompleteFeteAdapter.getCount()<= 0*/) {
                        relativeNoDataFete.setVisibility(View.VISIBLE);
                        txtNoDataFete.setText( Html.fromHtml( getString(R.string.txtErrorFieldsNodata , "" ) ) );
                    }
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }
    private void doOnsetTextChangedListenerSearchEvent() {
        editSearchEvent.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Log.d(Config.TAGKEY, " - editSearchEvent.addTextChangedListener - onTextChanged - s: "+s.length() );
                if( s.length()>0 ) {

                    if (s.length()>2/* && autocompleteFeteAdapter.getCount()<= 0*/) {
                    }
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void doRemoveTagArtistByPosition(int position) {
        artistsTAGIDsArray.remove(position);
        artistsTAGNamesArray.remove(position);
        adapterArtists.removeAt(position);
    }

    private void doOnsetItemClickEditArtists() {
        editArtist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                Log.d(Config.TAGKEY, " - artistsTAGIDsArray : "+artistsTAGIDsArray );

                EventArtistesDjOrganisateurRetrofit item = (EventArtistesDjOrganisateurRetrofit) parent.getItemAtPosition(position);

                if (!artistsTAGIDsArray.contains(item.getId())) {

                    artistsTAGIDsArray.add(item.getId());
                    artistsTAGNamesArray.add(item.getName());

                    // Ajouter dans le adapter du recyclerview
                    doRefreshAdapterArtists(item);

                } else {
                    Utils.showSnackLong(coordinatorLayout, getString(R.string.txtErrorArtistsExistsIntag));
                    // Toast.makeText(TsaraApplication.getContext(),TsaraApplication.getContext().getString(R.string.txtv_error_artists_exists_intag),Toast.LENGTH_LONG).show();
                }

                editArtist.setText("");



                editArtist.setSelection(editArtist.getText().length());


                BusEvent busEvent = new BusEvent();
                busEvent.setHideKeyboard(true);
                EventBus.getDefault().post(busEvent);
            }
        });
    }

    private void doRefreshAdapterArtists(EventArtistesDjOrganisateurRetrofit item) {
        artistsLists.add(item);
        adapterArtists.notifyDataSetChanged();
        // Afficher le recyclerview
        recyclerviewArtists.setVisibility(View.VISIBLE);
    }

    private void doOnsetItemClickEditFete() {
        editFete.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                Log.d(Config.TAGKEY, " - feteTAGIDsArray : "+feteTAGIDsArray );


                EventLocalRetrofit item = (EventLocalRetrofit) parent.getItemAtPosition(position);

                Log.d(Config.TAGKEY, " - item.getName() : "+item.getName() );

                if (!feteTAGIDsArray.contains(item.getId())) {

                    feteTAGIDsArray.add(item.getId());
                    feteTAGNamesArray.add(item.getName());

                    // Ajouter dans le adapter du recyclerview
                    doRefreshAdapterFetes(item);


                } else {
                    Utils.showSnackLong(coordinatorLayout, getString(R.string.txtErrorFeteExistsIntag));
                    // Toast.makeText(TsaraApplication.getContext(),TsaraApplication.getContext().getString(R.string.txtv_error_artists_exists_intag),Toast.LENGTH_LONG).show();
                }

                editFete.setText("");



                editFete.setSelection(editFete.getText().length());

                // Cacher le bouton ajout fete
                relativeNoDataFete.setVisibility(View.GONE);


                BusEvent busEvent = new BusEvent();
                busEvent.setHideKeyboard(true);
                EventBus.getDefault().post(busEvent);
            }
        });
    }
    private void doOnsetItemClickEditSearchEvent() {
        editSearchEvent.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                EventRetrofit item = (EventRetrofit) parent.getItemAtPosition(position);

                // Boit de dialog
                doSetConfirmUpdateEvent(item);

                editSearchEvent.setText("");



                editSearchEvent.setSelection(editSearchEvent.getText().length());


                BusEvent busEvent = new BusEvent();
                busEvent.setHideKeyboard(true);
                EventBus.getDefault().post(busEvent);
            }
        });
    }

    private void doRefreshAdapterFetes(EventLocalRetrofit item) {
        fetesLists.add(item);
        adapterFetes.notifyDataSetChanged();
        // Afficher le recyclerview
        recyclerviewFetes.setVisibility(View.VISIBLE);
    }


    /**
     * Au clic sur un item dans auto completion lieu
     */
    private void doOnsetItemClickEditLieu() {
        editLieu.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                Log.d(Config.TAGKEY, " - lieuxTAGIDsArray : "+lieuxTAGIDsArray );

                EventlieuRetrofit eventlieu = (EventlieuRetrofit) parent.getItemAtPosition(position);

                if (!lieuxTAGIDsArray.contains(eventlieu.getId())) {

                    lieuxTAGIDsArray.add(eventlieu.getId());
                    lieuxTAGNamesArray.add(eventlieu.getName());


                    // Ajouter dans le adapter du recyclerview
                    doRefreshAdapter(eventlieu);


                } else {
                    Utils.showSnackLong(coordinatorLayout, getString(R.string.txtErrorLieuExistsIntag));
                    // Toast.makeText(TsaraApplication.getContext(),TsaraApplication.getContext().getString(R.string.txtv_error_artists_exists_intag),Toast.LENGTH_LONG).show();
                }

                editLieu.setText("");



                editLieu.setSelection(editLieu.getText().length());

                // Cacher le bouton d'ajout
                relativeNoDataLieux.setVisibility(View.GONE);


                BusEvent busEvent = new BusEvent();
                busEvent.setHideKeyboard(true);
                EventBus.getDefault().post(busEvent);
            }
        });
    }

    private void doRefreshAdapter(EventLocalRetrofit item) {
        fetesLists.add(item);
        adapterFetes.notifyDataSetChanged();
        // Afficher le recyclerview
        recyclerviewFetes.setVisibility(View.VISIBLE);
    }

    private void doRefreshAdapter(EventArtistesDjOrganisateurRetrofit item) {
        artistsLists.add(item);
        adapterArtists.notifyDataSetChanged();
        // Afficher le recyclerview
        recyclerviewArtists.setVisibility(View.VISIBLE);
    }

    private void doRefreshAdapter(EventlieuRetrofit item) {
        lieuxLists.add(item);
        adapterLieux.notifyDataSetChanged();
        // Afficher le recyclerview
        recyclerviewLieux.setVisibility(View.VISIBLE);
    }

    private void doRefreshAdapter(EventFlyersRetrofit item) {
        flyersLists.add(item);
        Log.d(Config.TAGKEY, " doRefreshAdapter - flyersLists "+flyersLists.toString());
        if (flyersLists.size()>0) {
            int nb = 0;
            for(EventFlyersRetrofit liste : flyersLists) {

                if (null!=liste.getmImageUri() && nb>0) {
                    flyersLists.remove(nb);
                    Log.d(Config.TAGKEY, " doRefreshAdapter - flyersLists " + nb + " -- remove : " + liste.getmImageUri().toString());
                } else
                    Log.d(Config.TAGKEY, " doRefreshAdapter - flyersLists " +nb+" -- "+ liste.getImage());

                nb++;
            }
        }
        adapterFlyers.notifyDataSetChanged();
        // Afficher le recyclerview
        recyclerviewFlyers.setVisibility(View.VISIBLE);
    }

    private void doRefreshAdapter(EventFlyersRetrofit item, int position) {
        Log.d(Config.TAGKEY, " doRefreshAdapter - position "+position);
        Log.d(Config.TAGKEY, " doRefreshAdapter - item.getImage() "+item.getImage());
        Log.d(Config.TAGKEY, " doRefreshAdapter - item.getId() "+item.getId());
        Log.d(Config.TAGKEY, " doRefreshAdapter - item.getTmp() "+item.getTmp() );
        Log.d(Config.TAGKEY, " doRefreshAdapter - item.getUri() "+item.getUri() );
        Log.d(Config.TAGKEY, " doRefreshAdapter - adapterFlyers.getItemCount() "+adapterFlyers.getItemCount());
        flyersLists.set(position,item);
        //flyersLists.add(position,item);
        adapterFlyers.notifyItemRangeChanged(position,adapterFlyers.getItemCount());
        //adapterFlyers.notifyItemChanged(position);
        Log.d(Config.TAGKEY, " doRefreshAdapter - adapterFlyers.getItemCount() "+adapterFlyers.getItemCount());
        // Afficher le recyclerview
        recyclerviewFlyers.setVisibility(View.VISIBLE);
    }

    /*private void doClickDate() {
        txtDateEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doOpenCalendarDialog();
            }
        });
    }*/
    private void doClickDateUnique() {
        btnDateEventUnique.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dateSelected.clear();

                Calendar lastYear = Calendar.getInstance();
                lastYear.add(Calendar.YEAR, -1);
                Calendar nextYear = Calendar.getInstance();
                nextYear.add(Calendar.YEAR, 1);

                String title = getString(R.string.txtDialogHeaderChooseDateUnique);
                showCalendarInDialogWithHour(title, R.layout.dialog_timesquare);

                Date minDate = getMinDate();
                Date maxDate = getMaxDate();

//                dialogView.init(new Date(), nextYear.getTime()) //
                dialogView.init(minDate, maxDate) //
                        .inMode(CalendarPickerView.SelectionMode.SINGLE) //
                        .withSelectedDate(minDate);


                dateSelected.add(new Date());

                doSelectedDateWithValues();




                dialogView.setOnDateSelectedListener(new CalendarPickerView.OnDateSelectedListener() {
                    @Override
                    public void onDateSelected(Date date) {
                        Calendar cal = Calendar.getInstance();
                        cal.setTime(date);
                        String s = cal.get(Calendar.DAY_OF_MONTH)+"/"+cal.get(Calendar.MONTH)+"/"+cal.get(Calendar.YEAR);
                        Log.d(Config.TAGKEY, " doClickDateTs onDateSelected  : "+s );
                        // add in date

                        if (!dateSelected.contains(date))
                            dateSelected.add(date);

                        if (dateSelected.size()>1)
                            dateSelected.remove(0);

                        Log.d(Config.TAGKEY, " doClickDateTs onDateSelected  : "+dateSelected.toString() );
                    }

                    @Override
                    public void onDateUnselected(Date date) {
                        Calendar cal = Calendar.getInstance();
                        cal.setTime(date);
                        String s = cal.get(Calendar.DAY_OF_MONTH)+"/"+cal.get(Calendar.MONTH)+"/"+cal.get(Calendar.YEAR);
                        Log.d(Config.TAGKEY, " doClickDateTs onDateUnselected  : "+s );
                        // Remove from date
                        if ( dateSelected.contains(date))
                            dateSelected.remove(date);

                        Log.d(Config.TAGKEY, " doClickDateTs onDateUnselected  : "+dateSelected.toString() );

                    }
                });
            }
        });
    }

    private void showCalendarInDialogWithHour(String title, int layoutResId) {
        dialogView = (CalendarPickerView) getActivity().getLayoutInflater().inflate(layoutResId, null, false);
        theDialog = new AlertDialog.Builder(getActivity()) //
                .setTitle(title)
                .setView(dialogView)
                .setNeutralButton(getString(R.string.btnChooseHeures), new DialogInterface.OnClickListener() {
                    @Override public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        doInstanceTimePickerTimesquare();
                        tp.show();

                    }
                })
                .create();
        theDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override public void onShow(DialogInterface dialogInterface) {
                Log.d(TAG, "onShow: fix the dimens!");
                dialogView.fixDialogDimens();
            }
        });
        theDialog.show();
    }

    private void doClickDateRange() {
        btnDateEventRange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dateSelected.clear();

                Calendar lastYear = Calendar.getInstance();
                lastYear.add(Calendar.YEAR, -1);
                Calendar nextYear = Calendar.getInstance();
                nextYear.add(Calendar.YEAR, 1);

                String title = getString(R.string.txtDialogHeaderChooseDateRange);
                showCalendarInDialogRange(title, R.layout.dialog_timesquare);

                Calendar c = Calendar.getInstance();
                c.setTime(new Date());
                Log.d(Config.TAGKEY, " doClickDateTs c.toString()  : "+c.toString() );

                Date minDate = getMinDate();
                Date maxDate = getMaxDate();

//                dialogView.init(new Date(), nextYear.getTime()) //
                dialogView.init(minDate, maxDate) //
                        .inMode(CalendarPickerView.SelectionMode.RANGE) //
                        /*.withSelectedDate(minDate)*/;



                doSelectedDateWithValues();




                dateSelected.add(new Date());

                dialogView.setOnDateSelectedListener(new CalendarPickerView.OnDateSelectedListener() {
                    @Override
                    public void onDateSelected(Date date) {
                        Calendar cal = Calendar.getInstance();
                        cal.setTime(date);
                        String s = cal.get(Calendar.DAY_OF_MONTH)+"/"+cal.get(Calendar.MONTH)+"/"+cal.get(Calendar.YEAR);
                        Log.d(Config.TAGKEY, " doClickDateTs onDateSelected  : "+s );
                        // add in date

                        if (!dateSelected.contains(date))
                            dateSelected.add(date);

                        if (dateSelected.size()>2)
                            dateSelected.remove(0);

                        Log.d(Config.TAGKEY, " doClickDateTs onDateSelected  : "+dateSelected.toString() );
                    }

                    @Override
                    public void onDateUnselected(Date date) {
                        Calendar cal = Calendar.getInstance();
                        cal.setTime(date);
                        String s = cal.get(Calendar.DAY_OF_MONTH)+"/"+cal.get(Calendar.MONTH)+"/"+cal.get(Calendar.YEAR);
                        Log.d(Config.TAGKEY, " doClickDateTs onDateUnselected  : "+s );
                        // Remove from date
                        if ( dateSelected.contains(date))
                            dateSelected.remove(date);

                        Log.d(Config.TAGKEY, " doClickDateTs onDateUnselected  : "+dateSelected.toString() );

                    }
                });
            }
        });
    }


    private Date getDateWithYearAndMonthForDay(int year, int month, int day) {
        final Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.DAY_OF_MONTH, day);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        return cal.getTime();
    }
    private void doClickDateMultiple() {
        btnDateEventMultiple.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dateSelected.clear();

                Calendar lastYear = Calendar.getInstance();
                lastYear.add(Calendar.YEAR, -1);
                Calendar nextYear = Calendar.getInstance();
                nextYear.add(Calendar.YEAR, 1);

                String title = getString(R.string.txtDialogHeaderChooseDateMultiples);
                showCalendarInDialogMultiple(title, R.layout.dialog_timesquare);

                Date minDate = getMinDate();
                Date maxDate = getMaxDate();

//                dialogView.init(new Date(), nextYear.getTime()) //
                dialogView.init(minDate, maxDate) //
                        .inMode(CalendarPickerView.SelectionMode.MULTIPLE) //
                        /*.withSelectedDate(minDate)*/;

                doSelectedDateWithValues();


//                dateSelected.add(new Date());

                dialogView.setOnDateSelectedListener(new CalendarPickerView.OnDateSelectedListener() {
                    @Override
                    public void onDateSelected(Date date) {
                        Calendar cal = Calendar.getInstance();
                        cal.setTime(date);
                        String s = cal.get(Calendar.DAY_OF_MONTH)+"/"+cal.get(Calendar.MONTH)+"/"+cal.get(Calendar.YEAR);
                        Log.d(Config.TAGKEY, " doClickDateTs onDateSelected  : "+s );
                        // add in date

                        if (!dateSelected.contains(date))
                            dateSelected.add(date);


                        Log.d(Config.TAGKEY, " doClickDateTs onDateSelected  : "+dateSelected.toString() );
                    }

                    @Override
                    public void onDateUnselected(Date date) {
                        Calendar cal = Calendar.getInstance();
                        cal.setTime(date);
                        String s = cal.get(Calendar.DAY_OF_MONTH)+"/"+cal.get(Calendar.MONTH)+"/"+cal.get(Calendar.YEAR);
                        Log.d(Config.TAGKEY, " doClickDateTs onDateUnselected  : "+s );
                        // Remove from date
                        if ( dateSelected.contains(date))
                            dateSelected.remove(date);

                        Log.d(Config.TAGKEY, " doClickDateTs onDateUnselected  : "+dateSelected.toString() );

                    }
                });
            }
        });
    }

    private Date getMinDate() {
        Calendar cal = Calendar.getInstance();
        // SI on est en mode ajout event - disable date avant aujourd'hui
        if (TextUtils.isEmpty(saveTxtDateEvent)) {
            Date nd = new Date();
            cal.setTime(nd);

            // Retirer 5 j de moins
            cal.add(Calendar.DAY_OF_MONTH,-5);
            return cal.getTime();
        }
        // Selon le type de la date
        Log.d(Config.TAGKEY," getTxtDateEvent  "+saveHiddenFieldOptDate);
        Date d = null;

        switch (saveHiddenFieldOptDate) {
            case "multiplesdate":
                d = getMinDateMultipleDate();
                break;

            case "rangedate":
                d = getMinDateRangeDate();
                break;

            default:
                d = getMinDateUniqueDate();
                break;

        }

        
        cal.setTime(d);

        // Retirer un an de -
        cal.add(Calendar.YEAR,-1);

        return cal.getTime();
    }

    private Date getMinDateMultipleDate() {

        // 2017-07-20 12:00; 2017-07-22 12:00; 2017-07-25 12:00;
        ArrayList<Date> dList = getListMultipleDate();

        return Collections.min(dList);

    }

    private ArrayList<Date> getListMultipleDate() {
        ArrayList<Date> dList = new ArrayList<Date>();

        String[] sl = TextUtils.split(saveTxtDateEvent,";");

        int nb = 0;
        for(String date: sl) {

            date = date.trim();
            String[] arrDf = TextUtils.split(date.trim()," ");
            String eachDate = arrDf[0].trim();

            String[] arrD = TextUtils.split(eachDate,"-");


            int d = Integer.parseInt(arrD[2].trim());
            int m = Integer.parseInt(arrD[1].trim());
            int y = Integer.parseInt(arrD[0].trim());

            dList.add(getDateWithYearAndMonthForDay(y, m-1, d));

            if ((nb+1)==sl.length)
                dList.add(getDateWithYearAndMonthForDay(y, m-1, d+1));

            nb++;


        }
        return dList;
    }

    private Date getMinDateRangeDate() {

        // 2017-10-18 12:00 au 2017-10-27 12:00
        ArrayList<Date> dList = getListRangeDate();

        return Collections.min(dList);

    }

    private ArrayList<Date> getListRangeDate() {

        String[] sl = TextUtils.split(saveTxtDateEvent,"au");


        String dateDebut = sl[0];
        String[] arrDd = TextUtils.split(dateDebut.trim()," ");
        dateDebut = arrDd[0].trim();

        String[] slDd = TextUtils.split(dateDebut,"-");

        String dateFin = sl[1];
        String[] arrDf = TextUtils.split(dateFin.trim()," ");
        dateFin = arrDf[0].trim();

        String[] slDf = TextUtils.split(dateFin,"-");



        int d1 = Integer.parseInt(slDd[2].trim());
        int m1 = Integer.parseInt(slDd[1].trim());
        int y1 = Integer.parseInt(slDd[0].trim());

        int d2 = Integer.parseInt(slDf[2].trim());
        int m2 = Integer.parseInt(slDf[1].trim());
        int y2 = Integer.parseInt(slDf[0].trim());

        ArrayList<Date> dList = new ArrayList<Date>();
        dList.add(getDateWithYearAndMonthForDay(y1, m1-1, d1));
        dList.add(getDateWithYearAndMonthForDay(y2, m2-1, d2));
        dList.add(getDateWithYearAndMonthForDay(y2, m2-1, (d2+1)));

        Log.d(Config.TAGKEY," getListRangeDate "+dList.toString());

        return dList;
    }

    private Date getMinDateUniqueDate() {

        // 2017-09-01 21:30---23:59
        String[] arrDf = TextUtils.split(saveTxtDateEvent.trim()," ");
        String tmpDate = arrDf[0].trim();

        String[] slDd = TextUtils.split(tmpDate,"-");

        int d1 = Integer.parseInt(slDd[2].trim());
        int m1 = Integer.parseInt(slDd[1].trim());
        int y1 = Integer.parseInt(slDd[0].trim());

        return getDateWithYearAndMonthForDay(y1, m1-1, d1);

    }

    private Date getMaxDate() {
        Calendar nextYear = Calendar.getInstance();
        nextYear.add(Calendar.YEAR, 1);
        if (TextUtils.isEmpty(saveTxtDateEvent))
            return nextYear.getTime();

        Date d = null;

        switch (saveHiddenFieldOptDate) {
            case "multiplesdate":
                d = getMaxDateMultipleDate();
                break;

            case "rangedate":
                d = getMaxDateRangeDate();
                break;
            default:
                d = getMaxDateUniqueDate();
                break;
        }

        Calendar cal = Calendar.getInstance();
        cal.setTime(d);

        // Ajouter un an de +
        cal.add(Calendar.YEAR,1);

        return cal.getTime();

    }

    private Date getMaxDateMultipleDate() {
        // 2017-07-20 12:00; 2017-07-22 12:00; 2017-07-25 12:00;
        ArrayList<Date> dList = getListMultipleDate();

        return Collections.max(dList);
    }

    private Date getMaxDateRangeDate() {
        // 2017-10-18 12:00 au 2017-10-27 12:00
        ArrayList<Date> dList = getListRangeDate();

        return Collections.max(dList);

    }

    private Date getMaxDateUniqueDate() {

        Date d = getMinDateUniqueDate();
        Calendar cal = Calendar.getInstance();
        cal.setTime(d);

        return cal.getTime();
    }

    private void doSelectedDateWithValues() {
        if (TextUtils.isEmpty(saveTxtDateEvent))
            return;

        // Selon le type de la date
        Log.d(Config.TAGKEY," doSelectedDateWithValues saveHiddenFieldOptDate: "+saveHiddenFieldOptDate);

        switch (saveHiddenFieldOptDate) {
            case "multiplesdate":
                doSelectedMultiplesDateWithValues();
                break;
            case "rangedate":
                doSelectedRangeDateWithValues();
                break;
            default:
                doSelectedUniqueDateWithValues();
                break;
        }

        Log.d(Config.TAGKEY, " doSelectedDateWithValues dateSelected : "+dateSelected.toString() );


    }

    private void doSelectedUniqueDateWithValues() {
        // 2017-09-01 21:30---23:59
        if (TextUtils.isEmpty(saveTxtDateEvent))
            return;

        String[] arrDd = TextUtils.split(saveTxtDateEvent.trim()," ");
        String tmpDate = arrDd[0].trim();

        String[] slDd = TextUtils.split(tmpDate,"-");

        int d1 = Integer.parseInt(slDd[2].trim());
        int m1 = Integer.parseInt(slDd[1].trim());
        int y1 = Integer.parseInt(slDd[0].trim());

        dialogView.selectDate(getDateWithYearAndMonthForDay(y1, m1-1, d1));

        if (!dateSelected.contains(getDateWithYearAndMonthForDay(y1, m1-1, d1)))
            dateSelected.add(getDateWithYearAndMonthForDay(y1, m1-1, d1));
    }

    private void doSelectedRangeDateWithValues() {
        // 2017-10-18 12:00 au 2017-10-27 12:00
        if (TextUtils.isEmpty(saveTxtDateEvent))
            return;

        Log.d(Config.TAGKEY, " doSelectedRangeDateWithValues "+saveTxtDateEvent );

        String[] sl = TextUtils.split(saveTxtDateEvent,"au");
        if (sl.length!=2)
            return;

        String dateDebut = sl[0];
        String[] arrDd = TextUtils.split(dateDebut.trim()," ");
        dateDebut = arrDd[0].trim();
        Log.d(Config.TAGKEY, " doSelectedRangeDateWithValues dateDebut : "+dateDebut );
        String[] slDd = TextUtils.split(dateDebut,"-");

        String dateFin = sl[1];
        String[] arrDf = TextUtils.split(dateFin.trim()," ");
        dateFin = arrDf[0].trim();
        Log.d(Config.TAGKEY, " doSelectedRangeDateWithValues dateFin : "+dateFin );
        String[] slDf = TextUtils.split(dateFin,"-");

        if (slDd.length!=3 || slDf.length!=3 )
            return;

        int d1 = Integer.parseInt(slDd[2].trim());
        int m1 = Integer.parseInt(slDd[1].trim());
        int y1 = Integer.parseInt(slDd[0].trim());

        Log.d(Config.TAGKEY, " doSelectedRangeDateWithValues d1-m1-y1 : "+d1+"-"+m1+"-"+y1 );

        int d2 = Integer.parseInt(slDf[2].trim());
        int m2 = Integer.parseInt(slDf[1].trim());
        int y2 = Integer.parseInt(slDf[0].trim());

        Log.d(Config.TAGKEY, " doSelectedRangeDateWithValues d2-m2-y2 : "+d2+"-"+m2+"-"+y2 );

        dialogView.selectDate(getDateWithYearAndMonthForDay(y1, m1-1, d1));
        dialogView.selectDate(getDateWithYearAndMonthForDay(y2, m2-1, d2));

        if (!dateSelected.contains(getDateWithYearAndMonthForDay(y1, m1-1, d1)))
            dateSelected.add(getDateWithYearAndMonthForDay(y1, m1-1, d1));

        if (!dateSelected.contains(getDateWithYearAndMonthForDay(y2, m2-1, d2)))
            dateSelected.add(getDateWithYearAndMonthForDay(y2, m2-1, d2));
    }

    private void doSelectedMultiplesDateWithValues() {
        // 2017-07-20 12:00; 2017-07-22 12:00; 2017-07-25 12:00;
        if (TextUtils.isEmpty(saveTxtDateEvent))
            return;

        String[] sl = TextUtils.split(saveTxtDateEvent,";");
        if (sl.length<=0)
            return;

        for(String date: sl) {

            date = date.trim();
            String[] arrDf = TextUtils.split(date.trim()," ");
            String eachDate = arrDf[0].trim();


            String[] arrD = TextUtils.split(eachDate,"-");

            if (arrD.length!=3)
                return;

            int d = Integer.parseInt(arrD[2].trim());
            int m = Integer.parseInt(arrD[1].trim());
            int y = Integer.parseInt(arrD[0].trim());



            dialogView.selectDate(getDateWithYearAndMonthForDay(y, m-1, d));

            if (!dateSelected.contains(getDateWithYearAndMonthForDay(y, m-1, d)))
                dateSelected.add(getDateWithYearAndMonthForDay(y, m-1, d));

            Log.d(Config.TAGKEY," doSelectedMultiplesDateWithValues Ok pour "+y+"-"+(m-1)+"-"+d);


        }

    }


    private void showCalendarInDialogRange(String title, int layoutResId) {
        dialogView = (CalendarPickerView) getActivity().getLayoutInflater().inflate(layoutResId, null, false);
        theDialog = new AlertDialog.Builder(getActivity()) //
                .setTitle(title)
                .setView(dialogView)
                .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                    @Override public void onClick(DialogInterface dialogInterface, int i) {

                        if ( dateSelected.size()!=2 )
                            return;

                        Log.d(Config.TAGKEY, " showCalendarInDialogRange dateSelected  : "+dateSelected.toString() );

                        Calendar cal1 = Calendar.getInstance();
                        cal1.setTime(dateSelected.get(0));

                        Calendar cal2 = Calendar.getInstance();
                        cal2.setTime(dateSelected.get(1));

                        dialogInterface.dismiss();

                        int d1 = cal1.get(Calendar.DAY_OF_MONTH);
                        int m1 = cal1.get(Calendar.MONTH)+1;
                        int y1 = cal1.get(Calendar.YEAR);

                        int d2 = cal2.get(Calendar.DAY_OF_MONTH);
                        int m2 = cal2.get(Calendar.MONTH)+1;
                        int y2 = cal2.get(Calendar.YEAR);

                        String datechoosen =  d1 + "-" + m1 ;
                        datechoosen +=  " au ";
                        datechoosen +=  d2 + "-" + m2 + "-" + y2;
                        txtdateSelected.setText( datechoosen);

                        // 2017-06-16 12:00 au 2017-06-17 12:00
                        String hiddenDate = y1+"-"+(m1<10?"0"+m1:m1)+"-"+(d1<10?"0"+d1:d1)+" 12:00 au "+y2+"-"+(m2<10?"0"+m2:m2)+"-"+(d2<10?"0"+d2:d2)+" 12:00";
                        hiddenFieldDate.setText( hiddenDate );
                        hiddenFieldOptDate.setText( "rangedate" );

                        saveTxtDateEvent = hiddenDate;
                        saveHiddenFieldOptDate = "rangedate";
                    }
                })
                .create();
        theDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override public void onShow(DialogInterface dialogInterface) {
                Log.d(TAG, "onShow: fix the dimens!");
                dialogView.fixDialogDimens();
            }
        });
        theDialog.show();
    }
    private void showCalendarInDialogMultiple(String title, int layoutResId) {
        // 2017-07-20 12:00; 2017-07-22 12:00;
        dialogView = (CalendarPickerView) getActivity().getLayoutInflater().inflate(layoutResId, null, false);
        theDialog = new AlertDialog.Builder(getActivity()) //
                .setTitle(title)
                .setView(dialogView)
                .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                    @Override public void onClick(DialogInterface dialogInterface, int i) {

                        if ( dateSelected.size()<=0 )
                            return;

                        Log.d(Config.TAGKEY, " showCalendarInDialogMultiple dateSelected  : "+dateSelected.toString() );

                        ArrayList<String> hd = new ArrayList<String>();
                        ArrayList<String> ds = new ArrayList<String>();

                        int nb = 0;
                        for (Date date: dateSelected) {
                            Calendar c = Calendar.getInstance();
                            c.setTime(date);

                            int d = c.get(Calendar.DAY_OF_MONTH);
                            int m = c.get(Calendar.MONTH)+1;
                            int y = c.get(Calendar.YEAR);

                            String hiddenDate = y+"-"+(m<10?"0"+m:m)+"-"+(d<10?"0"+d:d)+" 12:00";
                            String showDate = (d<10?"0"+d:d)+"-"+(m<10?"0"+m:m) ;

                            if (!hd.contains(hiddenDate))
                                hd.add(hiddenDate);

                            if (!ds.contains(showDate) && nb<4)
                                ds.add(showDate);



                            nb++;

                        }



                        txtdateSelected.setText( TextUtils.join(", ", ds)+(nb>4 ? "..." : "") );

                        String hiddenDate = TextUtils.join("; ", hd);

                        // 2017-06-16 12:00 au 2017-06-17 12:00
                        hiddenFieldDate.setText( hiddenDate );
                        hiddenFieldOptDate.setText( "multiplesdate" );

                        Log.d(Config.TAGKEY, " showCalendarInDialogMultiple TextUtils join  : "+TextUtils.join("; ", hd) );

                        saveTxtDateEvent = hiddenDate;
                        saveHiddenFieldOptDate = "multiplesdate";
                    }
                })
                .create();
        theDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override public void onShow(DialogInterface dialogInterface) {
                Log.d(TAG, "onShow: fix the dimens!");
                dialogView.fixDialogDimens();
            }
        });
        theDialog.show();
    }



    private void doInstanceTimePickerTimesquare() {
        is24hour = DateFormat.is24HourFormat(getActivity());

        // 2017-09-01 21:30---23:59
        if ( !TextUtils.isEmpty(saveTxtDateEvent)) {
            String[] arrTmpHeures = TextUtils.split(saveTxtDateEvent.trim()," ");
            String tmpHours = arrTmpHeures[1].trim();

            String[] arrTmpHourFirst = TextUtils.split(tmpHours,"---");

            String tmpH =  arrTmpHourFirst[0].trim();

            String[] arrTmpHi = TextUtils.split(tmpH,":");

            hour = Integer.parseInt(arrTmpHi[0].trim());
            minute = Integer.parseInt(arrTmpHi[1].trim());

        }




        tp = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {

                if (dateSelected.size()<=0)
                    return;

                Log.d(Config.TAGKEY, " doInstanceTimePickerTimesquare dateSelected  : "+dateSelected.toString() );

                Calendar cal = Calendar.getInstance();
                cal.setTime(dateSelected.get(0));

                int d = cal.get(Calendar.DAY_OF_MONTH);
                int m = cal.get(Calendar.MONTH)+1;
                int y = cal.get(Calendar.YEAR);

                int h = hourOfDay;
                int i = minute;


                String datechoosen = d+"-"+m+"-"+y;
                datechoosen += " - ";
                datechoosen += String.valueOf(h) + ":" + String.valueOf(i);

                // 2017-06-25 18:00---23:59
                String hiddenDate = y+"-"+(m<10?"0"+m:m)+"-"+(d<10?"0"+d:d)+" "+(h<10?"0"+h:h)+":"+(i<10?"0"+i:i)+"---23:59";
                txtdateSelected.setText( datechoosen);
                hiddenFieldDate.setText( hiddenDate );
                hiddenFieldOptDate.setText( "" );

                Log.d(Config.TAGKEY, " doInstanceTimePickerTimesquare hiddenDate  : "+hiddenDate );
                Log.d(Config.TAGKEY, " doInstanceTimePickerTimesquare datechoosen  : "+datechoosen );

                saveTxtDateEvent = hiddenDate;
                saveHiddenFieldOptDate = "unique";

            }
        }, hour, minute, is24hour );


    }

    private void doFillSpinnerThematique() {

        eventtypeRetrofits = getAllThematique();
        spinnAdapter = new SpinBaseAdapter(getActivity(), eventtypeRetrofits);
        spinnThematique.setAdapter(spinnAdapter);



        Handler handler = new Handler();
        Runnable runnable;
        runnable = new Runnable() {
            @Override
            public void run() {

                Log.d(Config.TAGKEY,   " - doFillSpinnerThematique "+spinnAdapter.getCount());
                if (!isEditMode && !TextUtils.isEmpty(saveSpinnThematique) && spinnAdapter.getCount()>1) {
                    spinnThematique.setSelection(spinnAdapter.getPositionByItem(Integer.parseInt(saveSpinnThematique)));
                }
            }
        };
        handler.postDelayed(runnable,1000);


    }

    private ArrayList<EventtypeRetrofit> getAllThematique() {
        eventtypeRetrofits = new ArrayList<>();
        eventtypeRetrofits.add(new EventtypeRetrofit(0,getString(R.string.hintSpinnerThematique )));



        Call<AllEventTypeRetrofit> call = apiService.getAllEventType();
        call.enqueue(new Callback<AllEventTypeRetrofit>() {
            @Override
            public void onResponse(Call<AllEventTypeRetrofit> call, Response<AllEventTypeRetrofit> response) {
                Log.d(Config.TAGKEY,   " - getAllThematique - onResponse - " + response.code() + " - " + response.message() + " - URLS : " + call.request().url());

                if (200!=response.code()) {
                    Utils.showSnackLong(coordinatorLayout, getString(R.string.txtErrorShow));
                    return;
                }

                List<EventtypeRetrofit> listEventType = response.body().getResults();
                for( EventtypeRetrofit evt: listEventType) {
                    int idEvent = evt.getId();
                    String nameEvent = evt.getName();
                    EventtypeRetrofit evtr = new EventtypeRetrofit(idEvent, nameEvent);
                    eventtypeRetrofits.add( evtr );
                }

                Log.d(Config.TAGKEY,   " - getAllThematique "+eventtypeRetrofits.size());

            }

            @Override
            public void onFailure(Call<AllEventTypeRetrofit> call, Throwable t) {
                Log.d(Config.TAGKEY,   " - uploadFlyer - onFailure - URLS : " + call.request().url()+" - t "+t.getMessage());
                Utils.showSnackLong(coordinatorLayout, getString(R.string.txtErrorServer));
            }
        });

        return eventtypeRetrofits;
    }

    private ArrayList<EntreetypeRetrofit> getAllEntreetype()
    {
        entreetypeRetrofits = new ArrayList<>();
        entreetypeRetrofits.add(new EntreetypeRetrofit(0,getString(R.string.hintSpinnerEntreetype )));



        Call<AllEntreeTypeRetrofit> call = apiService.getAllEntreeType();
        call.enqueue(new Callback<AllEntreeTypeRetrofit>() {
            @Override
            public void onResponse(Call<AllEntreeTypeRetrofit> call, Response<AllEntreeTypeRetrofit> response) {
                Log.d(Config.TAGKEY,   " - uploadFlyer - onResponse - " + response.code() + " - " + response.message() + " - URLS : " + call.request().url());

                if (200!=response.code()) {
                    Utils.showSnackLong(coordinatorLayout, getString(R.string.txtErrorShowAcces));
                    return;
                }

                List<EntreetypeRetrofit> listEventType = response.body().getResults();
                for( EntreetypeRetrofit evt: listEventType) {
                    int idEvent = evt.getId();
                    String nameEvent = evt.getName();
                    EntreetypeRetrofit evtr = new EntreetypeRetrofit(idEvent, nameEvent);
                    entreetypeRetrofits.add( evtr );
                }

            }

            @Override
            public void onFailure(Call<AllEntreeTypeRetrofit> call, Throwable t) {
                Log.d(Config.TAGKEY,   " - uploadFlyer - onFailure - URLS : " + call.request().url()+" - t "+t.getMessage());
                Utils.showSnackLong(coordinatorLayout, getString(R.string.txtErrorServer));

            }
        });

        return entreetypeRetrofits;
    }

    private void doUploadFlyer() {
        Log.d(Config.TAGKEY,"Clic sur Uploadflyer "+mimageUri.toString());

        // Activer progress bar
        pgHorizontal.setVisibility(View.VISIBLE);

        String filePath = UriHelpers.getFileFromUri(getActivity(), mimageUri);
        File file = new File(filePath);

        RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), file);

        MultipartBody.Part body = MultipartBody.Part.createFormData("flyer", file.getName(), requestFile);
        RequestBody oldfilename = RequestBody.create(MediaType.parse("text/plain"), "");
        RequestBody flyerurl = RequestBody.create(MediaType.parse("text/plain"), "");


        Call<UploadFlyerRetrofit> call = apiService.uploadFlyer( body, oldfilename, flyerurl );
        call.enqueue(new Callback<UploadFlyerRetrofit>() {
            @Override
            public void onResponse(Call<UploadFlyerRetrofit> call, Response<UploadFlyerRetrofit> response) {
                Log.d(Config.TAGKEY,   " - uploadFlyer - onResponse - " + response.code() + " - " + response.message() + " - URLS : " + call.request().url());

                if (200!=response.code()) {
                    doResetUpload();
                    return;
                }

                doAfterUpload(response.body());
            }

            @Override
            public void onFailure(Call<UploadFlyerRetrofit> call, Throwable t) {
                Log.d(Config.TAGKEY,   " - uploadFlyer - onFailure - URLS : " + call.request().url()+" - t "+t.getMessage());
                Utils.showSnackLong(coordinatorLayout, getString(R.string.txtErrorServer));
            }
        });
    }
    private void doUploadFlyer(final int position, EventFlyersRetrofit item) {
        // File file = new File(imageUri.toString());
        if (null==item.getmImageUri())
            return;

        String filePath = UriHelpers.getFileFromUri(getActivity(), mimageUri);
        File file = new File(filePath);

        RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), file);

        MultipartBody.Part body = MultipartBody.Part.createFormData("flyer", file.getName(), requestFile);
        RequestBody oldfilename = RequestBody.create(MediaType.parse("text/plain"), "");
        RequestBody flyerurl = RequestBody.create(MediaType.parse("text/plain"), "");


        Call<UploadFlyerRetrofit> call = apiService.uploadFlyer( body, oldfilename, flyerurl );
        call.enqueue(new Callback<UploadFlyerRetrofit>() {
            @Override
            public void onResponse(Call<UploadFlyerRetrofit> call, Response<UploadFlyerRetrofit> response) {
                Log.d(Config.TAGKEY,   " - uploadFlyer - onResponse - " + response.code() + " - " + response.message() + " - URLS : " + call.request().url());

                doAfterUpload(response.body(), position);
            }

            @Override
            public void onFailure(Call<UploadFlyerRetrofit> call, Throwable t) {
                Log.d(Config.TAGKEY,   " - uploadFlyer - onFailure - URLS : " + call.request().url()+" - t "+t.getMessage());
                Utils.showSnackLong(coordinatorLayout, getString(R.string.txtErrorServer));
            }
        });
    }
    private void doResetUpload() {
        // Afficher message
        Utils.showSnackLong(coordinatorLayout, getString(R.string.txtErrorUpload));

        // Cacher progress
        pgHorizontal.setVisibility(View.GONE);
    }

    private void doAfterUpload(UploadFlyerRetrofit body) {
        String filename = body.getFilename();
        String error = body.getError();
        String success = body.getSuccess();


        Log.d(Config.TAGKEY,   " - doAfterUpload -  getError: " + error+" - getFilename:  "+filename+" - getSuccess: "+success  );

        if (error!=null) {
            return;
        }

        UriHelpers.removeAllTmpFiles();

        doActionAfterUploadedFile(filename);


        Utils.showSnackLong(coordinatorLayout, getString(R.string.txtConfirmUploadFlyer, filename));

        // Enregistrer si after upload
        if (saveAfterUploadFlyer) {
            doValidform();
        }
    }


    private void doAfterUpload(UploadFlyerRetrofit body, int position) {
        String filename = body.getFilename();
        String error = body.getError();
        String success = body.getSuccess();


        Log.d(Config.TAGKEY,   " - doAfterUpload -  getError: " + error+" - getFilename:  "+filename+" - getSuccess: "+success  );

        if (error!=null) {
            return;
        }

        doActionAfterUploadedFile(filename,position);


        Utils.showSnackLong(coordinatorLayout, getString(R.string.txtConfirmUploadFlyer, filename));


    }
    private void doAfterDeleteFlyerServer(GenericResultRetrofit body, int position) {
        String successOrError = null;
        String error = null;
        String success = null;

        if (null!=body.getError())
            error = body.getError();

        if (null!=body.getSuccess())
            success = body.getSuccess();

        if (!TextUtils.isEmpty(success) && TextUtils.isEmpty(error))
            successOrError = success;
        else
            successOrError = error;

        Log.d(Config.TAGKEY,   " - doAfterUpload -  successOrError: " + successOrError  );

        // Supprimer dans l'adapter
        doRemoveFlyersByPosition(position);

        // Afficher message de retour
        Utils.showSnackLong(coordinatorLayout, successOrError);


    }
    private void doAfterSetFlyerServer(GenericResultRetrofit body, int position) {
        String successOrError = null;
        String error = null;
        String success = null;

        if (null!=body.getError())
            error = body.getError();

        if (null!=body.getSuccess())
            success = body.getSuccess();

        if (!TextUtils.isEmpty(success) && TextUtils.isEmpty(error))
            successOrError = success;
        else
            successOrError = error;

        Log.d(Config.TAGKEY,   " - doAfterSetFlyerServer -  successOrError: " + successOrError  );
        Log.d(Config.TAGKEY,   " - doAfterSetFlyerServer -  flyersLists: " + flyersLists.toString()  );
        if (null!=flyersLists.get(position) && null!=flyersLists.get(position).getImage()) {
            saveHiddenFieldImageView = flyersLists.get(position).getImage();
            fillImageView();
        }

        // Afficher message de retour
        Utils.showSnackLong(coordinatorLayout, successOrError);


    }

    private void doActionAfterUploadedFile(String filename) {

        if (imageView==null || buttonUploadFlyer==null)
            return;

        // Supprimer opacite image
        imageView.setAlpha(1.0f);

        // Masquer le bouton
        buttonUploadFlyer.setVisibility(View.GONE);

        // Masquer progress bar
        pgHorizontal.setVisibility(View.GONE);

        //
        txtUploadFlyer.setVisibility(View.VISIBLE);
        txtUploadFlyer.setText(getString(R.string.txtUploadFlyer, filename));
        hiddenFieldImageView.setText( filename );
    }
    private void doActionAfterUploadedFile(String filename, int position) {
        Log.d(Config.TAGKEY,   " - doActionAfterUploadedFile "+filename+" - position : "+position );
        Log.d(Config.TAGKEY,   " - doActionAfterUploadedFile "+flyersTAGIDsArray  );
        Log.d(Config.TAGKEY,   " - doActionAfterUploadedFile flyersTAGImagesArray before "+flyersTAGImagesArray  );

        // Call addFlyerToEvent(@Query("filename") String filename, @Query("eventid") int eventid)
        doAddFlyerToEvent(filename,position);


    }

    private void doAddFlyerToEvent(final String filename,final int position) {

        Log.d(Config.TAGKEY,   " - doAddFlyerToEvent");

        Call<GenericResultRetrofit> call = apiService.addFlyerToEvent(filename, eventId);

        call.enqueue(new Callback<GenericResultRetrofit>() {
            @Override
            public void onResponse(Call<GenericResultRetrofit> call, Response<GenericResultRetrofit> response) {
                Log.d(Config.TAGKEY,   " - uploadFlyer - onResponse - " + response.code() + " - " + response.message() + " - URLS : " + call.request().url());

                if (200!=response.code()) {
                    return;
                }

                flyersTAGIDsArray.set(position,response.body().getId());
                flyersTAGImagesArray.set(position,filename);

                // flyersLists.get(position).setUri(false);

                Log.d(Config.TAGKEY,   " - doAddFlyerToEvent flyersTAGImagesArray after "+flyersTAGImagesArray  );

                EventFlyersRetrofit eventFlyersRetrofit = new EventFlyersRetrofit();
                eventFlyersRetrofit.setImage(response.body().getName());
                eventFlyersRetrofit.setUri(false);
                eventFlyersRetrofit.setTmp(false);
                eventFlyersRetrofit.setId(response.body().getId());

                // Ajouter dans le adapter du recyclerview
                doRefreshAdapter(eventFlyersRetrofit, position);

            }

            @Override
            public void onFailure(Call<GenericResultRetrofit> call, Throwable t) {
                Log.d(Config.TAGKEY,   " - uploadFlyer - onFailure - URLS : " + call.request().url()+" - t "+t.getMessage());
                Utils.showSnackLong(coordinatorLayout, getString(R.string.txtErrorServer));
            }
        });



    }

    /**
     * Remplir Image View
     */
    private void fillImageView() {
        Log.d(Config.TAGKEY,  " - fillImageView " );
        if (TextUtils.isEmpty(saveHiddenFieldImageView) && mimageUri != null) {
            Log.d(Config.TAGKEY,  " - fillImageView mimageUri : "+mimageUri.toString() );
            // Update UI to reflect image being shared
            imageView.setImageURI(mimageUri);
            buttonUploadFlyer.setVisibility(View.VISIBLE);
        }

        if (!TextUtils.isEmpty(saveHiddenFieldImageView) ) {
            Log.d(Config.TAGKEY,  " - fillImageView saveHiddenFieldImageView: "+saveHiddenFieldImageView );
            buttonUploadFlyer.setVisibility(View.GONE);

            // Supprimer opacite image
            imageView.setAlpha(1.0f);

            Glide.
                    with(getActivity())
                    .load( PATH_FLYERS+saveHiddenFieldImageView )
                    .into( imageView );

        }

        if (!TextUtils.isEmpty(saveHiddenFieldImageView) && mimageUri != null ) {
            Log.d(Config.TAGKEY,  " - fillImageView saveHiddenFieldImageView: "+saveHiddenFieldImageView+" - mimageUri: "+mimageUri.toString() );
            flyersTAGIDsArray.add(Utils.getRandomNumberInRange(5,20));
            flyersTAGImagesArray.add(mimageUri.toString());
            // doRefreshAdapter(new EventFlyersRetrofit(true, mimageUri.toString()));
            doRefreshAdapter(new EventFlyersRetrofit(true, mimageUri.toString(), mimageUri));
        }
    }

    private List<EventLocalRetrofit> getSearchList(String s) {

        Call<List<EventLocalRetrofit>> servApi = apiService.getAllLocal(s);
        Log.d(Config.TAGKEY,  " - getResultAutoc URL : "+ servApi.request().url() );

        try {
            List<EventLocalRetrofit> EventlieuRetrofits = servApi.execute().body() ;
            return EventlieuRetrofits;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
