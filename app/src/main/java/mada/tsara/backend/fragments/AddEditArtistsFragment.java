package mada.tsara.backend.fragments;

import android.app.Dialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.joanzapata.iconify.widget.IconButton;
import com.joanzapata.iconify.widget.IconTextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import mada.tsara.backend.R;
import mada.tsara.backend.adapters.ArtistsAddEditEnfantsRecyclerviewAdapter;
import mada.tsara.backend.adapters.ArtistsAddEditParentsRecyclerviewAdapter;
import mada.tsara.backend.adapters.AutocompleteArtistsAdapter;
import mada.tsara.backend.adapters.SpinArtistetypeAdapter;
import mada.tsara.backend.customview.ClickListener;
import mada.tsara.backend.customview.RecyclerTouchListener;
import mada.tsara.backend.models.EventArtistesDjOrganisateurRetrofit;
import mada.tsara.backend.models.GenericResultRetrofit;
import mada.tsara.backend.service.RetrofitApi;
import mada.tsara.backend.service.RetrofitInterface;
import mada.tsara.backend.share.BusEvent;
import mada.tsara.backend.share.Config;
import mada.tsara.backend.share.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static mada.tsara.backend.R.id.editArtist;
import static mada.tsara.backend.R.id.relativeNoDataArtists;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AddEditArtistsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AddEditArtistsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddEditArtistsFragment extends Fragment {

    static String className = "AddEditArtistsFragment";

    private static final String ITEMID = "itemId";
    private static final String ARTISTE = "artisteName";


    int itemId;
    String artisteName = null;

    private OnFragmentInteractionListener mListener;

    // UI
    View rootView;

    ImageView imageView;

    IconButton btnSaveGeneric;

    IconTextView icontextview_close;

    ProgressBar progressBarSave;

    CoordinatorLayout coordinatorLayout;

    Spinner spinnType;

    SpinArtistetypeAdapter spinnAdapter;

    RetrofitInterface apiService = RetrofitApi.getClient().create(RetrofitInterface.class);

    AutoCompleteTextView editArtistsParents;
    AutoCompleteTextView editArtistsEnfants;

    EditText editName;

    AutocompleteArtistsAdapter autocompleteArtistsParentsAdapter;
    AutocompleteArtistsAdapter autocompleteArtistsEnfantsAdapter;

    ArrayList<Integer> artistsParentsIDsArray = new ArrayList<Integer>();
    ArrayList<Integer> artistsEnfantsIDsArray = new ArrayList<Integer>();


    // Saved Instance
    String fieldname;
    String fieldType;
    String fieldImageView;
    String typeArtisteDefault;
    List<EventArtistesDjOrganisateurRetrofit> fieldArtistEnfants;
    List<EventArtistesDjOrganisateurRetrofit> fieldArtistParents;

    Realm realm;

    RecyclerView recyclerviewArtistsEnfants;
    RecyclerView recyclerviewArtistsParents;

    List<EventArtistesDjOrganisateurRetrofit> artistsParentsLists  = new ArrayList<EventArtistesDjOrganisateurRetrofit>();
    List<EventArtistesDjOrganisateurRetrofit> artistsEnfantsLists  = new ArrayList<EventArtistesDjOrganisateurRetrofit>();

    ArtistsAddEditParentsRecyclerviewAdapter adapterArtistsParents = new ArtistsAddEditParentsRecyclerviewAdapter(getActivity(),artistsParentsLists, R.layout.adapter_artists);
    ArtistsAddEditEnfantsRecyclerviewAdapter adapterArtistsEnfants = new ArtistsAddEditEnfantsRecyclerviewAdapter(getActivity(),artistsEnfantsLists, R.layout.adapter_artists);

    boolean isEditMode = false;
    ProgressBar loader;

    public AddEditArtistsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AddEditArtistsFragment.
     */

    public static AddEditArtistsFragment newInstance(int itemId,String artisteName) {
        Log.d(Config.TAGKEY,  className+" - STEP - AddEditArtistsFragment newInstance - itemId "+itemId+" - artisteName: "+artisteName);
        AddEditArtistsFragment fragment = new AddEditArtistsFragment();
        Bundle args = new Bundle();
        args.putInt(ITEMID, itemId);
        args.putString(ARTISTE, artisteName);
        fragment.setArguments(args);
        return fragment;
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(Config.TAGKEY,  className+" - STEP - onCreate");
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            itemId = getArguments().getInt(ITEMID);
            artisteName = getArguments().getString(ARTISTE);
            Log.d(Config.TAGKEY,  className+" - STEP - onCreate - itemId: "+itemId+" - artisteName: "+artisteName);


        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(Config.TAGKEY,  className+" - STEP - onCreateView");
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_add_edit_artists, container, false);
        return rootView;
    }


    public void onButtonPressed(Uri uri) {
        Log.d(Config.TAGKEY,  className+" - STEP - onButtonPressed");
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        Log.d(Config.TAGKEY,  className+" - STEP - onAttach");
        super.onAttach(context);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDetach() {
        Log.d(Config.TAGKEY,  className+" - STEP - onDetach");
        super.onDetach();
        mListener = null;
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void onEvent(BusEvent event) {
        Log.d(Config.TAGKEY, className+" - STEP - onEvent" );
        if ( event.getParentBackPressed() ) {

        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        Log.d(Config.TAGKEY,  className+" - onActivityCreated");
        super.onActivityCreated(savedInstanceState);

        if ( savedInstanceState != null ) {
            Log.d(Config.TAGKEY,  className+" - onActivityCreated savedInstanceState not null");
        }

        // Initialiser les vues
        initUi();


        doSetUi();

        // Remplir si existence mode config change
        doFillUiWithInstancedState();

        // En mode edition de l'item
        doFillUiWhenEditMode();
    }

    private void initUi() {

        // Logo ou image
        imageView = (ImageView) rootView.findViewById(R.id.sentImage);

        btnSaveGeneric = (IconButton) rootView.findViewById(R.id.btnSaveGeneric);

        progressBarSave = (ProgressBar) rootView.findViewById(R.id.progressBarSave);

        coordinatorLayout = (CoordinatorLayout) rootView.findViewById(R.id.coordinatorLayout);

        spinnType = (Spinner) rootView.findViewById(R.id.spinnType);

        editArtistsParents = (AutoCompleteTextView) rootView.findViewById(R.id.editArtistsParents);
        editArtistsEnfants = (AutoCompleteTextView) rootView.findViewById(R.id.editArtistsEnfants);

        editName  = (EditText) rootView.findViewById(R.id.editName);

        autocompleteArtistsEnfantsAdapter = new AutocompleteArtistsAdapter(getActivity());
        autocompleteArtistsParentsAdapter = new AutocompleteArtistsAdapter(getActivity());

        recyclerviewArtistsParents = (RecyclerView) rootView.findViewById(R.id.recyclerviewArtistsParents);
        recyclerviewArtistsEnfants = (RecyclerView) rootView.findViewById(R.id.recyclerviewArtistEnfants);

        loader = (ProgressBar) rootView.findViewById(R.id.loader);

        icontextview_close = (IconTextView) rootView.findViewById(R.id.icontextview_close);


    }

    private void doSetUi() {
        // Click image
        doSetOnClickImage();

        // Fill Spinner Type artiste
        doFillSpinnerTypeArtiste();

        // Autocomplete ArtistsParents
        doFillAutoCompleteEditArtists(editArtistsParents, autocompleteArtistsParentsAdapter, artistsParentsIDsArray, artistsParentsLists, adapterArtistsParents, recyclerviewArtistsParents);

        // Autocomplete ArtistsEnfants
        doFillAutoCompleteEditArtists(editArtistsEnfants, autocompleteArtistsEnfantsAdapter, artistsEnfantsIDsArray,  artistsEnfantsLists, adapterArtistsEnfants, recyclerviewArtistsEnfants);

        fillImageView();

        // Clic save
        //doClickSave();

        // recycler Parents
        doSetRecyclerViewArtists(recyclerviewArtistsParents, artistsParentsLists , artistsParentsIDsArray, adapterArtistsParents);

        // recycler Enfants
         doSetRecyclerViewArtists(recyclerviewArtistsEnfants, artistsEnfantsLists, artistsEnfantsIDsArray, adapterArtistsEnfants);

        // Save
        btnSaveGeneric.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doValidform();
            }
        });

        if (null!=icontextview_close) {
            icontextview_close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // Fermer l'activity
                    BusEvent busEvent = new BusEvent();
                    busEvent.setFinishPopupActivity(true);
                    EventBus.getDefault().post(busEvent);
                }
            });
        }

    }

    private void doSaveItem(String fieldName, String fieldType, String fieldArtistsEnfants, String fieldArtistsParents) {

        Log.d(Config.TAGKEY," - doSaveEventServer " );

        doDisableAllFields();

        // Call webservice
        Call<GenericResultRetrofit> call = apiService.createArtists(
                fieldName,
                fieldType,
                "",
                "",
                fieldArtistsParents,
                fieldArtistsEnfants,
                ""
        );

        if (isEditMode) {
            call = apiService.updateArtists(
                    fieldName,
                    fieldType,
                    "",
                    "",
                    fieldArtistsParents,
                    fieldArtistsEnfants,
                    "",
                    itemId
            );
        }
        call.enqueue(new Callback<GenericResultRetrofit>() {
            @Override
            public void onResponse(Call<GenericResultRetrofit> call, Response<GenericResultRetrofit> response) {
                Log.d(Config.TAGKEY,   " - uploadFlyer - onResponse - " + response.code() + " - " + response.message() + " - URLS : " + call.request().url());

                if (200!=response.code()) {
                    doEnableAllFields();
                    return;
                }

                doAfterSaveServer(response.body());
            }

            @Override
            public void onFailure(Call<GenericResultRetrofit> call, Throwable t) {
                Log.d(Config.TAGKEY,   " - uploadFlyer - onFailure - URLS : " + call.request().url()+" - t "+t.getMessage());
                Utils.showSnackLong(coordinatorLayout, getString(R.string.txtErrorServer));
            }
        });

    }

    private void doAfterSaveServer(GenericResultRetrofit body) {

        String error = body.getError();
        String success = body.getSuccess();
        String name = body.getName();
        int id = body.getId();


        Log.d(Config.TAGKEY,   " - doAfterSaveServer -  getError: " + error+" - getSuccess: "+success  );

        if (error!=null) {
            return;
        }

        doEnableAllFields();

        Utils.showSnackLong(coordinatorLayout, getString(R.string.txtConfirmEntitySaved));
        
        doPushEvent(id,name);
    }

    private void doPushEvent(int id, String name) {

        Log.d(Config.TAGKEY," - doPushEvent ");

        BusEvent busEvent = new BusEvent();
        busEvent.setResultActivity(id,name);
        EventBus.getDefault().post(busEvent);
    }

    private void doSetRecyclerViewArtists(RecyclerView recyclerView, final List<EventArtistesDjOrganisateurRetrofit> artistsLists,  final ArrayList<Integer> integerArrayList, final ArtistsAddEditParentsRecyclerviewAdapter recyclerviewAdapter) {
        Log.d(Config.TAGKEY,  className+" - doSetRecyclerViewArtists "+getActivity());
        Log.d(Config.TAGKEY,  className+" - doSetRecyclerViewArtists "+artistsLists);
        Log.d(Config.TAGKEY,  className+" - doSetRecyclerViewArtists "+integerArrayList);


        recyclerView.setLayoutManager( new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        recyclerView.setAdapter(recyclerviewAdapter);

        // Onclick
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new ClickListener() {
            @Override
            public void onClick(View view,final int position) {
                // Utils.showSnackLong(coordinatorLayout,"Clic sur item");

                // Clic sur Fermer
                IconTextView iconDeleteT = (IconTextView) view.findViewById(R.id.iconDeleteT);
                if ( null==iconDeleteT)
                    return;


                if (null!=iconDeleteT ) {
                    iconDeleteT.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Utils.showSnackLong(coordinatorLayout,"Clic sur close");
                            doRemoveByPosition(position, integerArrayList, recyclerviewAdapter);
                        }
                    });
                }


            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }
    private void doSetRecyclerViewArtists(RecyclerView recyclerView, final List<EventArtistesDjOrganisateurRetrofit> artistsLists,  final ArrayList<Integer> integerArrayList, final ArtistsAddEditEnfantsRecyclerviewAdapter recyclerviewAdapter) {
        Log.d(Config.TAGKEY,  className+" - doSetRecyclerViewArtists "+getActivity());
        Log.d(Config.TAGKEY,  className+" - doSetRecyclerViewArtists "+artistsLists);
        Log.d(Config.TAGKEY,  className+" - doSetRecyclerViewArtists "+integerArrayList);


        recyclerView.setLayoutManager( new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        recyclerView.setAdapter(recyclerviewAdapter);

        // Onclick
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new ClickListener() {
            @Override
            public void onClick(View view,final int position) {
                // Utils.showSnackLong(coordinatorLayout,"Clic sur item");

                // Clic sur Fermer
                IconTextView iconDeleteT = (IconTextView) view.findViewById(R.id.iconDeleteT);
                if ( null==iconDeleteT)
                    return;


                if (null!=iconDeleteT ) {
                    iconDeleteT.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Utils.showSnackLong(coordinatorLayout,"Clic sur close");
                            doRemoveByPosition(position, integerArrayList, recyclerviewAdapter);
                        }
                    });
                }


            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }
    private void doRemoveByPosition(int position,ArrayList<Integer> idsArray, ArtistsAddEditEnfantsRecyclerviewAdapter adapter) {
        idsArray.remove(position);
        adapter.removeAt(position);
    }

    private void doRemoveByPosition(int position,ArrayList<Integer> idsArray, ArtistsAddEditParentsRecyclerviewAdapter adapter) {
        idsArray.remove(position);
        adapter.removeAt(position);
    }


    private void doFillSpinnerTypeArtiste() {
        spinnAdapter = new SpinArtistetypeAdapter( getActivity() , Utils.getGenericArtisteType() );
        spinnType.setAdapter(spinnAdapter);

        // Selected par defaut
        Handler handler = new Handler();
        Runnable runnable;
        runnable = new Runnable() {
            @Override
            public void run() {

                Log.d(Config.TAGKEY,   " - doFillSpinnerTypeArtiste "+spinnAdapter.getCount());
                if (!isEditMode && spinnAdapter.getCount()>1) {
                    // Selectionner artiste par defaut
                    spinnType.setSelection(1);
                }
            }
        };
        handler.postDelayed(runnable,1000);
    }

    private void doSetOnClickImage() {
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // doOpenFullscreenImage(mimageUri,Config.PATH_FLYERS+saveHiddenFieldImageView);
            }
        });
    }

    /**
     * Auto completion lieux
     */
    private void doFillAutoCompleteEditArtists(AutoCompleteTextView view, AutocompleteArtistsAdapter autocadapter, ArrayList<Integer> integerArrayList, List<EventArtistesDjOrganisateurRetrofit> lists, ArtistsAddEditEnfantsRecyclerviewAdapter adapter, RecyclerView recyclerview ) {
        Log.d(Config.TAGKEY,  className+" - doFillAutoCompleteEditArtists " );
        view.setAdapter(autocadapter);

        // Au clic sur un item dans la liste proposee
        doOnsetItemClickEditArtists(view, integerArrayList, lists, adapter, recyclerview);
    }

    /**
     * Auto completion lieux
     */
    private void doFillAutoCompleteEditArtists(AutoCompleteTextView view, AutocompleteArtistsAdapter autocadapter, ArrayList<Integer> integerArrayList, List<EventArtistesDjOrganisateurRetrofit> lists, ArtistsAddEditParentsRecyclerviewAdapter adapter, RecyclerView recyclerview ) {
        Log.d(Config.TAGKEY,  className+" - doFillAutoCompleteEditArtists " );
        view.setAdapter(autocadapter);

        // Au clic sur un item dans la liste proposee
        doOnsetItemClickEditArtists(view, integerArrayList, lists, adapter, recyclerview);
    }


    /**
     * Remplir Image View
     */
    private void fillImageView() {
        Log.d(Config.TAGKEY,  className+" - fillImageView - isAdded(): " +isAdded() );
        if (!isAdded())
            return;

        if (!TextUtils.isEmpty(fieldImageView) ) {
            Log.d(Config.TAGKEY,  className+" - fillImageView "+fieldImageView );

            Glide.
                    with(getActivity())
                    .load( Config.PATH_FLYERS+fieldImageView )
                    .into( imageView );

        }
    }

    /**
     * Au clic sur un item dans auto completion lieu
     */
    private void doOnsetItemClickEditArtists(final AutoCompleteTextView autoCompleteTextView, final ArrayList<Integer> integerArrayList, final List<EventArtistesDjOrganisateurRetrofit> lists, final ArtistsAddEditEnfantsRecyclerviewAdapter adapter, final RecyclerView recyclerview) {
        Log.d(Config.TAGKEY, className+" - doOnsetItemClickEditArtists" );
        autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Log.d(Config.TAGKEY, className+" - doOnsetItemClickEditArtists - integerArrayList : "+integerArrayList );

                EventArtistesDjOrganisateurRetrofit item = (EventArtistesDjOrganisateurRetrofit) parent.getItemAtPosition(position);

                if (!integerArrayList.contains(item.getId())) {
                    Log.d(Config.TAGKEY, className+" - doOnsetItemClickEditArtists add "+item.getId() );
                    Log.d(Config.TAGKEY, className+" - doOnsetItemClickEditArtists lists "+lists );
                    Log.d(Config.TAGKEY, className+" - doOnsetItemClickEditArtists adapter "+adapter );
                    Log.d(Config.TAGKEY, className+" - doOnsetItemClickEditArtists recyclerview "+recyclerview );

                    integerArrayList.add(item.getId());

                    // Ajouter dans le adapter du recyclerview
                    doRefreshAdapter(item, lists, adapter, recyclerview);


                } else {
                    Utils.showSnackLong(coordinatorLayout, getString(R.string.txtErrorLieuExistsIntag));
                    // Toast.makeText(TsaraApplication.getContext(),TsaraApplication.getContext().getString(R.string.txtv_error_artists_exists_intag),Toast.LENGTH_LONG).show();
                }

                autoCompleteTextView.setText("");



                autoCompleteTextView.setSelection(autoCompleteTextView.getText().length());
            }
        });
    }

    /**
     * Au clic sur un item dans auto completion lieu
     */
    private void doOnsetItemClickEditArtists(final AutoCompleteTextView autoCompleteTextView, final ArrayList<Integer> integerArrayList, final List<EventArtistesDjOrganisateurRetrofit> lists, final ArtistsAddEditParentsRecyclerviewAdapter adapter, final RecyclerView recyclerview) {
        Log.d(Config.TAGKEY, className+" - doOnsetItemClickEditArtists" );
        autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Log.d(Config.TAGKEY, className+" - doOnsetItemClickEditArtists - integerArrayList : "+integerArrayList );

                EventArtistesDjOrganisateurRetrofit item = (EventArtistesDjOrganisateurRetrofit) parent.getItemAtPosition(position);

                if (!integerArrayList.contains(item.getId())) {
                    Log.d(Config.TAGKEY, className+" - doOnsetItemClickEditArtists add "+item.getId() );
                    Log.d(Config.TAGKEY, className+" - doOnsetItemClickEditArtists lists "+lists );
                    Log.d(Config.TAGKEY, className+" - doOnsetItemClickEditArtists adapter "+adapter );
                    Log.d(Config.TAGKEY, className+" - doOnsetItemClickEditArtists recyclerview "+recyclerview );

                    integerArrayList.add(item.getId());

                    // Ajouter dans le adapter du recyclerview
                    doRefreshAdapter(item, lists, adapter, recyclerview);


                } else {
                    Utils.showSnackLong(coordinatorLayout, getString(R.string.txtErrorLieuExistsIntag));
                    // Toast.makeText(TsaraApplication.getContext(),TsaraApplication.getContext().getString(R.string.txtv_error_artists_exists_intag),Toast.LENGTH_LONG).show();
                }

                autoCompleteTextView.setText("");



                autoCompleteTextView.setSelection(autoCompleteTextView.getText().length());
            }
        });
    }

    private void doRefreshAdapter(EventArtistesDjOrganisateurRetrofit item, List<EventArtistesDjOrganisateurRetrofit> lists, ArtistsAddEditEnfantsRecyclerviewAdapter adapter, RecyclerView recyclerview) {
        Log.d(Config.TAGKEY,  className+" - doRefreshAdapter");
        Log.d(Config.TAGKEY,  className+" - doRefreshAdapter "+item.getName());
        Log.d(Config.TAGKEY,  className+" - doRefreshAdapter "+recyclerview);


        if (null!=lists)
            lists.add(item);

        if (null!=adapter)
            adapter.notifyDataSetChanged();

        // Afficher le recyclerview
        if (null!=recyclerview)
            recyclerview.setVisibility(View.VISIBLE);

    }

    private void doRefreshAdapter(EventArtistesDjOrganisateurRetrofit item, List<EventArtistesDjOrganisateurRetrofit> lists, ArtistsAddEditParentsRecyclerviewAdapter adapter, RecyclerView recyclerview) {
        Log.d(Config.TAGKEY,  className+" - doRefreshAdapter");
        Log.d(Config.TAGKEY,  className+" - doRefreshAdapter "+item.getName());
        Log.d(Config.TAGKEY,  className+" - doRefreshAdapter "+recyclerview);


        if (null!=lists)
            lists.add(item);

        if (null!=adapter)
            adapter.notifyDataSetChanged();

        // Afficher le recyclerview
        if (null!=recyclerview)
            recyclerview.setVisibility(View.VISIBLE);

    }

    private void doFillUiWithInstancedState() {

        doSetDefaultValues();

        if (!TextUtils.isEmpty(artisteName))
            fieldname = artisteName;

        Log.d(Config.TAGKEY,  className+" - doFillUiWithInstancedState");
        Log.d(Config.TAGKEY,  className+" - doFillUiWithInstancedState - fieldname: "+fieldname);
        Log.d(Config.TAGKEY,  className+" - doFillUiWithInstancedState - fieldType: "+fieldType);
        Log.d(Config.TAGKEY,  className+" - doFillUiWithInstancedState - fieldArtistEnfants: "+fieldArtistEnfants);
        Log.d(Config.TAGKEY,  className+" - doFillUiWithInstancedState - fieldArtistParents: "+fieldArtistParents);


        if (!TextUtils.isEmpty(fieldname))
            editName.setText(fieldname);

        if (!TextUtils.isEmpty(fieldType) && spinnAdapter.getCount()>0) {
            spinnType.setSelection(spinnAdapter.getPositionByItem( fieldType ));
        }

        fillImageView();

        if (null!=fieldArtistEnfants) {
            for (EventArtistesDjOrganisateurRetrofit artist: fieldArtistEnfants ) {
                if (!artistsEnfantsIDsArray.contains(artist.getId())) {

                    artistsEnfantsIDsArray.add(artist.getId());

                    // Ajouter dans le adapter du recyclerview
                    doRefreshAdapter(artist, artistsEnfantsLists, adapterArtistsEnfants, recyclerviewArtistsEnfants);

                }
            }
        }
        if (null!=fieldArtistParents) {
            for (EventArtistesDjOrganisateurRetrofit artist: fieldArtistParents ) {
                if (!artistsParentsIDsArray.contains(artist.getId())) {

                    artistsParentsIDsArray.add(artist.getId());

                    // Ajouter dans le adapter du recyclerview
                    doRefreshAdapter(artist, artistsParentsLists, adapterArtistsParents, recyclerviewArtistsParents);

                }
            }
        }
    }

    private void doSetDefaultValues() {
        if (isEditMode)
            return;

        // Type d'artiste par defaut
        typeArtisteDefault = Config.ARTISTETYPE; 
    }

    private void doValidform() {

        // Nom event rempli?
        final String fieldName = editName.getText().toString();

        // String fieldThematique = spinnThematique.getSelectedItem().toString();
        int selectedType = Integer.parseInt(spinnType.getSelectedItem().toString());
        final String fieldType = spinnAdapter.getItemByPosition(selectedType).getValue();

        String fieldArtistsEnfants = TextUtils.join(",", artistsEnfantsIDsArray );
        String fieldArtistsParents = TextUtils.join(",", artistsParentsIDsArray );



        // Les champs obligatoires sont remplies ? fieldName, fieldThematique, fiedAccess, fieldDate
        int nbFieldInvalid = 0;
        nbFieldInvalid += TextUtils.isEmpty(fieldName)?1:0;
        nbFieldInvalid += TextUtils.isEmpty(fieldType)?1:0;

        Log.d(Config.TAGKEY,"FORM begin");
        Log.d(Config.TAGKEY,"FORM fieldName : "+fieldName);
        Log.d(Config.TAGKEY,"FORM fieldType : "+fieldType);
        Log.d(Config.TAGKEY,"FORM fieldArtistsEnfants : "+fieldArtistsEnfants);
        Log.d(Config.TAGKEY,"FORM fieldArtistsParents : "+fieldArtistsParents);
        Log.d(Config.TAGKEY,"FORM nbFieldInvalid : "+nbFieldInvalid);
        Log.d(Config.TAGKEY,"FORM itemId : "+itemId);
        Log.d(Config.TAGKEY,"FORM isEditMode? : "+isEditMode);
        Log.d(Config.TAGKEY,"FORM end");


        // Des champs ne sont pas remplis
        if (nbFieldInvalid>0) {
            final int finalNbFieldInvalid = nbFieldInvalid;
            Utils.showSnackLong(coordinatorLayout, getString(R.string.txtErrorFieldsEmpty),getString(R.string.txtMore), new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d(Config.TAGKEY, " - showSnackLong - OnClickListener ");
                    ArrayList<String> errorFields = new ArrayList<String>();
                    if (TextUtils.isEmpty(fieldName)) {
                        errorFields.add("Titre");
                    }
                    if (TextUtils.isEmpty(fieldType)) {
                        errorFields.add("Thematique");
                    }

                    doOpenDialogError(finalNbFieldInvalid,errorFields);
                }
            });
            return;
        }

        // Continuer si erreur vide

        doSaveItem(fieldName,fieldType ,fieldArtistsEnfants ,fieldArtistsParents );



    }

    private void doOpenDialogError(int finalNbFieldInvalid, ArrayList<String> errorFields) {
        Log.d(Config.TAGKEY," - doOpenDialogError " );
        Log.d(Config.TAGKEY," - doOpenDialogError - "+TextUtils.join(", ",errorFields) );

        final Dialog dDialog = new Dialog(getActivity());
        dDialog.setContentView(R.layout.dialog_info);

        final TextView textView_name = (TextView) dDialog.findViewById(R.id.textView_name);

        final IconButton iconCancel = (IconButton) dDialog.findViewById(R.id.iconCancel);

        textView_name.setText(getString(R.string.txtErrorDialogFormEvent, TextUtils.join(", ",errorFields)));

        iconCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dDialog.dismiss();
            }
        });

        dDialog.show();
    }

    private void doFillUiWhenEditMode() {
        Log.d(Config.TAGKEY,  className+" - doFillUiWhenEditMode");

        if (isEditMode)
            return;

        if (!TextUtils.isEmpty(artisteName))
            return;

        doDisableAllFields();

        if (itemId==0)
            return;


        Handler handler = new Handler();
        Runnable runnable;

        runnable = new Runnable() {
            @Override
            public void run() {
                Log.d(Config.TAGKEY,className+" - doFillUiWhenEditMode in Runnable ");
                // Recup infos sur le serveur
                getItemServer(itemId);

            }
        };
        handler.postDelayed(runnable,3000);
    }
    private void getItemServer(int eventid) {
        Log.d(Config.TAGKEY,  className+" - getItemServer");
        if (isEditMode)
            return;



        //Events de la semaine
        final Call<EventArtistesDjOrganisateurRetrofit> webservice = apiService.getArtiste( eventid );
        webservice.enqueue(new Callback<EventArtistesDjOrganisateurRetrofit>() {
            @Override
            public void onResponse(Call<EventArtistesDjOrganisateurRetrofit> call, Response<EventArtistesDjOrganisateurRetrofit> response) {
                Log.d(Config.TAGKEY, className+" - STEP - getItemServer - onResponse - "+response.code()+" - "+response.message()+" - URLS : "+call.request().url()  );

                if ( response.code() != 200 ) {
                    return;
                }

                EventArtistesDjOrganisateurRetrofit item = response.body() ;

                doSetFieldsByServerResults(item);


            }

            @Override
            public void onFailure(Call<EventArtistesDjOrganisateurRetrofit> call, Throwable t) {
                Log.d(Config.TAGKEY,  className+" - STEP - getItemServer - onFailure - message : "+t.getMessage()+" - URLS : "+call.request().url() );
                // checkRealmAndFill();
            }
        });

    }

    private void doEnableAllFields() {

        if (null!=editName)
            editName.setEnabled(true);

        if (null!=spinnType)
            spinnType.setEnabled(true);

        if (null!=editArtistsEnfants)
            editArtistsEnfants.setEnabled(true);

        if (null!=editArtistsParents)
            editArtistsParents.setEnabled(true);

        if (null!=loader)
            loader.setVisibility(View.GONE);

    }

    private void doDisableAllFields() {

        if (null!=editName)
            editName.setEnabled(false);

        if (null!=spinnType)
            spinnType.setEnabled(false);

        if (null!=editArtistsEnfants)
            editArtistsEnfants.setEnabled(false);

        if (null!=editArtistsParents)
            editArtistsParents.setEnabled(false);

        if (null!=loader)
            loader.setVisibility(View.VISIBLE);

    }

    private void doSetFieldsByServerResults(EventArtistesDjOrganisateurRetrofit item) {

        Log.d(Config.TAGKEY,  className+" - doSetFieldsByServerResults");

        isEditMode = true;

        if (null!=item.getName())
            fieldname = item.getName();

        if (null!=item.getType())
            fieldType = item.getType();

        if (null!=item.getPhoto())
            fieldImageView = item.getPhoto();

        if (null!=item.getEnfantArtiste())
            fieldArtistEnfants = item.getEnfantArtiste();

        if (null!=item.getParentArtiste())
            fieldArtistParents = item.getParentArtiste();

        doEnableAllFields();

        doFillUiWithInstancedState();
    }
}
