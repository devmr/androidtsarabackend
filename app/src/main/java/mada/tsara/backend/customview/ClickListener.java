package mada.tsara.backend.customview;

import android.view.View;

/**
 * Created by Miary on 28/06/2017.
 */

public interface ClickListener{
    public void onClick(View view, int position);
    public void onLongClick(View view,int position);
}
