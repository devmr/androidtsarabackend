package mada.tsara.backend.adapters;

import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import mada.tsara.backend.R;
import mada.tsara.backend.models.EventLocalRetrofit;
import mada.tsara.backend.share.Config;

/**
 * Created by Miary on 27/06/2017.
 */

public class FeteRecyclerviewAdapter extends SelectableRecyclerviewAdapter<RecyclerView.ViewHolder> {
    String className = "EventLocalAdapter";

    //Views
    final int DEFAULT_V = 0;

    private int rowLayout;

    private View view;

    private static List<EventLocalRetrofit> items;
    private static FragmentActivity activity;

    protected int serverListSize = -1;
    protected int loadedSize = -1;


    public FeteRecyclerviewAdapter(FragmentActivity activity, List<EventLocalRetrofit> items, int rowLayout  ) {
        Log.d(Config.TAGKEY, className+" - rowLayout : "+rowLayout );
        this.items = items;
        this.activity = activity;
        this.rowLayout = rowLayout;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.d(Config.TAGKEY, className+" - onCreateViewHolder - viewType : "+viewType );

        view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new ViewHolderDefault(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        Log.d(Config.TAGKEY, className+" - onBindViewHolder - position :  "+position + " - eventType.size() : "+items.size()+" - holder : "+holder );
        if(items.size()>0 && holder instanceof ViewHolderDefault && null!=items.get(position)){
            final EventLocalRetrofit ev = items.get(position);
            Log.d(Config.TAGKEY, className+" - onBindViewHolder - position :  "+position + " - instanceof ViewHolderDefault "+ev.getName()  );

            TextView textName = ( (ViewHolderDefault) holder).name;

            if (null!=textName && !TextUtils.isEmpty(ev.getName())) {
                textName.setText(ev.getName());
            }


        }
    }

    @Override
    public int getItemCount() {
        int nb =  items == null ? 0 : items.size()  ;
        Log.d(Config.TAGKEY, className+" - getItemCount -  " +nb  );
        return nb;
    }

    public boolean idInAdapter(int id) {
        Log.d(Config.TAGKEY, className+" - idInAdapter " +id + " - eventType.size(): " + items.size() );
        for( int i = 0; i < items.size(); i++ ) {
            if ( items.get(i).getId().equals(id) ) {
                Log.d(Config.TAGKEY, className+" - idInAdapter yes - " +items.get(i).getId() );
                return true;
            }
        }
        return false;
    }
    public void setLoadedSize(int loadedSize){
        Log.d(Config.TAGKEY, className+" - setLoadedSize - " +loadedSize );
        this.loadedSize = loadedSize;
    }
    public void setServerListSize(int serverListSize){
        Log.d(Config.TAGKEY, className+" - setServerListSize - " +serverListSize );
        this.serverListSize = serverListSize;
    }

    public void removeAt(int position) {
        items.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(0, items.size());
    }

    /**
     * ViewHolder
     */
    public static class ViewHolderDefault extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView name;
        public View overflow;

        public LinearLayout fond;

        public View selectedOverlay;
        private ClickListener listener;

        String className = "EventLocalAdapter > ViewHolderDefault";

        public ViewHolderDefault(View convertView) {

            super(convertView);
            Log.d(Config.TAGKEY, className+" - ViewHolderDefault" );

            //fond = (LinearLayout) convertView.findViewById(R.id.fond);

            name = (TextView) convertView.findViewById(R.id.name);



        }

        @Override
        public void onClick(View v) {
            Log.d(Config.TAGKEY, className+" - onClick" );
            if (listener != null) {
                listener.onItemClicked(getLayoutPosition());
            }

        }

        public interface ClickListener {
            public void onItemClicked(int position);
        }
    }
}
