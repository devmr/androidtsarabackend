package mada.tsara.backend.adapters;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import mada.tsara.backend.R;
import mada.tsara.backend.TsaraApplication;
import mada.tsara.backend.customview.CircleImageView;
import mada.tsara.backend.models.AllEventArtistesDjOrganisateurRetrofit;
import mada.tsara.backend.models.EventArtistesDjOrganisateurRetrofit;
import mada.tsara.backend.service.RetrofitApi;
import mada.tsara.backend.service.RetrofitInterface;
import mada.tsara.backend.share.BusEvent;
import mada.tsara.backend.share.Config;
import retrofit2.Call;

/**
 * Created by Miary on 31/05/2017.
 */

public class AutocompleteArtistsAdapter extends BaseAdapter implements Filterable {

    private Context mContext;
    private List<EventArtistesDjOrganisateurRetrofit> resultList = new ArrayList<EventArtistesDjOrganisateurRetrofit>();

    String className = "AutocompleteArtistsAdapter";

    int adaptercount = 0;

    RetrofitInterface apiService = RetrofitApi.getClient().create(RetrofitInterface.class);

    public AutocompleteArtistsAdapter(Context context) {
        mContext = context;
    }
    @Override
    public int getCount() {
        return adaptercount;
    }

    @Override
    public EventArtistesDjOrganisateurRetrofit getItem(int position) {
        return resultList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.adapter_autocomplete_artists, parent, false);
        }
        ((TextView) convertView.findViewById(R.id.textView_name)).setText(getItem(position).getName() );

        Log.d(Config.TAGKEY, className+ " - photoartis: "+getItem(position).getPhoto() );

        if ( !TextUtils.isEmpty(getItem(position).getPhoto())) {
            Glide.
                    with(TsaraApplication.getContext())
                    .load( Config.PATH_ARTISTES_THUMB+getItem(position).getPhoto() )
                    .into( ((CircleImageView) convertView.findViewById(R.id.imageView)) );
        }

        return convertView;
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint != null) {
                    AllEventArtistesDjOrganisateurRetrofit resultautocomplete = getResultAutoc(constraint.toString());

                    filterResults.values = resultautocomplete.getResults();
                    filterResults.count = resultautocomplete.getResults().size();

                    for(EventArtistesDjOrganisateurRetrofit artistesDjOrganisateur : resultautocomplete.getResults() ) {
                        /*Intent intent = new Intent(Intent.ACTION_SYNC, null, TsaraApplication.getContext(), SaveArtistsService.class);
                        intent.putExtra("lieux", artistesDjOrganisateur);
                        TsaraApplication.getContext().startService(intent);*/
                    }


                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                Log.d(Config.TAGKEY, className+ " - publishResults" );
                Log.d(Config.TAGKEY, className+ " - publishResults - search: "+constraint  );
                if (results != null && results.count > 0) {
                    Log.d(Config.TAGKEY, className+ " - publishResults - results.count : "+results.count );
                    resultList = (List<EventArtistesDjOrganisateurRetrofit>) results.values;

                    adaptercount = resultList.size();
                    notifyDataSetChanged();
                } else {
                    Log.d(Config.TAGKEY, className+ " - publishResults - notifyDataSetInvalidated" );
                    adaptercount = 0;
                    notifyDataSetInvalidated();
                    if (constraint!=null) {
                        BusEvent busEvent = new BusEvent();
                        busEvent.setShowNodata(true);
                        EventBus.getDefault().post(busEvent);
                    }
                }
            }};
        return filter;
    }

    private AllEventArtistesDjOrganisateurRetrofit getResultAutoc(String s) {

        Call<AllEventArtistesDjOrganisateurRetrofit> servApi = apiService.getArtisteByQuery(s);
        Log.d(Config.TAGKEY, className+ " - getResultAutoc URL : "+ servApi.request().url() );

        try {
            AllEventArtistesDjOrganisateurRetrofit items = servApi.execute().body() ;
            return items;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


}
