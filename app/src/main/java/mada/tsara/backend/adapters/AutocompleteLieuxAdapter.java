package mada.tsara.backend.adapters;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import mada.tsara.backend.R;
import mada.tsara.backend.TsaraApplication;
import mada.tsara.backend.models.AllLieuxRetrofit;
import mada.tsara.backend.models.EventlieuRetrofit;
import mada.tsara.backend.service.RetrofitApi;
import mada.tsara.backend.service.RetrofitInterface;
import mada.tsara.backend.share.BusEvent;
import mada.tsara.backend.share.Config;
import retrofit2.Call;

/**
 * Created by Miary on 31/05/2017.
 */

public class AutocompleteLieuxAdapter extends BaseAdapter implements Filterable {

    private Context mContext;
    private List<EventlieuRetrofit> resultList = new ArrayList<EventlieuRetrofit>();

    String className = "AutocompleteLieuxAdapter";

    int adaptercount = 0;

    RetrofitInterface apiService = RetrofitApi.getClient().create(RetrofitInterface.class);

    public AutocompleteLieuxAdapter(Context context) {
        mContext = context;
    }
    @Override
    public int getCount() {
        return adaptercount;
    }

    @Override
    public EventlieuRetrofit getItem(int position) {
        return resultList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.adapter_autocomplete, parent, false);
        }

        TextView textViewName = (TextView) convertView.findViewById(R.id.textView_name);
        ImageView imageViewLogo = (ImageView) convertView.findViewById(R.id.imageView);

        String evName = null;
        String evLogo = null;
        if (!TextUtils.isEmpty(getItem(position).getName())) {
            evName = getItem(position).getName();
        }
        if (!TextUtils.isEmpty(getItem(position).getLogo())) {
            evLogo = getItem(position).getLogo();
        }

        if (null!=textViewName && !TextUtils.isEmpty( evName )) {
            textViewName.setText(evName);
        }

        if (null!=imageViewLogo &&  !TextUtils.isEmpty(evLogo)) {
            Glide.
                    with(TsaraApplication.getContext())
                    .load( Config.PATH_FLYERS_CROP_100+evLogo)
                    .into(imageViewLogo);
        }
        return convertView;
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint != null) {
                    AllLieuxRetrofit resultautocomplete = getResultAutoc(constraint.toString());

                    filterResults.values = resultautocomplete.getLieu();
                    filterResults.count = resultautocomplete.getLieu().size();

                    for(EventlieuRetrofit artistesDjOrganisateur : resultautocomplete.getLieu() ) {
                        /*Intent intent = new Intent(Intent.ACTION_SYNC, null, TsaraApplication.getContext(), SaveArtistsService.class);
                        intent.putExtra("lieux", artistesDjOrganisateur);
                        TsaraApplication.getContext().startService(intent);*/
                    }


                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                Log.d(Config.TAGKEY, className+ " - publishResults" );
                Log.d(Config.TAGKEY, className+ " - publishResults - search: "+constraint  );
                if (results != null && results.count > 0) {
                    Log.d(Config.TAGKEY, className+ " - publishResults - results.count : "+results.count );
                    resultList = (List<EventlieuRetrofit>) results.values;

                    adaptercount = resultList.size();
                    notifyDataSetChanged();
                } else {
                    Log.d(Config.TAGKEY, className+ " - publishResults - notifyDataSetInvalidated" );
                    adaptercount = 0;
                    notifyDataSetInvalidated();
                    if (constraint!=null) {
                        BusEvent busEvent = new BusEvent();
                        busEvent.setShowNodata(true);
                        EventBus.getDefault().post(busEvent);
                    }
                }
            }};
        return filter;
    }

    private AllLieuxRetrofit getResultAutoc(String s) {

        Call<AllLieuxRetrofit> servApi = apiService.getAllLieux(s);
        Log.d(Config.TAGKEY, className+ " - getResultAutoc URL : "+ servApi.request().url() );

        try {
            AllLieuxRetrofit EventlieuRetrofits = servApi.execute().body() ;
            return EventlieuRetrofits;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


}
