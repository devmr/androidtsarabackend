package mada.tsara.backend.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import mada.tsara.backend.R;
import mada.tsara.backend.TsaraApplication;
import mada.tsara.backend.models.EventLocalRetrofit;
import mada.tsara.backend.service.RetrofitApi;
import mada.tsara.backend.service.RetrofitInterface;
import mada.tsara.backend.share.BusEvent;
import mada.tsara.backend.share.Config;
import mada.tsara.backend.share.Utils;
import retrofit2.Call;

/**
 * Created by Miary on 31/05/2017.
 */

public class AutocompleteFeteAdapter extends BaseAdapter implements Filterable {

    private Context mContext;
    private List<EventLocalRetrofit> resultList = new ArrayList<EventLocalRetrofit>();

    String className = "AutocompleteFeteAdapter";

    int adaptercount = 0;

    RetrofitInterface apiService = RetrofitApi.getClient().create(RetrofitInterface.class);

    public AutocompleteFeteAdapter(Context context) {
        mContext = context;
    }
    @Override
    public int getCount() {
        return adaptercount;
    }

    @Override
    public EventLocalRetrofit getItem(int position) {
        return resultList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.adapter_autocomplete_fete, parent, false);
        }
        CharSequence dateStart = Utils.getHumanizeDate(getItem(position).getStartdate(), "yyyy-MM-dd", "dd MMMM yyyy");
        CharSequence dateEnd = Utils.getHumanizeDate(getItem(position).getEnddate(), "yyyy-MM-dd", "dd MMMM yyyy");

        CharSequence date = (dateStart.equals(dateEnd) ? dateStart  : TsaraApplication.getContext().getString(R.string.txtIntervallesDate, dateStart, dateEnd));
        ((TextView) convertView.findViewById(R.id.textView_name)).setText(getItem(position).getName() );
        ((TextView) convertView.findViewById(R.id.textView_date)).setText( date );
        return convertView;
    }



    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint != null) {
                    List<EventLocalRetrofit> resultautocomplete = getResultAutoc(constraint.toString());

                    filterResults.values = resultautocomplete;
                    filterResults.count = resultautocomplete.size();

                    for(EventLocalRetrofit artistesDjOrganisateur : resultautocomplete  ) {
                        /*Intent intent = new Intent(Intent.ACTION_SYNC, null, TsaraApplication.getContext(), SaveArtistsService.class);
                        intent.putExtra("lieux", artistesDjOrganisateur);
                        TsaraApplication.getContext().startService(intent);*/
                    }


                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                Log.d(Config.TAGKEY, className+ " - publishResults" );
                Log.d(Config.TAGKEY, className+ " - publishResults - search: "+constraint  );
                if (results != null && results.count > 0) {
                    Log.d(Config.TAGKEY, className+ " - publishResults - results.count : "+results.count );
                    resultList = (List<EventLocalRetrofit>) results.values;

                    adaptercount = resultList.size();
                    notifyDataSetChanged();
                } else {
                    Log.d(Config.TAGKEY, className+ " - publishResults - notifyDataSetInvalidated" );
                    adaptercount = 0;
                    notifyDataSetInvalidated();
                    if (constraint!=null) {
                        BusEvent busEvent = new BusEvent();
                        busEvent.setShowNodata(true);
                        EventBus.getDefault().post(busEvent);
                    }
                }
            }};
        return filter;
    }

    private List<EventLocalRetrofit> getResultAutoc(String s) {

        Call<List<EventLocalRetrofit>> servApi = apiService.getAllLocal(s);
        Log.d(Config.TAGKEY, className+ " - getResultAutoc URL : "+ servApi.request().url() );

        try {
            List<EventLocalRetrofit> EventlieuRetrofits = servApi.execute().body() ;
            return EventlieuRetrofits;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


}
