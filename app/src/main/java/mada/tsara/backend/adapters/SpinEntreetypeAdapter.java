package mada.tsara.backend.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import mada.tsara.backend.R;
import mada.tsara.backend.models.EntreetypeRetrofit;

/**
 * Created by Miary on 28/05/2017.
 */

public class SpinEntreetypeAdapter extends BaseAdapter {

    private ArrayList<EntreetypeRetrofit> entreetypeRetrofits;
    private Context context;
    private LayoutInflater inflater;

    public SpinEntreetypeAdapter(Context activity, ArrayList<EntreetypeRetrofit> entreetypeRetrofits) {
        context= activity;
        this.entreetypeRetrofits = entreetypeRetrofits;
    }
    public EntreetypeRetrofit getItemByPosition(int i) {
        return entreetypeRetrofits.get(i);
    }
    @Override
    public int getCount() {
        return entreetypeRetrofits.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public int getPositionByItem(int itemid)
    {
        int position = 0;
        for (EntreetypeRetrofit event: entreetypeRetrofits) {
            if (event.getId()==itemid)
                return position;
            position++;
        }
        return position;
    }

    class Holder{
        private TextView tvCountryName;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        View myView = null;

        try {
            Holder holder;
            myView = convertView;

            if (myView == null) {
                inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                myView = inflater.inflate(R.layout.adapter_spinner_thematique, null);

                holder = new Holder();
                holder.tvCountryName = (TextView) myView.findViewById(R.id.tvCountryName);
                myView.setTag(holder);
            } else {
                holder = (Holder) myView.getTag();
            }

            holder.tvCountryName.setText(entreetypeRetrofits.get(i).getName());

        } catch (Exception e) {
            e.printStackTrace();
        }

        return myView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View myView =convertView;
        if (position == 0) {
            TextView tv = new TextView(context);
            tv.setHeight(0);
            tv.setVisibility(View.GONE);
            myView = tv;
        } else {
            myView = super.getDropDownView(position, null, parent);
        }
        return myView;
    }
}
