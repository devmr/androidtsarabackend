package mada.tsara.backend.adapters;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import mada.tsara.backend.R;
import mada.tsara.backend.TsaraApplication;
import mada.tsara.backend.models.EventRetrofit;
import mada.tsara.backend.service.RetrofitApi;
import mada.tsara.backend.service.RetrofitInterface;
import mada.tsara.backend.share.BusEvent;
import mada.tsara.backend.share.Config;
import mada.tsara.backend.share.Utils;
import retrofit2.Call;

/**
 * Created by Miary on 31/05/2017.
 */

public class AutocompleteSearchEventAdapter extends BaseAdapter implements Filterable {

    private Context mContext;
    private List<EventRetrofit> resultList = new ArrayList<EventRetrofit>();

    String className = "AutocompleteSearchEventAdapter";

    int adaptercount = 0;

    RetrofitInterface apiService = RetrofitApi.getClient().create(RetrofitInterface.class);

    public AutocompleteSearchEventAdapter(Context context) {
        mContext = context;
    }
    @Override
    public int getCount() {
        return adaptercount;
    }

    @Override
    public EventRetrofit getItem(int position) {
        return resultList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.adapter_autocomplete_search, parent, false);
        }

        CharSequence dateUnique = "";
        CharSequence dateStart = "";
        CharSequence dateEnd = "";
        if (!TextUtils.isEmpty(getItem(position).getDateUnique()))
            dateUnique = Utils.getHumanizeDate(getItem(position).getDateUnique(), "yyyy-MM-dd'T'HH:mm:ssZ", "dd MMMM yyyy");

        if (!TextUtils.isEmpty(getItem(position).getDateDebut()))
            dateStart = Utils.getHumanizeDate(getItem(position).getDateDebut(), "yyyy-MM-dd'T'HH:mm:ssZ", "dd MMMM yyyy");

        if (!TextUtils.isEmpty(getItem(position).getDateFin()))
            dateEnd = Utils.getHumanizeDate(getItem(position).getDateFin(), "yyyy-MM-dd'T'HH:mm:ssZ", "dd MMMM yyyy");

        CharSequence date = (dateStart.equals(dateEnd) ? dateUnique  : TsaraApplication.getContext().getString(R.string.txtIntervallesDate, dateStart, dateEnd));

        if (!TextUtils.isEmpty(date) && !TextUtils.isEmpty(dateUnique) )
            date = dateUnique;

        Log.d(Config.TAGKEY,   " - getName - "+getItem(position).getName());
        Log.d(Config.TAGKEY,   " - getDateUnique - "+getItem(position).getDateUnique());
        Log.d(Config.TAGKEY,   " - getDateDebut - "+getItem(position).getDateDebut());
        Log.d(Config.TAGKEY,   " - getDateFin - "+getItem(position).getDateFin());
        Log.d(Config.TAGKEY,   " - getFlyer - "+getItem(position).getFlyer());

        if (!TextUtils.isEmpty(getItem(position).getName()))
            ((TextView) convertView.findViewById(R.id.textView_name)).setText(getItem(position).getName() );

        if (null!= ((TextView) convertView.findViewById(R.id.textView_date)) && !TextUtils.isEmpty(date))
            ((TextView) convertView.findViewById(R.id.textView_date)).setText( date );

        if (!TextUtils.isEmpty(getItem(position).getFlyer())) {
            Glide.
                    with(mContext)
                    .load( getItem(position).getFlyer() )
                    .into( ((ImageView) convertView.findViewById(R.id.imageView)) );
        }

        return convertView;
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint != null) {
                    List<EventRetrofit> resultautocomplete = getResultAutoc(constraint.toString());

                    filterResults.values = resultautocomplete ;
                    filterResults.count = resultautocomplete.size();

                    for(EventRetrofit artistesDjOrganisateur : resultautocomplete  ) {
                        /*Intent intent = new Intent(Intent.ACTION_SYNC, null, TsaraApplication.getContext(), SaveArtistsService.class);
                        intent.putExtra("lieux", artistesDjOrganisateur);
                        TsaraApplication.getContext().startService(intent);*/
                    }


                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                Log.d(Config.TAGKEY, className+ " - publishResults" );
                Log.d(Config.TAGKEY, className+ " - publishResults - search: "+constraint  );
                if (results != null && results.count > 0) {
                    Log.d(Config.TAGKEY, className+ " - publishResults - results.count : "+results.count );
                    resultList = (List<EventRetrofit>) results.values;

                    adaptercount = resultList.size();
                    notifyDataSetChanged();
                } else {
                    Log.d(Config.TAGKEY, className+ " - publishResults - notifyDataSetInvalidated" );
                    adaptercount = 0;
                    notifyDataSetInvalidated();
                    if (constraint!=null) {
                        BusEvent busEvent = new BusEvent();
                        busEvent.setShowNodata(true);
                        EventBus.getDefault().post(busEvent);
                    }
                }
            }};
        return filter;
    }

    private List<EventRetrofit> getResultAutoc(String s) {

        Call<List<EventRetrofit>> servApi = apiService.getAllEvents(s);
        Log.d(Config.TAGKEY, className+ " - getResultAutoc URL : "+ servApi.request().url() );

        try {
            List<EventRetrofit> EventRetrofits = servApi.execute().body() ;
            return EventRetrofits;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


}
