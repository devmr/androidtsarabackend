package mada.tsara.backend.adapters;

import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.List;

import mada.tsara.backend.R;
import mada.tsara.backend.TsaraApplication;
import mada.tsara.backend.models.EventArtistesDjOrganisateurRetrofit;
import mada.tsara.backend.share.Config;

import static android.view.View.GONE;

/**
 * Created by Miary on 27/06/2017.
 */

public class ArtistsAddEditEnfantsRecyclerviewAdapter extends SelectableRecyclerviewAdapter<RecyclerView.ViewHolder> {
    static String className = "ArtistsAddEditRecyclerviewAdapter";

    //Views
    final int DEFAULT_V = 0;
    final int HEADER_V = 1;
    final int FOOTER_V = 2;
    final int LOADER_V = 3;

    private String[] viewtypes = {"DEFAULT_V","HEADER_V","FOOTER_V", "LOADER_V" };

    private int rowLayout;

    private View view;

    private static List<EventArtistesDjOrganisateurRetrofit> items;
    private static FragmentActivity activity;

    protected int serverListSize = -1;
    protected int loadedSize = -1;
    protected int serverTotal = 0;


    public ArtistsAddEditEnfantsRecyclerviewAdapter(FragmentActivity activity, List<EventArtistesDjOrganisateurRetrofit> items, int rowLayout  ) {
        Log.d(Config.TAGKEY, className+"  - rowLayout : "+rowLayout );
        Log.d(Config.TAGKEY, className+"  - items : "+items );
        Log.d(Config.TAGKEY, className+"  - activity : "+activity );
        this.items = items;
        this.activity = activity;
        this.rowLayout = rowLayout;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.d(Config.TAGKEY, className+" - onCreateViewHolder - viewType : "+viewType );
        view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new ViewHolderDefault(view);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Log.d(Config.TAGKEY, className+" - onBindViewHolder - position :  "+position + " - items.size() : "+getItemCount()+" - holder : "+holder );
        if(position-2<items.size() && items.size()>0 && holder instanceof ViewHolderDefault ){

            Log.d(Config.TAGKEY, className+" - onBindViewHolder - position :  "+position + " - instanceof ViewHolderDefault "  );
            EventArtistesDjOrganisateurRetrofit ev = items.get(position);


            ImageView imageView = ( (ViewHolderDefault) holder).image;
            TextView textName = ( (ViewHolderDefault) holder).name;
            final ProgressBar item_progress = ( (ViewHolderDefault) holder).item_progress;

            if (null!=textName && !TextUtils.isEmpty(ev.getName())) {
                textName.setText(ev.getName());
            }

            if (null!=imageView && !TextUtils.isEmpty(ev.getPhoto()) ) {
                String photo = ev.getPhoto();
                Glide.
                        with(TsaraApplication.getContext())
                        .load(Config.PATH_ARTISTES_THUMB + photo)
                        .listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {

                                if (item_progress!=null)
                                    item_progress.setVisibility(GONE);

                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                if (item_progress!=null)
                                    item_progress.setVisibility(GONE);

                                return false;
                            }
                        })
                        .into(imageView);
            }





        }
    }

    @Override
    public int getItemCount() {
        int nb =  items == null ? 0 : (items.size())   ; // + Header + Footer
        Log.d(Config.TAGKEY, className+" - getItemCount -  " +nb+" - items.size(): "+items.size()  );
        return nb>0?nb:0;
    }

    @Override
    public int getItemViewType(int position) {



        int viewtype = DEFAULT_V;
        return viewtype;
    }


    public void setLoadedSize(int loadedSize){
        Log.d(Config.TAGKEY, className+" - setLoadedSize - " +loadedSize );
        this.loadedSize = loadedSize;
    }
    public void setServerListSize(int serverListSize){
        Log.d(Config.TAGKEY, className+" - setServerListSize - " +serverListSize );
        this.serverListSize = serverListSize;
    }
    public void setServerTotal(int serverTotal){
        Log.d(Config.TAGKEY, className+" - ServerTotal - " +serverTotal );
        this.serverTotal = serverTotal;
    }

    public void removeAt(int position) {
        items.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(0, items.size());
    }

    /**
     * ViewHolder
     */
    public static class ViewHolderDefault extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView name;


        public ImageView image;
        public View overflow;

        public LinearLayout fond;

        public View selectedOverlay;
        private ClickListener listener;

        RecyclerView recyclerview_artistes;

        ProgressBar item_progress;





        String className = "ArtistsAdapter > ViewHolderDefault";

        public ViewHolderDefault(View convertView) {

            super(convertView);
            Log.d(Config.TAGKEY, className+" - ViewHolderDefault" );

            fond = (LinearLayout) convertView.findViewById(R.id.fond);

            name = (TextView) convertView.findViewById(R.id.name);
            image = (ImageView) convertView.findViewById(R.id.image);
            item_progress = (ProgressBar) convertView.findViewById(R.id.item_progress);




            fond.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Log.d(Config.TAGKEY, className+" - onClick" );
            if (listener != null) {
                listener.onItemClicked(getPosition());
            }



            //Reinitialiser le fond
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    //nd.setBackgroundColor(Color.TRANSPARENT);

                }
            }, 500);


        }

        public interface ClickListener {
            public void onItemClicked(int position);
        }
    }

    /**
     * ViewHolder
     */
    public static class ViewHolderHeader extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView name;
        public TextView date;
        public TextView gratuit;

        public ImageView image;
        public View overflow;

        public RelativeLayout fond;

        public View selectedOverlay;
        private ClickListener listener;

        String className = "eventweekAdapter > ViewHolderHeader";

        public ViewHolderHeader(View convertView) {

            super(convertView);
            Log.d(Config.TAGKEY, className+" - ViewHolderHeader" );



            convertView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Log.d(Config.TAGKEY, className+" - onClick" );

        }

        public interface ClickListener {
            public void onItemClicked(int position);
        }
    }

    /**
     * ViewHolder
     */
    public static class ViewHolderFooter extends RecyclerView.ViewHolder implements View.OnClickListener {

        public Button btn_more_header_current_eventweek;
        private ClickListener listener;

        String className = "eventweekAdapter > ViewHolderFooter";

        public ViewHolderFooter(View convertView) {

            super(convertView);
            Log.d(Config.TAGKEY, className+" - ViewHolderFooter" );



            convertView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Log.d(Config.TAGKEY, className+" - onClick" );

        }

        public interface ClickListener {
            public void onItemClicked(int position);
        }
    }

    /**
     * ViewHolder
     */
    public static class ViewHolderLoader extends RecyclerView.ViewHolder implements View.OnClickListener {


        private ClickListener listener;

        String className = "eventweekAdapter > ViewHolderLoader";

        public ViewHolderLoader(View convertView) {

            super(convertView);
            Log.d(Config.TAGKEY, className+" - ViewHolderLoader" );



            convertView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Log.d(Config.TAGKEY, className+" - onClick" );

        }

        public interface ClickListener {
            public void onItemClicked(int position);
        }
    }


}
