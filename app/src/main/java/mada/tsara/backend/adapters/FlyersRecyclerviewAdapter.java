package mada.tsara.backend.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.joanzapata.iconify.widget.IconButton;

import java.util.List;

import mada.tsara.backend.R;
import mada.tsara.backend.models.EventFlyersRetrofit;
import mada.tsara.backend.share.Config;

/**
 * Created by Miary on 27/06/2017.
 */

public class FlyersRecyclerviewAdapter extends SelectableRecyclerviewAdapter<RecyclerView.ViewHolder> {
    String className = "FlyersRecyclerviewAdapter";

    //Views
    final int DEFAULT_V = 0;

    private int rowLayout;

    private View view;

    private static List<EventFlyersRetrofit> items;
    private static FragmentActivity activity;

    protected int serverListSize = -1;
    protected int loadedSize = -1;


    public FlyersRecyclerviewAdapter(FragmentActivity activity, List<EventFlyersRetrofit> items, int rowLayout  ) {
        Log.d(Config.TAGKEY, className+" - rowLayout : "+rowLayout );
        this.items = items;
        this.activity = activity;
        this.rowLayout = rowLayout;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.d(Config.TAGKEY, className+" - onCreateViewHolder - viewType : "+viewType );

        view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new ViewHolderDefault(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        Log.d(Config.TAGKEY, className+" - onBindViewHolder - position :  "+position + " - eventType.size() : "+items.size()+" - holder : "+holder );
        if(items.size()>0 && holder instanceof ViewHolderDefault && null!=items.get(position)){
            final EventFlyersRetrofit ev = items.get(position);


            ImageView image = ( (ViewHolderDefault) holder).image;
            IconButton iconUpload = ( (ViewHolderDefault) holder).iconUpload;
            IconButton iconSetMainFlyer = ( (ViewHolderDefault) holder).iconSetMainFlyer;
            LinearLayout fond = ( (ViewHolderDefault) holder).fond;

            if (null!=image && !TextUtils.isEmpty(ev.getImage())) {
                //textName.setText(ev.getImage());

                String photo = ev.getImage();
                boolean uri = false;
                boolean tmp = false;
                boolean isMain = false;

                if (null!=ev.getUri())
                    uri = ev.getUri();

                if (null!=ev.getTmp())
                    tmp = ev.getTmp();

                if (null!=ev.getIsmain())
                    isMain = ev.getIsmain();

                String pathPhoto = photo;

                if (!uri && !tmp)
                    pathPhoto = Config.PATH_FLYERS + photo;

                if (!uri && tmp)
                    pathPhoto = Config.PATH_TMP + photo;

                Log.d(Config.TAGKEY, className+" - onBindViewHolder - position :  "+position + " - instanceof ViewHolderDefault "+pathPhoto+" - uri: "+uri+" - tmp: "+tmp  );

                Log.d(Config.TAGKEY," onBindViewHolder photo : "+photo);
                Log.d(Config.TAGKEY," onBindViewHolder pathPhoto : "+pathPhoto);
                Glide.
                        with(activity)
                        .load(pathPhoto)
                        .listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {



                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {


                                return false;
                            }
                        })
                        .into(image);

                // Modif opacity
                if (uri) {
                    image.setAlpha(0.3f);
                }

                if (!uri && null!=iconUpload) {
                    iconUpload.setVisibility(View.GONE);
                }

                if ( uri  ) {
                    iconSetMainFlyer.setVisibility(View.GONE);
                }

                /*if ( isMain  ) {
                    Context context = fond.getContext();
                    int rid = context.getResources().getIdentifier("border_solid_blue_4dp", "drawable", context.getPackageName());
                    fond.setBackgroundResource(rid);
                }*/

            }


        }
    }

    @Override
    public int getItemCount() {
        int nb =  items == null ? 0 : items.size()  ;
        Log.d(Config.TAGKEY, className+" - getItemCount -  " +nb  );
        return nb;
    }

    public boolean idInAdapter(int id) {
        Log.d(Config.TAGKEY, className+" - idInAdapter " +id + " - eventType.size(): " + items.size() );
        for( int i = 0; i < items.size(); i++ ) {
            if ( items.get(i).getId().equals(id) ) {
                Log.d(Config.TAGKEY, className+" - idInAdapter yes - " +items.get(i).getId() );
                return true;
            }
        }
        return false;
    }
    public void setLoadedSize(int loadedSize){
        Log.d(Config.TAGKEY, className+" - setLoadedSize - " +loadedSize );
        this.loadedSize = loadedSize;
    }
    public void setServerListSize(int serverListSize){
        Log.d(Config.TAGKEY, className+" - setServerListSize - " +serverListSize );
        this.serverListSize = serverListSize;
    }

    public void removeAt(int position) {
        items.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(0, items.size());
    }

    /**
     * ViewHolder
     */
    public static class ViewHolderDefault extends RecyclerView.ViewHolder implements View.OnClickListener {

        public ImageView image;
        public IconButton iconUpload;
        public IconButton iconSetMainFlyer;
        public View overflow;
        public LinearLayout fond;

        public View selectedOverlay;
        private ClickListener listener;

        String className = "EventLocalAdapter > ViewHolderDefault";

        public ViewHolderDefault(View convertView) {

            super(convertView);
            Log.d(Config.TAGKEY, className+" - ViewHolderDefault" );

            image = (ImageView) convertView.findViewById(R.id.image);
            iconUpload = (IconButton) convertView.findViewById(R.id.iconUpload);
            iconSetMainFlyer = (IconButton) convertView.findViewById(R.id.iconSetMainFlyer);
            fond = (LinearLayout) convertView.findViewById(R.id.fond);



        }

        @Override
        public void onClick(View v) {
            Log.d(Config.TAGKEY, className+" - onClick" );
            if (listener != null) {
                listener.onItemClicked(getLayoutPosition());
            }

        }

        public interface ClickListener {
            public void onItemClicked(int position);
        }
    }
}
