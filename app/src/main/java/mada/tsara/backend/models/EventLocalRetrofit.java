package mada.tsara.backend.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Miary on 31/05/2017.
 */

public class EventLocalRetrofit  implements Parcelable {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("slug")
    @Expose
    private String slug;
    @SerializedName("startdate")
    @Expose
    private String startdate;
    @SerializedName("enddate")
    @Expose
    private String enddate;
    @SerializedName("recurrence")
    @Expose
    private String recurrence;
    public EventLocalRetrofit(){}

    public EventLocalRetrofit( String name ){
        this.name = name;
    }


    public EventLocalRetrofit(int id, String name, String startdate, String slug,  String  enddate, String recurrence){
        this.id = id;
        this.name = name;
        this.startdate = startdate;
        this.slug = slug;
        this.recurrence = recurrence;
        this.enddate = enddate;
    }

    protected EventLocalRetrofit(Parcel in) {
        name = in.readString();
        slug = in.readString();
        startdate = in.readString();
        enddate = in.readString();
        recurrence = in.readString();
        id = in.readInt();
    }

    public static final Creator<EventLocalRetrofit> CREATOR = new Creator<EventLocalRetrofit>() {
        @Override
        public EventLocalRetrofit createFromParcel(Parcel in) {
            return new EventLocalRetrofit(in);
        }

        @Override
        public EventLocalRetrofit[] newArray(int size) {
            return new EventLocalRetrofit[size];
        }
    };

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The slug
     */
    public String getSlug() {
        return slug;
    }

    /**
     *
     * @param slug
     * The slug
     */
    public void setSlug(String slug) {
        this.slug = slug;
    }

    /**
     *
     * @return
     * The startdate
     */
    public String getStartdate() {
        return startdate;
    }

    /**
     *
     * @param startdate
     * The startdate
     */
    public void setStartdate(String startdate) {
        this.startdate = startdate;
    }

    /**
     *
     * @return
     * The enddate
     */
    public String getEnddate() {
        return enddate;
    }

    /**
     *
     * @param enddate
     * The enddate
     */
    public void setEnddate(String enddate) {
        this.enddate = enddate;
    }

    /**
     *
     * @return
     * The recurrence
     */
    public String getRecurrence() {
        return recurrence;
    }

    /**
     *
     * @param recurrence
     * The recurrence
     */
    public void setRecurrence(String recurrence) {
        this.recurrence = recurrence;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(slug);
        dest.writeString(startdate);
        dest.writeString(enddate);
        dest.writeString(recurrence);
        dest.writeInt(id);
    }
}
