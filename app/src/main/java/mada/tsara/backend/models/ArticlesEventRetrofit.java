package mada.tsara.backend.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Miary on 31/05/2017.
 */

class ArticlesEventRetrofit implements Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("author")
    @Expose
    private String author;
    @SerializedName("content")
    @Expose
    private String content;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("addartistes_inevent")
    @Expose
    private Boolean addartistesInevent;

    @SerializedName("event_id")
    @Expose
    private Integer eventid;
    @SerializedName("event_name")
    @Expose
    private String eventname;
    @SerializedName("event_flyer")
    @Expose
    private String eventflyer;
    @SerializedName("event_slug")
    @Expose
    private String eventslug;
    @SerializedName("event_position")
    @Expose
    private Integer eventposition;
    @SerializedName("event_dateunique")
    @Expose
    private String eventdateUnique;
    @SerializedName("event_heuredebut")
    @Expose
    private String eventheureDebut;
    @SerializedName("event_heurefin")
    @Expose
    private String eventheureFin;
    @SerializedName("event_datedebut")
    @Expose
    private String eventdateDebut;
    @SerializedName("event_datefin")
    @Expose
    private String eventdateFin;

    @SerializedName("position")
    @Expose
    private Integer position;

    public ArticlesEventRetrofit(){

    }

    protected ArticlesEventRetrofit(Parcel in) {
        url = in.readString();
        title = in.readString();
        author = in.readString();
        content = in.readString();
        createdAt = in.readString();
        eventname = in.readString();
        eventflyer = in.readString();
        eventslug = in.readString();
        eventdateUnique = in.readString();
        eventheureDebut = in.readString();
        eventheureFin = in.readString();
        eventdateDebut = in.readString();
        eventdateFin = in.readString();
    }

    public static final Creator<ArticlesEventRetrofit> CREATOR = new Creator<ArticlesEventRetrofit>() {
        @Override
        public ArticlesEventRetrofit createFromParcel(Parcel in) {
            return new ArticlesEventRetrofit(in);
        }

        @Override
        public ArticlesEventRetrofit[] newArray(int size) {
            return new ArticlesEventRetrofit[size];
        }
    };

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The url
     */
    public String getUrl() {
        return url;
    }

    /**
     *
     * @param url
     * The url
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     *
     * @return
     * The title
     */
    public String getTitle() {
        return title;
    }

    /**
     *
     * @param title
     * The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     *
     * @return
     * The author
     */
    public String getAuthor() {
        return author;
    }

    /**
     *
     * @param author
     * The author
     */
    public void setAuthor(String author) {
        this.author = author;
    }

    /**
     *
     * @return
     * The content
     */
    public String getContent() {
        return content;
    }

    /**
     *
     * @param content
     * The content
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     *
     * @return
     * The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     *
     * @param createdAt
     * The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     *
     * @return
     * The addartistesInevent
     */
    public Boolean getAddartistesInevent() {
        return addartistesInevent;
    }

    /**
     *
     * @param addartistesInevent
     * The addartistes_inevent
     */
    public void setAddartistesInevent(Boolean addartistesInevent) {
        this.addartistesInevent = addartistesInevent;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(url);
        dest.writeString(title);
        dest.writeString(author);
        dest.writeString(content);
        dest.writeString(createdAt);
        dest.writeString(eventname);
        dest.writeString(eventflyer);
        dest.writeString(eventslug);
        dest.writeString(eventdateUnique);
        dest.writeString(eventheureDebut);
        dest.writeString(eventheureFin);
        dest.writeString(eventdateDebut);
        dest.writeString(eventdateFin);
    }

    /**
     *
     * @return
     * The id
     */
    public Integer getEventid() {
        return eventid;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setEventid(Integer eventid) {
        this.eventid = eventid;
    }

    /**
     *
     * @return
     * The name
     */
    public String getEventname() {
        return eventname;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setEventname(String eventname) {
        this.eventname = eventname;
    }

    /**
     *
     * @return
     * The flyer
     */
    public String getEventflyer() {
        return eventflyer;
    }

    /**
     *
     * @param flyer
     * The flyer
     */
    public void setEventflyer(String eventflyer) {
        this.eventflyer = eventflyer;
    }

    /**
     *
     * @return
     * The slug
     */
    public String getEventslug() {
        return eventslug;
    }

    /**
     *
     * @param slug
     * The slug
     */
    public void setEventslug(String eventslug) {
        this.eventslug = eventslug;
    }



    /**
     *
     * @return
     * The position
     */
    public Integer getEventposition() {
        return eventposition;
    }

    /**
     *
     * @param position
     * The position
     */
    public void setEventposition(Integer eventposition) {
        this.eventposition = eventposition;
    }



    /**
     *
     * @return
     * The dateUnique
     */
    public String getEventdateUnique() {
        return eventdateUnique;
    }

    /**
     *
     * @param dateUnique
     * The date_unique
     */
    public void setEventdateUnique(String eventdateUnique) {
        this.eventdateUnique = eventdateUnique;
    }

    /**
     *
     * @return
     * The heureDebut
     */
    public String getEventheureDebut() {
        return eventheureDebut;
    }

    /**
     *
     * @param heureDebut
     * The heure_debut
     */
    public void setEventheureDebut(String eventheureDebut) {
        this.eventheureDebut = eventheureDebut;
    }

    /**
     *
     * @return
     * The heureFin
     */
    public String getEventheureFin() {
        return eventheureFin;
    }

    /**
     *
     * @param heureFin
     * The heure_fin
     */
    public void setEventheureFin(String eventheureFinFin) {
        this.eventheureFin = eventheureFin;
    }


    /**
     *
     * @return
     * The dateDebut
     */
    public String getEventdateDebut() {
        return eventdateDebut;
    }

    /**
     *
     * @param dateDebut
     * The date_debut
     */
    public void setEventdateDebut(String eventdateDebut) {
        this.eventdateDebut = eventdateDebut;
    }

    /**
     *
     * @return
     * The dateFin
     */
    public String getEventdateFin() {
        return eventdateFin;
    }

    /**
     *
     * @param dateFin
     * The date_fin
     */
    public void setEventdateFin(String eventdateFin) {
        this.eventdateFin = eventdateFin;
    }

    /**
     *
     * @return
     * The position
     */
    public Integer getPosition() {
        return position;
    }

    /**
     *
     * @param position
     * The position
     */
    public void setPosition(Integer position) {
        this.position = position;
    }
}
