package mada.tsara.backend.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Miary on 31/05/2017.
 */

public class EventMultiLieuRetrofit {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("slug")
    @Expose
    private String slug;
    @SerializedName("position")
    @Expose
    private Integer position;
    @SerializedName("gps")
    @Expose
    private String gps;
    @SerializedName("heure")
    @Expose
    private List<Object> heure = new ArrayList<Object>();
    @SerializedName("country")
    @Expose
    private CountryRetrofit country;
    @SerializedName("region")
    @Expose
    private RegionRetrofit region;
    @SerializedName("locality")
    @Expose
    private LocalityRetrofit locality;
    @SerializedName("_tels_lieu")
    @Expose
    private List<Object> telsLieu = new ArrayList<Object>();

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The slug
     */
    public String getSlug() {
        return slug;
    }

    /**
     *
     * @param slug
     * The slug
     */
    public void setSlug(String slug) {
        this.slug = slug;
    }

    /**
     *
     * @return
     * The position
     */
    public Integer getPosition() {
        return position;
    }

    /**
     *
     * @param position
     * The position
     */
    public void setPosition(Integer position) {
        this.position = position;
    }

    /**
     *
     * @return
     * The gps
     */
    public String getGps() {
        return gps;
    }

    /**
     *
     * @param gps
     * The gps
     */
    public void setGps(String gps) {
        this.gps = gps;
    }

    /**
     *
     * @return
     * The heure
     */
    public List<Object> getHeure() {
        return heure;
    }

    /**
     *
     * @param heure
     * The heure
     */
    public void setHeure(List<Object> heure) {
        this.heure = heure;
    }

    /**
     *
     * @return
     * The country
     */
    public CountryRetrofit getCountry() {
        return country;
    }

    /**
     *
     * @param country
     * The country
     */
    public void setCountry(CountryRetrofit country) {
        this.country = country;
    }

    /**
     *
     * @return
     * The region
     */
    public RegionRetrofit getRegion() {
        return region;
    }

    /**
     *
     * @param region
     * The region
     */
    public void setRegion(RegionRetrofit region) {
        this.region = region;
    }

    /**
     *
     * @return
     * The locality
     */
    public LocalityRetrofit getLocality() {
        return locality;
    }

    /**
     *
     * @param locality
     * The locality
     */
    public void setLocality(LocalityRetrofit locality) {
        this.locality = locality;
    }

    /**
     *
     * @return
     * The telsLieu
     */
    public List<Object> getTelsLieu() {
        return telsLieu;
    }

    /**
     *
     * @param telsLieu
     * The _tels_lieu
     */
    public void setTelsLieu(List<Object> telsLieu) {
        this.telsLieu = telsLieu;
    }
}
