package mada.tsara.backend.models;

import android.net.Uri;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Miary on 31/05/2017.
 */

public class EventFlyersRetrofit {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("crdate")
    @Expose
    private String crdate;
    @SerializedName("hidden")
    @Expose
    private Boolean hidden;

    @SerializedName("uri")
    @Expose
    private Boolean uri;



    @SerializedName("tmp")
    @Expose
    private Boolean tmp;

    public Boolean getIsmain() {
        return ismain;
    }

    public void setIsmain(Boolean ismain) {
        this.ismain = ismain;
    }

    @SerializedName("ismain")
    @Expose
    private Boolean ismain;


    @SerializedName("mImageUri")
    @Expose
    private Uri mImageUri;

    public EventFlyersRetrofit(){
    }

    public EventFlyersRetrofit(Boolean uri, String image ){
        this.uri = uri;
        this.image = image;
    }

    public EventFlyersRetrofit(Boolean uri, String image, Uri mImageUri){
        this.uri = uri;
        this.image = image;
        this.mImageUri = mImageUri;
    }

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The image
     */
    public String getImage() {
        return image;
    }

    /**
     *
     * @param image
     * The image
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     *
     * @return
     * The crdate
     */
    public String getCrdate() {
        return crdate;
    }

    /**
     *
     * @param crdate
     * The crdate
     */
    public void setCrdate(String crdate) {
        this.crdate = crdate;
    }

    /**
     *
     * @return
     * The hidden
     */
    public Boolean getHidden() {
        return hidden;
    }

    /**
     *
     * @param hidden
     * The hidden
     */
    public void setHidden(Boolean hidden) {
        this.hidden = hidden;
    }

    public Boolean getUri() {
        return uri;
    }

    public void setUri(Boolean uri) {
        this.uri = uri;
    }

    public Uri getmImageUri() {
        return mImageUri;
    }

    public void setmImageUri(Uri mImageUri) {
        this.mImageUri = mImageUri;
    }

    public Boolean getTmp() {
        return tmp;
    }

    public void setTmp(Boolean tmp) {
        this.tmp = tmp;
    }
}
