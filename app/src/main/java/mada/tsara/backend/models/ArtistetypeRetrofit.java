package mada.tsara.backend.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import static android.R.attr.id;

/**
 * Created by Miary on 28/05/2017.
 */

public class ArtistetypeRetrofit implements Parcelable {

    @SerializedName("value")
    @Expose
    private String value;
    @SerializedName("name")
    @Expose
    private String name;


    public ArtistetypeRetrofit(){

    }
    public ArtistetypeRetrofit(String value, String name ){
        this.value = value;
        this.name = name;
    }


    protected ArtistetypeRetrofit(Parcel in) {
        name = in.readString();
        value = in.readString();

    }

    public static final Creator<ArtistetypeRetrofit> CREATOR = new Creator<ArtistetypeRetrofit>() {
        @Override
        public ArtistetypeRetrofit createFromParcel(Parcel in) {
            return new ArtistetypeRetrofit(in);
        }

        @Override
        public ArtistetypeRetrofit[] newArray(int size) {
            return new ArtistetypeRetrofit[size];
        }
    };

    /**
     *
     * @return
     * The id
     */
    public String  getValue() {
        return value;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }



    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(value);
    }
}
