package mada.tsara.backend.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Miary on 31/05/2017.
 */

public class EventArtistesDjOrganisateurRetrofit  implements Parcelable {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("vztid")
    @Expose
    private Integer vztid;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("slug")
    @Expose
    private String slug;

    @SerializedName("orig_slug")
    @Expose
    private String origSlug;


    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("_articles_event")
    @Expose
    private List<ArticlesEventRetrofit> articlesEvent = new ArrayList<ArticlesEventRetrofit>();

    @SerializedName("photo")
    @Expose
    private String photo;

    @SerializedName("viewholder")
    @Expose
    private String viewholder;

    @SerializedName("enfant_artiste")
    @Expose
    private List<EventArtistesDjOrganisateurRetrofit> enfantArtiste = new ArrayList<EventArtistesDjOrganisateurRetrofit>();

    @SerializedName("parent_artiste")
    @Expose
    private List<EventArtistesDjOrganisateurRetrofit> parentArtiste = new ArrayList<EventArtistesDjOrganisateurRetrofit>();



    public EventArtistesDjOrganisateurRetrofit(){}

    public EventArtistesDjOrganisateurRetrofit( String name, String type ){
        this.name = name;
        this.type = type;
    }

    protected EventArtistesDjOrganisateurRetrofit(Parcel in) {
        name = in.readString();
        slug = in.readString();
        origSlug = in.readString();
        type = in.readString();
        photo = in.readString();
        viewholder = in.readString();
        id = in.readInt();
    }

    public static final Creator<EventArtistesDjOrganisateurRetrofit> CREATOR = new Creator<EventArtistesDjOrganisateurRetrofit>() {
        @Override
        public EventArtistesDjOrganisateurRetrofit createFromParcel(Parcel in) {
            return new EventArtistesDjOrganisateurRetrofit(in);
        }

        @Override
        public EventArtistesDjOrganisateurRetrofit[] newArray(int size) {
            return new EventArtistesDjOrganisateurRetrofit[size];
        }
    };

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The vztid
     */
    public Integer getVztid() {
        return vztid;
    }

    /**
     *
     * @param vztid
     * The vztid
     */
    public void setVztid(Integer vztid) {
        this.vztid = vztid;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The slug
     */
    public String getSlug() {
        return slug;
    }

    /**
     *
     * @param slug
     * The slug
     */
    public void setSlug(String slug) {
        this.slug = slug;
    }

    /**
     *
     * @return
     * The slug
     */
    public String getOrigSlug() {
        return origSlug;
    }

    /**
     *
     * @param slug
     * The slug
     */
    public void setOrigSlug(String origSlug) {
        this.origSlug = origSlug;
    }

    /**
     *
     * @return
     * The type
     */
    public String getType() {
        return type;
    }

    /**
     *
     * @param type
     * The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     *
     * @return
     * The articlesEvent
     */
    public List<ArticlesEventRetrofit> getArticlesEvent() {
        return articlesEvent;
    }

    /**
     *
     * @param articlesEvent
     * The _articles_event
     */
    public void setArticlesEvent(List<ArticlesEventRetrofit> articlesEvent) {
        this.articlesEvent = articlesEvent;
    }

    /**
     *
     * @return
     * The type
     */
    public String getPhoto() {
        return photo;
    }

    /**
     *
     * @param type
     * The type
     */
    public void setPhoto(String photo) {
        this.photo = photo;
    }

    /**
     *
     * @return
     * The type
     */
    public String getViewholder() {
        return viewholder;
    }

    public List<EventArtistesDjOrganisateurRetrofit> getEnfantArtiste() {
        return enfantArtiste;
    }

    public void setEnfantArtiste(List<EventArtistesDjOrganisateurRetrofit> enfantArtiste) {
        this.enfantArtiste = enfantArtiste;
    }

    public List<EventArtistesDjOrganisateurRetrofit> getParentArtiste() {
        return parentArtiste;
    }

    public void setParentArtiste(List<EventArtistesDjOrganisateurRetrofit> parentArtiste) {
        this.parentArtiste = parentArtiste;
    }

    /**
     *
     * @param type
     * The type
     */
    public void setViewholder(String viewholder) {
        this.viewholder = viewholder;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(slug);
        dest.writeString(origSlug);
        dest.writeString(type);
        dest.writeString(photo);
        dest.writeString(viewholder);
        dest.writeInt(id);
    }
}
