package mada.tsara.backend.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Miary on 28/05/2017.
 */

public class AllEventTypeRetrofit {
    @SerializedName("results")
    @Expose
    private List<EventtypeRetrofit> results = new ArrayList<EventtypeRetrofit>();

    /**
     *
     * @return
     * The results
     */
    public List<EventtypeRetrofit> getResults() {
        return results;
    }

    /**
     *
     * @param results
     * The results
     */
    public void setResults(List<EventtypeRetrofit> results) {
        this.results = results;
    }
}
