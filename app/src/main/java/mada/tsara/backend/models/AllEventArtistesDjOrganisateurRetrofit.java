package mada.tsara.backend.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Miary on 02/06/2017.
 */

public class AllEventArtistesDjOrganisateurRetrofit  {
    @SerializedName("end_key")
    @Expose
    private Integer endKey;
    @SerializedName("endresults")
    @Expose
    private Boolean endresults;
    @SerializedName("total")
    @Expose
    private Integer total;
    @SerializedName("nextpage")
    @Expose
    private Integer nextpage;
    @SerializedName("limit")
    @Expose
    private String limit;

    @SerializedName("results")
    @Expose
    private List<EventArtistesDjOrganisateurRetrofit> liste = new ArrayList<EventArtistesDjOrganisateurRetrofit>();

    @SerializedName("allids")
    @Expose
    private String allids;

    /**
     *
     * @return
     * The endKey
     */
    public Integer getEndKey() {
        return endKey;
    }

    /**
     *
     * @param endKey
     * The end_key
     */
    public void setEndKey(Integer endKey) {
        this.endKey = endKey;
    }

    /**
     *
     * @return
     * The endresults
     */
    public Boolean getEndresults() {
        return endresults;
    }

    /**
     *
     * @param endresults
     * The endresults
     */
    public void setEndresults(Boolean endresults) {
        this.endresults = endresults;
    }

    /**
     *
     * @return
     * The total
     */
    public Integer getTotal() {
        return total;
    }

    /**
     *
     * @param total
     * The total
     */
    public void setTotal(Integer total) {
        this.total = total;
    }

    /**
     *
     * @return
     * The nextpage
     */
    public Integer getNextpage() {
        return nextpage;
    }

    /**
     *
     * @param nextpage
     * The nextpage
     */
    public void setNextpage(Integer nextpage) {
        this.nextpage = nextpage;
    }
    /**
     *
     * @return
     * The limit
     */
    public String getLimit() {
        return limit;
    }

    /**
     *
     * @param limit
     * The limit
     */
    public void setLimit(String limit) {
        this.limit = limit;
    }
    /**
     *
     * @return
     * The liste
     */
    public List<EventArtistesDjOrganisateurRetrofit> getResults() {
        return liste;
    }

    /**
     *
     * @param liste
     * The liste
     */
    public void setResults(List<EventArtistesDjOrganisateurRetrofit> liste) {
        this.liste = liste;
    }

    /**
     *
     * @return
     * The limit
     */
    public String getAllids() {
        return allids;
    }

    /**
     *
     * @param limit
     * The limit
     */
    public void setAllids(String allids) {
        this.allids = allids;
    }
}
