package mada.tsara.backend.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Miary on 28/05/2017.
 */

public class EventtypeRetrofit implements Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("faicon")
    @Expose
    private String faicon;
    @SerializedName("slug")
    @Expose
    private String slug;
    @SerializedName("position")
    @Expose
    private Integer position;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("logo")
    @Expose
    private String logo;

    public EventtypeRetrofit(){

    }
    public EventtypeRetrofit(int id, String name ){
        this.id = id;
        this.name = name;
    }
    public EventtypeRetrofit(int id, String name, String faicon, String slug, int position, String  category, String logo){
        this.id = id;
        this.name = name;
        this.faicon = faicon;
        this.slug = slug;
        this.position = position;
        this.logo = logo;
        this.category = category;
    }

    protected EventtypeRetrofit(Parcel in) {
        name = in.readString();
        faicon = in.readString();
        slug = in.readString();
        category = in.readString();
        logo = in.readString();
        id = in.readInt();
    }

    public static final Creator<EventtypeRetrofit> CREATOR = new Creator<EventtypeRetrofit>() {
        @Override
        public EventtypeRetrofit createFromParcel(Parcel in) {
            return new EventtypeRetrofit(in);
        }

        @Override
        public EventtypeRetrofit[] newArray(int size) {
            return new EventtypeRetrofit[size];
        }
    };

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The faicon
     */
    public String getFaicon() {
        return faicon;
    }

    /**
     *
     * @param faicon
     * The faicon
     */
    public void setFaicon(String faicon) {
        this.faicon = faicon;
    }

    /**
     *
     * @return
     * The slug
     */
    public String getSlug() {
        return slug;
    }

    /**
     *
     * @param slug
     * The slug
     */
    public void setSlug(String slug) {
        this.slug = slug;
    }

    /**
     *
     * @return
     * The position
     */
    public Integer getPosition() {
        return position;
    }

    /**
     *
     * @param position
     * The position
     */
    public void setPosition(Integer position) {
        this.position = position;
    }

    /**
     *
     * @return
     * The category
     */
    public String getCategory() {
        return category;
    }

    /**
     *
     * @param category
     * The category
     */
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     *
     * @return
     * The logo
     */
    public String getLogo() {
        return logo;
    }

    /**
     *
     * @param logo
     * The logo
     */
    public void setLogo(String logo) {
        this.logo = logo;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(faicon);
        dest.writeString(slug);
        dest.writeString(category);
        dest.writeString(logo);
        dest.writeInt(id);
    }
}
