package mada.tsara.backend.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Miary on 31/05/2017.
 */

public class EventRetrofit implements Parcelable {


    @SerializedName("id")
    @Expose
    private Integer id;


    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("flyer")
    @Expose
    private String flyer;
    @SerializedName("slug")
    @Expose
    private String slug;
    @SerializedName("cancelled_at")
    @Expose
    private Boolean cancelledAt;
    @SerializedName("position")
    @Expose
    private Integer position;

    @SerializedName("prixenclair")
    @Expose
    private String prixenclair;

    @SerializedName(value="date_unique", alternate={ "dateUnique" } )
    @Expose
    private String dateUnique;

    @SerializedName(value="heure_debut", alternate={ "heureDebut" })
    @Expose
    private String heureDebut;
    @SerializedName(value="heure_fin", alternate={ "heureFin" })
    @Expose
    private String heureFin;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("hidden")
    @Expose
    private Boolean hidden;
    @SerializedName(value="date_debut", alternate={ "dateDebut" })
    @Expose
    private String dateDebut;
    @SerializedName(value="date_fin", alternate={ "dateFin" })
    @Expose
    private String dateFin;

    @SerializedName("eventlieu")
    @Expose
    private EventlieuRetrofit eventlieu;

    @SerializedName("eventmultilieu")
    @Expose
    private List<EventMultiLieuRetrofit> eventmultilieu = new ArrayList<EventMultiLieuRetrofit>();

    @SerializedName("entreetype")
    @Expose
    private EntreetypeRetrofit entreetype;
    @SerializedName("eventlocal")
    @Expose
    private List<EventLocalRetrofit> eventlocal = new ArrayList<EventLocalRetrofit>();
    @SerializedName("eventtype")
    @Expose
    private List<EventtypeRetrofit> eventtype = new ArrayList<EventtypeRetrofit>();
    @SerializedName("_event_artistes_dj_organisateurs")
    @Expose
    private List<EventArtistesDjOrganisateurRetrofit> eventArtistesDjOrganisateurs = new ArrayList<EventArtistesDjOrganisateurRetrofit>();
    @SerializedName("_event_price")
    @Expose
    private List<EventPriceRetrofit> eventPrice = new ArrayList<EventPriceRetrofit>();
    @SerializedName("_event_flyers")
    @Expose
    private List<EventFlyersRetrofit> eventFlyers = new ArrayList<EventFlyersRetrofit>();
    @SerializedName("_event_date")
    @Expose
    private List<EventDateRetrofit> eventDate = new ArrayList<EventDateRetrofit>();
    @SerializedName("_eventvideos")
    @Expose
    private List<EventVideosRetrofit> eventvideos = new ArrayList<EventVideosRetrofit>();
    @SerializedName("_articles_event")
    @Expose
    private List<ArticlesEventRetrofit> articlesEvent = new ArrayList<ArticlesEventRetrofit>();
    @SerializedName("_event_related")
    @Expose
    private List<EventRetrofit> eventRelated = new ArrayList<EventRetrofit>();
    @SerializedName("_event_date_home")
    @Expose
    private List<EventDateHomeRetrofit> eventDateHome = new ArrayList<EventDateHomeRetrofit>();
    @SerializedName("_event_date_home_local")
    @Expose
    private List<EventDateHomeLocalRetrofit> eventDateHomeLocal = new ArrayList<EventDateHomeLocalRetrofit>();

    @SerializedName("typerelation")
    @Expose
    private String typerelation;

    @SerializedName("dateclair")
    @Expose
    private String dateClair;

    @SerializedName("optdateclair")
    @Expose
    private String optDateClair;

    public String getDateClair() {
        return dateClair;
    }

    public void setDateClair(String dateClair) {
        this.dateClair = dateClair;
    }

    public String getOptDateClair() {
        return optDateClair;
    }

    public void setOptDateClair(String optDateClair) {
        this.optDateClair = optDateClair;
    }

    protected EventRetrofit(Parcel in) {
        name = in.readString();
        flyer = in.readString();
        slug = in.readString();
        dateUnique = in.readString();
        heureDebut = in.readString();
        heureFin = in.readString();
        createdAt = in.readString();
        dateDebut = in.readString();
        dateFin = in.readString();
        typerelation = in.readString();
        dateClair = in.readString();
        optDateClair = in.readString();
    }

    public EventRetrofit(){}

    public static final Creator<EventRetrofit> CREATOR = new Creator<EventRetrofit>() {
        @Override
        public EventRetrofit createFromParcel(Parcel in) {
            return new EventRetrofit(in);
        }

        @Override
        public EventRetrofit[] newArray(int size) {
            return new EventRetrofit[size];
        }
    };

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The flyer
     */
    public String getFlyer() {
        return flyer;
    }

    /**
     *
     * @param flyer
     * The flyer
     */
    public void setFlyer(String flyer) {
        this.flyer = flyer;
    }

    /**
     *
     * @return
     * The slug
     */
    public String getSlug() {
        return slug;
    }

    /**
     *
     * @param slug
     * The slug
     */
    public void setSlug(String slug) {
        this.slug = slug;
    }

    /**
     *
     * @return
     * The cancelledAt
     */
    public Boolean getCancelledAt() {
        return cancelledAt;
    }

    /**
     *
     * @param cancelledAt
     * The cancelled_at
     */
    public void setCancelledAt(Boolean cancelledAt) {
        this.cancelledAt = cancelledAt;
    }

    /**
     *
     * @return
     * The position
     */
    public Integer getPosition() {
        return position;
    }

    /**
     *
     * @param position
     * The position
     */
    public void setPosition(Integer position) {
        this.position = position;
    }

    /**
     *
     * @return
     * The position
     */
    public String getPrixenclair() {
        return prixenclair;
    }

    /**
     *
     * @param position
     * The position
     */
    public void setPrixenclair(String position) {
        this.prixenclair = prixenclair;
    }

    /**
     *
     * @return
     * The dateUnique
     */
    public String getDateUnique() {
        return dateUnique;
    }

    /**
     *
     * @param dateUnique
     * The date_unique
     */
    public void setDateUnique(String dateUnique) {
        this.dateUnique = dateUnique;
    }

    /**
     *
     * @return
     * The heureDebut
     */
    public String getHeureDebut() {
        return heureDebut;
    }

    /**
     *
     * @param heureDebut
     * The heure_debut
     */
    public void setHeureDebut(String heureDebut) {
        this.heureDebut = heureDebut;
    }

    /**
     *
     * @return
     * The heureFin
     */
    public String getHeureFin() {
        return heureFin;
    }

    /**
     *
     * @param heureFin
     * The heure_fin
     */
    public void setHeureFin(String heureFin) {
        this.heureFin = heureFin;
    }

    /**
     *
     * @return
     * The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     *
     * @param createdAt
     * The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     *
     * @return
     * The hidden
     */
    public Boolean getHidden() {
        return hidden;
    }

    /**
     *
     * @param hidden
     * The hidden
     */
    public void setHidden(Boolean hidden) {
        this.hidden = hidden;
    }

    /**
     *
     * @return
     * The dateDebut
     */
    public String getDateDebut() {
        return dateDebut;
    }

    /**
     *
     * @param dateDebut
     * The date_debut
     */
    public void setDateDebut(String dateDebut) {
        this.dateDebut = dateDebut;
    }

    /**
     *
     * @return
     * The dateFin
     */
    public String getDateFin() {
        return dateFin;
    }

    /**
     *
     * @param dateFin
     * The date_fin
     */
    public void setDateFin(String dateFin) {
        this.dateFin = dateFin;
    }

    /**
     *
     * @return
     * The entreetype
     */
    public EventlieuRetrofit getEventLieu() {
        return eventlieu;
    }

    /**
     *
     * @param entreetype
     * The entreetype
     */
    public void setEventLieu(EventlieuRetrofit eventlieu) {
        this.eventlieu = eventlieu;
    }

    /**
     *
     * @return
     * The eventmultilieu
     */
    public List<EventMultiLieuRetrofit> getEventmultilieu() {
        return eventmultilieu;
    }

    /**
     *
     * @param eventmultilieu
     * The eventmultilieu
     */
    public void setEventmultilieu(List<EventMultiLieuRetrofit> eventmultilieu) {
        this.eventmultilieu = eventmultilieu;
    }



    /**
     *
     * @return
     * The entreetype
     */
    public EntreetypeRetrofit getEntreetype() {
        return entreetype;
    }

    /**
     *
     * @param entreetype
     * The entreetype
     */
    public void setEntreetype(EntreetypeRetrofit entreetype) {
        this.entreetype = entreetype;
    }

    /**
     *
     * @return
     * The eventlocal
     */
    public List<EventLocalRetrofit> getEventlocal() {
        return eventlocal;
    }

    /**
     *
     * @param eventlocal
     * The eventlocal
     */
    public void setEventlocal(List<EventLocalRetrofit> eventlocal) {
        this.eventlocal = eventlocal;
    }

    /**
     *
     * @return
     * The eventtype
     */
    public List<EventtypeRetrofit> getEventtype() {
        return eventtype;
    }

    /**
     *
     * @param eventtype
     * The eventtype
     */
    public void setEventtype(List<EventtypeRetrofit> eventtype) {
        this.eventtype = eventtype;
    }

    /**
     *
     * @return
     * The eventArtistesDjOrganisateurs
     */
    public List<EventArtistesDjOrganisateurRetrofit> getEventArtistesDjOrganisateurs() {
        return eventArtistesDjOrganisateurs;
    }

    /**
     *
     * @param eventArtistesDjOrganisateurs
     * The _event_artistes_dj_organisateurs
     */
    public void setEventArtistesDjOrganisateurs(List<EventArtistesDjOrganisateurRetrofit> eventArtistesDjOrganisateurs) {
        this.eventArtistesDjOrganisateurs = eventArtistesDjOrganisateurs;
    }

    /**
     *
     * @return
     * The eventPrice
     */
    public List<EventPriceRetrofit> getEventPrice() {
        return eventPrice;
    }

    /**
     *
     * @param eventPrice
     * The _event_price
     */
    public void setEventPrice(List<EventPriceRetrofit> eventPrice) {
        this.eventPrice = eventPrice;
    }

    /**
     *
     * @return
     * The eventFlyers
     */
    public List<EventFlyersRetrofit> getEventFlyers() {
        return eventFlyers;
    }

    /**
     *
     * @param eventFlyers
     * The _event_flyers
     */
    public void setEventFlyers(List<EventFlyersRetrofit> eventFlyers) {
        this.eventFlyers = eventFlyers;
    }

    /**
     *
     * @return
     * The eventDate
     */
    public List<EventDateRetrofit> getEventDate() {
        return eventDate;
    }

    /**
     *
     * @param eventDate
     * The _event_date
     */
    public void setEventDate(List<EventDateRetrofit> eventDate) {
        this.eventDate = eventDate;
    }

    /**
     *
     * @return
     * The eventvideos
     */
    public List<EventVideosRetrofit> getEventvideos() {
        return eventvideos;
    }

    /**
     *
     * @param eventvideos
     * The _eventvideos
     */
    public void setEventvideos(List<EventVideosRetrofit> eventvideos) {
        this.eventvideos = eventvideos;
    }

    /**
     *
     * @return
     * The articlesEvent
     */
    public List<ArticlesEventRetrofit> getArticlesEvent() {
        return articlesEvent;
    }

    /**
     *
     * @param articlesEvent
     * The _articles_event
     */
    public void setArticlesEvent(List<ArticlesEventRetrofit> articlesEvent) {
        this.articlesEvent = articlesEvent;
    }



    /**
     *
     * @return
     * The eventDateHome
     */
    public List<EventDateHomeRetrofit> getEventDateHome() {
        return eventDateHome;
    }

    /**
     *
     * @param eventDateHome
     * The _event_date_home
     */
    public void setEventDateHome(List<EventDateHomeRetrofit> eventDateHome) {
        this.eventDateHome = eventDateHome;
    }

    /**
     *
     * @return
     * The eventDateHomeLocal
     */
    public List<EventDateHomeLocalRetrofit> getEventDateHomeLocal() {
        return eventDateHomeLocal;
    }

    /**
     *
     * @param eventDateHomeLocal
     * The _event_date_home_local
     */
    public void setEventDateHomeLocal(List<EventDateHomeLocalRetrofit> eventDateHomeLocal) {
        this.eventDateHomeLocal = eventDateHomeLocal;
    }

    /**
     *
     * @return
     * The name
     */
    public String getTypeRelation() {
        return typerelation;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setTypeRelation(String typerelation) {
        this.typerelation = typerelation;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(flyer);
        dest.writeString(slug);
        dest.writeString(dateUnique);
        dest.writeString(heureDebut);
        dest.writeString(heureFin);
        dest.writeString(createdAt);
        dest.writeString(dateDebut);
        dest.writeString(dateFin);
        dest.writeString(dateClair);
        dest.writeString(optDateClair);
    }
}
