package mada.tsara.backend.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Miary on 26/05/2017.
 */

public class UploadFlyerRetrofit {

    @SerializedName("success")
    @Expose
    private String success;

    @SerializedName("filename")
    @Expose
    private String filename;

    @SerializedName("error")
    @Expose
    private String error;

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }
}
