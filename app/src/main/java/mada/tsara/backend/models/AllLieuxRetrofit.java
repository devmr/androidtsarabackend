package mada.tsara.backend.models;

import android.graphics.Region;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Miary on 31/05/2017.
 */

public class AllLieuxRetrofit {

    @SerializedName("lieu")
    @Expose
    private List<EventlieuRetrofit> lieu = new ArrayList<EventlieuRetrofit>();

    @SerializedName("country")
    @Expose
    private List<CountryRetrofit> countries = new ArrayList<CountryRetrofit>();

    @SerializedName("region")
    @Expose
    private List<RegionRetrofit> regions = new ArrayList<RegionRetrofit>();

    @SerializedName("quartier")
    @Expose
    private List<QuartierRetrofit> quartiers = new ArrayList<QuartierRetrofit>();


    /**
     *
     * @return
     * The event
     */
    public List<EventlieuRetrofit> getLieu() {
        return lieu;
    }

    /**
     *
     * @param event
     * The Event
     */
    public void setLieu(List<EventlieuRetrofit> lieu) {
        this.lieu = lieu;
    }

    /**
     *
     * @return
     * The event
     */
    public List<CountryRetrofit> getCountries() {
        return countries;
    }

    /**
     *
     * @param event
     * The Event
     */
    public void setCountries(List<CountryRetrofit> countries) {
        this.countries = countries;
    }

    /**
     *
     * @return
     * The event
     */
    public List<QuartierRetrofit> getQuartiers() {
        return quartiers;
    }

    /**
     *
     * @param event
     * The Event
     */
    public void setQuartiers(List<QuartierRetrofit> quartiers) {
        this.quartiers = quartiers;
    }

    /**
     *
     * @return
     * The event
     */
    public List<RegionRetrofit> getRegions() {
        return regions;
    }

    /**
     *
     * @param event
     * The Event
     */
    public void setRegions(List<RegionRetrofit> regions) {
        this.regions = regions;
    }

}
