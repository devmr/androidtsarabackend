package mada.tsara.backend.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Miary on 28/05/2017.
 */

public class AllEntreeTypeRetrofit {
    @SerializedName("results")
    @Expose
    private List<EntreetypeRetrofit> results = new ArrayList<EntreetypeRetrofit>();

    /**
     *
     * @return
     * The results
     */
    public List<EntreetypeRetrofit> getResults() {
        return results;
    }

    /**
     *
     * @param results
     * The results
     */
    public void setResults(List<EntreetypeRetrofit> results) {
        this.results = results;
    }
}
