package mada.tsara.backend.activities;

import android.support.v7.app.AppCompatActivity;

public class GenericActivity extends AppCompatActivity {

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onStop() {
        super.onStop();

    }
}
