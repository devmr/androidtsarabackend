package mada.tsara.backend.activities;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import mada.tsara.backend.R;
import mada.tsara.backend.fragments.AddeventFragment;
import mada.tsara.backend.share.BusEvent;
import mada.tsara.backend.share.Config;
import mada.tsara.backend.share.Utils;

public class AddeventActivity extends GenericActivity {

    Uri imageUri;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(Config.TAGKEY, " - STEP - onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addevent);

         // Intent
        doGetIntent();



    }

    @Override
    public void onStart() {
        Log.d(Config.TAGKEY, " - STEP - onStart");
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        Log.d(Config.TAGKEY, " - STEP - onStop");
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onEvent(BusEvent event) {

        Log.d(Config.TAGKEY, " - STEP - onEvent");
        if ( event.getFinishActivity() ) {
            Log.d(Config.TAGKEY,  " - STEP - onEvent - getFinishActivity");
            this.finish();
        }


    }

    @Override
    public void onBackPressed() {
        Log.d(Config.TAGKEY, " AddeventActivity - STEP - onBackPressed");
//        super.onBackPressed();
        BusEvent busEvent = new BusEvent();
        busEvent.setParentBackPressed(true);
        EventBus.getDefault().post(busEvent);
        overridePendingTransition(R.anim.activity_back_in, R.anim.activity_back_out);
    }

    @Override
    protected void onResume() {
        Log.d(Config.TAGKEY, " - STEP - onResume");
        super.onResume();

    }



    /**
     * Recuperer les infos de l'intent et afficher l'image
     */
    void doGetIntent() {

        Intent i = getIntent();
        String action = i.getAction();
        String type = i.getType();

        if (Intent.ACTION_SEND.equals(action) && type != null) {
            if (type.startsWith("image/")) {
                handleSendImage(i); // Handle single image being sent
            }
        }
    }

    private void showFragment() {
        Log.d(Config.TAGKEY, " - STEP - showFragment");

        AddeventFragment fragment = new AddeventFragment().newInstance(imageUri);

        android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.contentframelayout, fragment).commit();


    }
    /**
     * Afficher l'image si permission ok
     *
     * @param intent
     */
    void handleSendImage(Intent intent) {

        int REQUEST_EXTERNAL_STORAGE = 0;

        if (Utils.getPermissionReadExternalStorage(this)) {
            imageUri = (Uri) intent.getParcelableExtra(Intent.EXTRA_STREAM);
            showFragment();
        }
    }





    /**
     * Callback received when a permissions request has been completed.
     *
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case 0 :
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted
                    new Thread(new Runnable() {
                        public void run() {

                        }
                    }).start();
                }
                break;

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        Log.d(Config.TAGKEY, " - STEP - onActivityResult");
        if(resultCode == 200){
            String artistename = data.getStringExtra("artistename");
            int artisteid = data.getIntExtra("artisteid",0);

            // Envoyer vers le fragment pour etre rempli dans le recycler
            BusEvent busEvent = new BusEvent();
            busEvent.setResultActivity(artisteid,artistename);
            EventBus.getDefault().post(busEvent);
        }
    }
}
