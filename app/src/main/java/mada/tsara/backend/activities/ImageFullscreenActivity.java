package mada.tsara.backend.activities;

import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;

import com.bumptech.glide.Glide;
import com.github.chrisbanes.photoview.PhotoView;

import mada.tsara.backend.R;
import mada.tsara.backend.share.Config;
import mada.tsara.backend.share.Utils;

public class ImageFullscreenActivity extends AppCompatActivity {

    boolean is_slide_up = false;

    Toolbar toolbar;

    Animation anim_slide_up;
    Animation anim_slide_down;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFullscreen();


        setContentView(R.layout.activity_image_fullscreen);

        anim_slide_up = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.slide_up);
        anim_slide_down = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.slide_down);


//        final TouchImageView imageView = (TouchImageView) findViewById(R.id.imageView);
//        final ImageView imageView = (ImageView) findViewById(R.id.imageView);
        final PhotoView imageView = (PhotoView) findViewById(R.id.imageView);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //toolbar.setVisibility( toolbar.getVisibility()==View.GONE ? View.VISIBLE : View.GONE  );
                if ( !is_slide_up ) {
                    //toolbar.startAnimation(anim_slide_up);
                    toolbar.animate().translationY(-toolbar.getHeight()).setInterpolator(new AccelerateInterpolator(2));
                    is_slide_up = true;
                } else {
                    //toolbar.startAnimation(anim_slide_down);
                    toolbar.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2));
                    is_slide_up = false;
                }
            }
        });

        Intent i = getIntent();
        if (TextUtils.isEmpty(i.getStringExtra("fileremote")) && i.getParcelableExtra("mimageUri") != null && Utils.getPermissionReadExternalStorage(this)) {
            Uri mimageUri = i.getParcelableExtra("mimageUri");
            Log.d(Config.TAGKEY, " ImageFullscreenActivity - STEP - mimageUri "+mimageUri.toString());
            /*Glide.
                    with(this)
                    .load(mimageUri.toString())
                    .into(imageView);*/
            imageView.setImageURI(mimageUri);
        } else if ( i.getStringExtra("fileremote") != null && !TextUtils.isEmpty(i.getStringExtra("fileremote")) ) {
            String fileRemote = i.getStringExtra("fileremote");
            Log.d(Config.TAGKEY, " ImageFullscreenActivity - STEP - fileRemote "+fileRemote);
             Glide.
                    with(this)
                    .load(fileRemote)
                    .into(imageView);




        }

        setupToolbar();
    }

    private void setupToolbar() {



        toolbar = (Toolbar)  findViewById(R.id.toolbar);
        //toolbar.startAnimation(anim_slide_down);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                toolbar.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2));
            }
        }, 500);


        setSupportActionBar(toolbar);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_clear_white_24dp);

        getSupportActionBar().setTitle( "" );





        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageFullscreenActivity.this.finish();
            }
        });

    }

    private void setFullscreen() {
        setFullscreenNewer();
        if (Build.VERSION.SDK_INT > 10) {
            registerSystemUiVisibility();
        }
    }
    public void setFullscreenNewer() {
        if (Build.VERSION.SDK_INT > 10) {
            int flags = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_FULLSCREEN;

            if (isImmersiveAvailable()) {
                flags |= View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION |
                        View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
            }

            getWindow().getDecorView().setSystemUiVisibility(flags);
        } else {
            getWindow()
                    .setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
    }
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void registerSystemUiVisibility() {
        final View decorView = getWindow().getDecorView();
        decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {

            @Override
            public void onSystemUiVisibilityChange(int visibility) {
                if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
                    setFullscreenNewer();
                }
            }
        });
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void unregisterSystemUiVisibility() {
        final View decorView = getWindow().getDecorView();
        decorView.setOnSystemUiVisibilityChangeListener(null);
    }
    public static boolean isImmersiveAvailable() {
        return android.os.Build.VERSION.SDK_INT >= 19;
    }
    public void exitFullscreen() {
        if (Build.VERSION.SDK_INT > 10) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);
        } else {
            getWindow()
                    .setFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN,
                            WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
        }
    }
    /**
     * Callback received when a permissions request has been completed.
     *
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case 0 :
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted
                    new Thread(new Runnable() {
                        public void run() {

                        }
                    }).start();
                }
                break;

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}
