package mada.tsara.backend.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import mada.tsara.backend.R;
import mada.tsara.backend.fragments.AddeventFragment;
import mada.tsara.backend.share.BusEvent;
import mada.tsara.backend.share.Config;

public class EditeventActivity extends AppCompatActivity {

    Uri imageUri;

    String fieldImageView;

    int eventId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editevent);

        // Intent
        doGetIntent();
    }
    @Override
    public void onStart() {
        Log.d(Config.TAGKEY, " - STEP - onStart");
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        Log.d(Config.TAGKEY, " - STEP - onStop");
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onEvent(BusEvent event) {

        Log.d(Config.TAGKEY, " - STEP - onEvent");
        if ( event.getFinishActivity() ) {
            Log.d(Config.TAGKEY,  " - STEP - onEvent - getFinishActivity");
            this.finish();
        }


    }
    @Override
    public void onBackPressed() {
        Log.d(Config.TAGKEY, " AddeventActivity - STEP - onBackPressed");
//        super.onBackPressed();
        BusEvent busEvent = new BusEvent();
        busEvent.setParentBackPressedByEdit(true);
        EventBus.getDefault().post(busEvent);
    }
    @Override
    protected void onResume() {
        Log.d(Config.TAGKEY, " - STEP - onResume");
        super.onResume();

    }

    /**
     * Recuperer les infos de l'intent et afficher l'image
     */
    void doGetIntent() {

        Intent i = getIntent();
        boolean eventIdNotEmpty = i.hasExtra("eventId");
        boolean uriNotEmpty = i.hasExtra("mimageUri");
        boolean imageviewNotEmpty = i.hasExtra("fieldImageView");

        if (uriNotEmpty)
            imageUri = (Uri) i.getParcelableExtra("mimageUri");

        if (imageviewNotEmpty)
            fieldImageView = (String) i.getStringExtra("fieldImageView");

        if (eventIdNotEmpty)
            eventId = (int) i.getIntExtra("eventId",0);

        showFragment();

    }

    private void showFragment() {
        Log.d(Config.TAGKEY, " - STEP - showFragment "+eventId+"-"+imageUri.toString()+"-"+fieldImageView);

        AddeventFragment fragment = new AddeventFragment().newInstance(eventId,imageUri,fieldImageView);

        android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.contentframelayout, fragment).commit();


    }
}
