package mada.tsara.backend.activities;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import mada.tsara.backend.R;
import mada.tsara.backend.fragments.AddEditArtistsFragment;
import mada.tsara.backend.share.BusEvent;
import mada.tsara.backend.share.Config;

import static mada.tsara.backend.activities.ImageFullscreenActivity.isImmersiveAvailable;

public class PopupActivity extends AppCompatActivity {

//    IconButton icontextview_close;
    Fragment fragment;

    String className = "PopupActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setFullscreen();
        setContentView(R.layout.activity_popup);

        // Initialiser les vues
        initUi();

        // Action sur les vues
        doSetUi();
    }

    @Override
    public void onStart() {
        Log.d(Config.TAGKEY, className+" - STEP - onStart");
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        Log.d(Config.TAGKEY, className+" - STEP - onStop");
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onEvent(BusEvent event) {

        Log.d(Config.TAGKEY, " - STEP - onEvent");
        if ( event.getFinishPopupActivity() ) {
            Log.d(Config.TAGKEY,  " - STEP - onEvent - getFinishPopupActivity");
            this.finish();
        }

        if (event.getResultActivity()) {
            Log.d(Config.TAGKEY,  " - STEP - onEvent - getResultActivity");
            publishResult(event.getResultActivityArtisteId(), event.getResultActivityArtisteName());
        }


    }

    @Override
    protected void onResume() {
        Log.d(Config.TAGKEY, className+" - STEP - onResume");
        super.onResume();

    }

    /**
     * Action sur les vues
     */
    private void doSetUi() {
        Log.d(Config.TAGKEY, className+" - STEP - doSetUi");

        /*// Bouton Fermer
        if (null!=icontextview_close) {
            icontextview_close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // Fermer l'activity
                    finish();
                }
            });
        }*/
    }

    /**
     * Initialiser les vues
     */
    protected void initUi() {
        Log.d(Config.TAGKEY, className+" - STEP - initUi");

        // Les differentes vues
//        icontextview_close = (IconButton) findViewById(R.id.icontextview_close);

        // Intent
        doSetIntent();

        // Remplir Fragment
        if(null!=fragment)
            showFragment(fragment);
    }

    private void doSetIntent() {
        Log.d(Config.TAGKEY, className+" - STEP - doSetIntent");
        Intent i = getIntent();
        if ( i.getStringExtra("fragment") != null ) {
            String fragmentStr = i.getStringExtra("fragment");
            int itemId = i.getIntExtra("itemId",0);
            String artisteName = i.getStringExtra("artisteName");
            Log.d(Config.TAGKEY, className+" - STEP - doSetIntent - fragmentStr : "+fragmentStr+" - itemId: "+itemId+" - artisteName: "+artisteName);
            switch (fragmentStr) {
                case "artiste":
                    fragment = new AddEditArtistsFragment().newInstance(itemId,artisteName);
                    break;
                case "fete":
                    fragment = new AddEditArtistsFragment().newInstance(itemId,"");
                    break;
                case "lieux":
                    fragment = new AddEditArtistsFragment().newInstance(itemId,"");
                    break;
            }
        }
        Log.d(Config.TAGKEY, className+" - STEP - doSetIntent - fragment: "+fragment);
    }

    private void showFragment(Fragment fragment) {
        Log.d(Config.TAGKEY, className+" - STEP - showFragment");
        // AddEditArtistsFragment fragment = new AddEditArtistsFragment().newInstance("","");
        android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
//        fragmentTransaction.replace(R.id.contentframelayout, fragment).commit();
        fragmentTransaction.add(R.id.contentframelayout, fragment).commit();


    }

    private void publishResult(int id, String name) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("artisteid",id);
        returnIntent.putExtra("artistename",name);
        setResult(200,returnIntent);
        finish();
    }

    @Override
    public void onBackPressed() {
        Log.d(Config.TAGKEY, className + " - STEP - onBackPressed");
        // finish() is called in super: we only override this method to be able to override the transition
        super.onBackPressed();
    }

    private void setFullscreen() {
        setFullscreenNewer();
        if (Build.VERSION.SDK_INT > 10) {
            registerSystemUiVisibility();
        }
    }
    public void setFullscreenNewer() {
        if (Build.VERSION.SDK_INT > 10) {
            int flags = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_FULLSCREEN;

            if (isImmersiveAvailable()) {
                flags |= View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION |
                        View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
            }

            getWindow().getDecorView().setSystemUiVisibility(flags);
        } else {
            getWindow()
                    .setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
    }
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void registerSystemUiVisibility() {
        final View decorView = getWindow().getDecorView();
        decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {

            @Override
            public void onSystemUiVisibilityChange(int visibility) {
                if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
                    setFullscreenNewer();
                }
            }
        });
    }


}
