package mada.tsara.backend.service;

import java.util.List;

import mada.tsara.backend.models.AllEntreeTypeRetrofit;
import mada.tsara.backend.models.AllEventArtistesDjOrganisateurRetrofit;
import mada.tsara.backend.models.AllEventTypeRetrofit;
import mada.tsara.backend.models.AllLieuxRetrofit;
import mada.tsara.backend.models.EventArtistesDjOrganisateurRetrofit;
import mada.tsara.backend.models.EventLocalRetrofit;
import mada.tsara.backend.models.EventRetrofit;
import mada.tsara.backend.models.EventlieuRetrofit;
import mada.tsara.backend.models.GenericResultRetrofit;
import mada.tsara.backend.models.UploadFlyerRetrofit;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

/**
 * Created by Miary on 26/05/2017.
 */

public interface RetrofitInterface {

    //get All local
    @GET("lieux")
    Call<AllLieuxRetrofit> getAllLieux();

    //get All local
    @GET("lieux")
    Call<AllLieuxRetrofit> getAllLieux(@Query("name") String name );

    //get All artistes by query
    @GET("artistesdj")
    Call<AllEventArtistesDjOrganisateurRetrofit> getArtisteByQuery(@Query("name") String name );

    //Get all Thematique
    @GET("thematiques")
    Call<AllEventTypeRetrofit> getAllEventType();


    //Get all Thematique
    @GET("entreetypes")
    Call<AllEntreeTypeRetrofit> getAllEntreeType();

    @GET("local")
    Call<List<EventLocalRetrofit>> getAllLocal(@Query("name") String s );

    @GET("local")
    Call<List<EventLocalRetrofit>> getAllLocal(
            @Query("name") String s,
            @Query("limit") int limit
    );

    @GET("event")
    Call<List<EventRetrofit>> getAllEvents(@Query("name") String s );

    // Upload flyer
    @Multipart
    @POST("uploadflyers")
    Call<UploadFlyerRetrofit> uploadFlyer(
            @Part MultipartBody.Part flyer,
            @Part("oldfilename") RequestBody oldfilename,
            @Part("flyerurl") RequestBody flyerurl
    );

    @FormUrlEncoded
    @POST("saveevents")
    Call<GenericResultRetrofit> createEvents(
            @Field("data[name]") String name,
            @Field("data[prixenclair]") String prixenclair,
            @Field("data[hidden]") int hidden,
            @Field("data[cancelledat]") int cancelledat,
            @Field("data[dateclair]") String dateclair,
            @Field("data[eventtype]") int eventtype,
            @Field("data[filenameajax]") String filenameajax,
            @Field("data[dragandrop]") String dragandrop,
            @Field("data[eventmultilieu]") String eventmultilieu,
            @Field("data[eventlocal]") String eventlocal,
            @Field("data[artistesdj]") String artistesdj,
            @Field("data[entreetype]") int entreetype,
            @Field("data[multiflyer]") String multiflyer,
            @Field("data[api]") String api,
            @Field("data[optdateclair]") String optdateclair
    );

    @FormUrlEncoded
    @POST("saveevents")
    Call<GenericResultRetrofit> updateEvents(
            @Field("data[name]") String name,
            @Field("data[prixenclair]") String prixenclair,
            @Field("data[hidden]") int hidden,
            @Field("data[cancelledat]") int cancelledat,
            @Field("data[dateclair]") String dateclair,
            @Field("data[eventtype]") int eventtype,
            @Field("data[filenameajax]") String filenameajax,
            @Field("data[dragandrop]") String dragandrop,
            @Field("data[eventmultilieu]") String eventmultilieu,
            @Field("data[eventlocal]") String eventlocal,
            @Field("data[artistesdj]") String artistesdj,
            @Field("data[entreetype]") int entreetype,
            @Field("data[multiflyer]") String multiflyer,
            @Field("data[api]") String api,
            @Field("data[optdateclair]") String optdateclair,
            @Field("data[id]") int id
    );

    //Creer un user
    @POST("savelocals")
    Call<GenericResultRetrofit> createLocal(@Body EventLocalRetrofit user);

    // Creer ou update lieu
    @FormUrlEncoded
    @POST("savelieus")
    Call<GenericResultRetrofit> createLieu(
            @Field("data[name]") String name,
            @Field("data[tel]") String tel,
            @Field("data[facebook]") String facebook,
            @Field("data[email]") String email,
            @Field("data[www]") String www,
            @Field("data[gps]") String gps,
            @Field("data[id]") Integer id
    );

    // Creer un lieu
    @FormUrlEncoded
    @POST("savelieus")
    Call<GenericResultRetrofit> createLieu(
            @Field("data[name]") String name
    );

    //Creer un artiste
    @FormUrlEncoded
    @POST("saveartists")
    Call<GenericResultRetrofit> createArtists(
            @Field("data[name]") String name,
            @Field("data[type]") String type
    );
    //Creer un artiste
    @FormUrlEncoded
    @POST("saveartists")
    Call<GenericResultRetrofit> createArtists(
            @Field("data[name]") String name,
            @Field("data[type]") String type,
            @Field("data[filenameajax]") String filenameajax,
            @Field("data[dragandrop]") String dragandrop,
            @Field("data[parentartisteid]") String parentartisteid,
            @Field("data[enfantartisteid]") String enfantartisteid,
            @Field("data[event]") String event
    );

    //Creer un artiste
    @FormUrlEncoded
    @POST("saveartists")
    Call<GenericResultRetrofit> updateArtists(
            @Field("data[name]") String name,
            @Field("data[type]") String type,
            @Field("data[filenameajax]") String filenameajax,
            @Field("data[dragandrop]") String dragandrop,
            @Field("data[parentartisteid]") String parentartisteid,
            @Field("data[enfantartisteid]") String enfantartisteid,
            @Field("data[event]") String event,
            @Field("data[id]") int id
    );

    //get event detail
    @GET("eventdetail")
    Call<EventRetrofit> getEvent(@Query("id") int id );

    //Creer un user
    @GET("deleteflyer")
    Call<GenericResultRetrofit> deleteFlyer(@Query("id") int id);

    //Creer un user
    @GET("setflyer")
    Call<GenericResultRetrofit> setFlyer(@Query("eventid") int eventid, @Query("id") int id);

    //Creer un user
    @GET("addtmpflyertoevent")
    Call<GenericResultRetrofit> addFlyerToEvent(@Query("filename") String filename, @Query("eventid") int eventid);

    //get event detail
    @GET("artistedetail")
    Call<EventArtistesDjOrganisateurRetrofit> getArtiste(@Query("id") int id );

}
